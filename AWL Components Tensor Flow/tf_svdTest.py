#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 16:21:50 2017

@author: james
"""

# The tensor flow version of svd scales incredibly poorly.... :(

import tensorflow as tf
import numpy as np
import time as t

def svdTest():
    
    S = np.random.rand(3000, 200)
    R = np.matmul(S, np.transpose(S) )
    
    s, U, V = tf.svd(R)
    
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    s_val = []
    U_val = []
    V_val = []
    
    sampleNum = 10
    start=t.time()
    for i in range(0,sampleNum):
        
        s_val, U_val, V_val= sess.run([s, U, V])
    
    stop=t.time()
    procTime = (stop-start)/sampleNum
    
    print('Step Time: '+ str(procTime) +'sec')
    print(s_val.shape)
    print(U_val.shape)
    print(V_val.shape)
    
svdTest()
    
        