#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 15:25:58 2017

@author: james

This is just a small set of method for unpacking the CIFAR 100 dataset

"""

import numpy as np

cifarDir10 = "../Basic Data Sets/cifar-10-batches-py/"
cifarDir100 = "../Basic Data Sets/cifar-100-python/"


# Unpack the raw data
def getCifarData(dataset='100', batch=1):
    
    cifarDir = []
    
    dictTrain = []
    dictTest = []
    
    if dataset == '100':
        cifarDir = cifarDir100
        dictTrain = unpickle(cifarDir+"train")
        dictTest  = unpickle(cifarDir+"test")
    else:
        cifarDir = cifarDir10
        dictTrain = unpickle(cifarDir+"data_batch_"+str(batch))
        dictTest  = unpickle(cifarDir+"test_batch")
        
    
    trainDat = dictTrain[b'data']
    testDat = dictTest[b'data']
    
    if dataset == '100':
        trainLabels = (dictTrain[b'coarse_labels'],dictTrain[b'fine_labels'],)
        testLabels = (dictTest[b'coarse_labels'],dictTest[b'fine_labels'],) 
    else:
        trainLabels = (dictTrain[b'labels'],dictTrain[b'labels'])
        testLabels = (dictTest[b'labels'],dictTest[b'labels']) 
    
    return trainDat, trainLabels, testDat, testLabels

# Get the data but put the images in an image shape
def getCifarShaped(dataset='100', batch=1):
    
    trainDat, trainLabels, testDat, testLabels = getCifarData(dataset=dataset, batch=batch)
    
    # Reshape to 32x32xCxL 
    #print(trainDat.transpose().shape)
    trainDat = np.reshape(trainDat.transpose(), [3, 32, 32, trainDat.shape[0]])
    testDat = np.reshape(testDat.transpose(), [3, 32, 32, testDat.shape[0]])
    
    trainDat = trainDat.transpose([1,2,0,3])
    testDat = testDat.transpose([1,2,0,3])
    
    return trainDat, trainLabels, testDat, testLabels
    

def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        Dict = pickle.load(fo, encoding='bytes')
    return Dict



def cifarTest():
    import numpy as np
    import matplotlib.pyplot as plt
    
    trainDat, trainLabels, testDat, testLabels = getCifarShaped()
    
    #print(trainLabels[0])
    
    for i in range(0,50):
        plt.cla()
        plt.imshow(np.sum(testDat[:,:,:,i],2), cmap='gray')
        print('Stim '+str(i))
        plt.pause(1)
    
    
#cifarTest()