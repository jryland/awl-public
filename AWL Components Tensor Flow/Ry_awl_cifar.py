#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 15:51:55 2017

@author: james


Ry_awl_cifar will contain several simulation tests to see if AWEL can create a
V1 like map representation and to see if deep layering of AWEL can create 
high-level category seperated maps and features. 

simulation calls
-AWEL_Cifar_V1()
-AWEL_Cifar_Deep()


"""

# Imports for all functions and classes
import tensorflow as tf
import Foveal as fv
import numpy as np
import matplotlib.pyplot as plt
import data_cifar100

import Ry_awl as awl
import Ry_LayerGraphs  as lg
import Ry_BasicMathTF as bm

import StopWatch
from NumFormat import sigdig


"""
___________________________________________________________________STIMULUS GEN
Here we will create some convenient classes for generating stimuli from cifar
this will bundle the fixation management and source updating together.
"""
class CifarFixManager:
    
    # Unpack cifar and setup fixation manager
    def __init__(self, baseRez, cycles, scales, factor, returnRez, topFov, dataset='10', color='lum+op'):
        # Define the stimulus generators size and dimensions
        self.dataset = dataset
        self.trainDat, self.trainLab, self.testDat, self.testLab = data_cifar100.getCifarShaped(dataset)
        self.numS = self.trainDat.shape[3]
        self.returnRez = returnRez
        
        # Define the fixation manager
        shift = 3
        inhDecay = 1
        self.fixMan = fv.FixationManager(baseRez, cycles, scales, factor, shift, inhDecay, col=color, scaleDown = 1, topFov = topFov)
        
        # Internal timer for training data
        self.t = 0
        
        # Internal timer for testing data
        self.i = 0
        
        # Current stimulus
        self.pick = 0
        
        
    # get the next training stimulus and update the source when nescessary
    def getNextTrain(self, newSource, randFixJump = 0):
        
        s_val, stimIm, ID, idVecs, finalFix = self.getNext('Train', newSource, randFixJump = randFixJump)
        
        # Update internal time
        self.t = self.t+1
        
        return  s_val, stimIm, ID, idVecs, finalFix
    
    # get the next training stimulus and update the source when nescessary
    def getNextTest(self, newSource, randFixJump = 0):
        
        s_val, stimIm, ID, idVecs, finalFix = self.getNext('Test', newSource, randFixJump = randFixJump)
        
        # Update test index
        self.i = self.i+1
        
        return  s_val, stimIm, ID, idVecs, finalFix
     
        
    
    def getNext(self, phase, newSource, randFixJump = 0):
        
        finalFix = False
        
        
        # Set new source Training phase
        if phase=='Train':
            
            if newSource:
                self.pick = np.random.choice(self.numS)
                self.fixMan.newSource(self.trainDat[:,:,:,self.pick])
            else:
                self.fixMan.nextFixation(noiseLevel = .2) # Throw out first fixation
            
        # Set new source Testing phase
        else : # Test
            if newSource:
                self.fixMan.newSource(self.testDat[:,:,:,self.i])
                self.fixMan.nextFixation(noiseLevel = 1) # Throw out first fixation
            else:
                self.fixMan.nextFixation(noiseLevel = .2) # Throw out first fixation
        
        # Jump a random number of fixations
        if randFixJump>0:
            numFix = np.random.choice(randFixJump)
            for i in range(0,numFix):
                self.fixMan.nextFixation(noiseLevel = .2)
        
        
        # Get new fixation info
        scaleBlock = self.fixMan.getScaleBlockRez(self.returnRez, 'NoScaleMask')
        
        #print(scaleBlock.shape)
        
        # Apply a disk mask to the input
        scaleBlock, s_val = fv.applyDisk(scaleBlock)
        
        #print(scaleBlock.shape)
        
        # Create and image out of the scale block
        stimIm = fv.scaleBlock_to_img(scaleBlock)
        
        # Normalize the image to prevent network saturation
        s_val = s_val/np.linalg.norm(s_val)
        
        # Get labels
        labCourse = []
        labFine = []
        if phase=='Train':
            labCourse = self.trainLab[0][self.pick]
            labFine = self.trainLab[1][self.pick]
        else:
            labCourse = self.testLab[0][self.i]
            labFine = self.testLab[1][self.i]
        
        ID = str(labCourse)+":"+str(labFine)
        
        # Create vector representation for training
        finVec = np.zeros( (100,1))
        finVec[labFine]   = 1
        crsVec = np.zeros( (20,1))
        crsVec[labCourse] = 1
        
        if self.dataset == '10':
            crsVec = np.zeros( (10,1))
            crsVec[labCourse] = 1
        
        
        # Pack idVecs together
        idVecs = (crsVec, finVec)
        
        return  s_val, stimIm, ID, idVecs, finalFix
    
    
    def testManagager(self):
        
        for j in range(0,100):            
            s_val, stimIm, ID, idVecs, finalFix = self.getNextTrain(np.mod(j,5)==0)
            print(self.i)
            print(finalFix)
            plt.cla()
            plt.imshow(stimIm, cmap='gray')
            plt.pause(0.2)
            


"""
____________________________________________________________________CIFAR_4DEEP
The goal of this model is to see if the AWEL network will create V1 like
features and maps in response to real data. We will also be using the foveal
fixation model from Foveal.py in order to futher sample that dataset.

"""

def AWEL_Cifar_Deep(readBias = True, readRate = .1, sheetPool = True):
    
    batch = True
    batchSize = 10
      
    # Create stimulus generator
    baseRez = 50    # resolution for underlying image manipulation
    cycles = 19     # functional resolution
    rez = 20        # actually used by network
    depth = 2       # number of spatial scales
    factor = 1.65   # mangification factor of each scale
    topFov = .75    # .5 is the size of the image, 1 is twice the image size
    dataset = '10'  # whether to use the cifar10 or cifar100 dataset
    cMan = CifarFixManager(baseRez, cycles, depth, factor, rez, topFov, dataset)
    #cMan.testManagager()
    
    # This is the effective natural neighborhood created just by filtering
    width = rez/cycles
    
    s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(True)
    inSize = s_val.shape[0]
    
    # Test what images look like with these settings
    #cMan.testManagager()
    
    # Define the input to the Network
    s = tf.placeholder(tf.float32, shape = (inSize, None))
        
    f = tf.placeholder(tf.float32, shape = (100,None)) # Fine label
    c = tf.placeholder(tf.float32, shape = ( 10,None)) # 10 set labels

    numModules = 1

    # Find Layer sizes
    hidSize = np.zeros(5, dtype = int)
    outSize = np.zeros(5, dtype = int)
    
    # V1
    hidSize[0] = round(inSize  *4)
    outSize[0] = round(inSize  *3)
    # V2
    hidSize[1] = round(inSize  *4)
    outSize[1] = round(inSize  *3)
    # V3
    hidSize[2] = round(inSize  *4)
    outSize[2] = round(inSize  *3)
    # V4
    hidSize[3] = round(inSize  *2)
    outSize[3] = round(inSize  *1)
    # LOC
    hidSize[4] = round(inSize  *2)
    outSize[4] = round(inSize  *1)
    
    
    
    # Create StagedAWEL modules
    bufLength = 300
    dims = [80, 80]             # The dimesnionality of the windowed corr space/ seems to need to increase as cortical sheet size increases
    spreadHalfs  = [100, 100]   # How long it takes for the spread size to halve
    spreadMins   = [  0,  0]    # The miniimum spread value/ zero should be a default... # there may be something wrong here?
    spreadStarts = [  1,  1]    # Initial size of the spread
    cutoffs = [0, 0]            # Membership values at which to abreviate the membership matrices
    maxWinds = [25, 25]       # The maximum number of units in the windows and pools
    shapeTs = [2000, 3000]      # How long the embedding takes to go from neighbor biased to normal OBJ
    exclusiveness = True        # Whether the pools are mostly non-overlapping or not
    poolAdj = 10.0000  #.0001   # Ajustment for how exlusive the pools are
    biasUnits = False           # Whether or not to use bias units| seems to severely distort first level maps
    batchNorm = 'hidden'          # whether to batch norm the input or hidden layers of the module
    magNorm = False
    poolBySheet = sheetPool      # Whether to use AWL learning or create fixed pools based on cortical sheet location 
    lateralInh = ['DIV', 'DIV'] # What kind of local response normalization to use for hidden and pool layers
    inhWidth = [3, 3]           # Width of lateral inhibition
    sheetSpread = .1            # Width of pools/when non-exlusive and poolBysheet=True
    
    # Define arbitrarily deep awel network
    q_prev = s
    buffUpAll = 0
    # Catch the final module's variables
    h = []
    q = []
    Lh = []
    Lq = []
    W_h = []
    W_q = []
    op_all = []
    eOnlys = []
    wOnlys = []
    rOnlys = []
    
    # Loop through the module definitions
    for m in range(0, numModules):
        
        # Set up module m
        hidUp, poolUp, Lh, Lq, U,V,h,q, buffUp, W_h, W_q = awl.StagedAWEL(q_prev,  inSize,   hidSize[m], outSize[m], bufLength, dims, spreadHalfs, spreadStarts, spreadMins, cutoffs, maxWinds, shapeTs=shapeTs, exclusiveness=exclusiveness, poolAdj=poolAdj, biasUnits=biasUnits, batchNorm=batchNorm, magNorm=magNorm, poolBySheet=poolBySheet, lateralInh=lateralInh, inhWidth=inhWidth, sheetSpread=sheetSpread, batch=batch)
        inSize = outSize[m]
        q_prev = q
        
        # Set up the buffer updaters
        buffUpAll = buffUpAll+buffUp
        
        # Set the critical periods for each layer
        op_all.append(hidUp)
        eOnlys.append(2000)
        wOnlys.append(3000)
        rOnlys.append(2000)
        
        print('___________________________')
        print(' hidden'+str(m)+':'+str(hidSize[m]))
        print('   pool'+str(m)+':'+str(outSize[m]))
    
    eOnlys = np.array( eOnlys )
    wOnlys = np.array( wOnlys )
    rOnlys = np.array( rOnlys )
   
    # Define readout networks
    rate = readRate/batchSize    # .1
    decay = .5                  # .9
    momentum = .1               # 0
    bias = readBias
    pc, lc, lc_train = bm.ReadoutLayer(q, outSize[numModules-1], c,  10, False, rate, decay, momentum, bias=bias, center = True, magNorm = True)
    pf, lf, lf_train = bm.ReadoutLayer(q, outSize[numModules-1], f, 100, False, rate, decay, momentum, bias=bias, center = True, magNorm = True)
    
    
    lRateE = .5             # Learning rate for embedding
    lRateW = .1             # Learning rate for window learning
    lRateF = .5/batchSize   # Learning rate for feature learning 
    upWPeriod = 1000        # how often to recalculate W membership and indices (Very expensive for larger inputs)
    cleanUp = 200           # Period of time to only update W membership
    # Create stage clock, this automatically creates optimizers for AWEL layers
    # and chooses the correct operations to run during the training phases
    sClock = awl.StageClock( 'AWEL', op_all, eOnlys, wOnlys, rOnlys, lRateE, lRateW, lRateF, upWPeriod=upWPeriod, cleanUp=cleanUp)
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Create Basic Visiualization of h and q and s image
    sizeAdj = [.05, .05]
    plt.show()
    actPanel1 = lg.ActivityPanel(Lh, Lq, sizeAdj = sizeAdj, raster=False, rasterRez=500)
    
    # Create panel to track accuracy of readout layer
    accPanelC = lg.AccuracyPanel()
    #accPanelF = lg.AccuracyPanel()
    
    testPanelC = lg.AccuracyPanel()
    #testPanelF = lg.AccuracyPanel()
    
    # Keep track of time, so you can know how long lunch should be...
    watch = StopWatch.StopWatch()
    
    fixNum = 2   # number of fixations per stimulus
    
    t = 0
    # Fill Buffers
    for i in range(0, bufLength):
        
        # Create Stimulus
        newSource = np.mod(i,fixNum)==0
        s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain( newSource)
        c_val = idVecs[0] 
        f_val = idVecs[1]

        
        feed_dict = {s:s_val, c:c_val, f:f_val}
        
        # Run Models
        h_val, q_val, dummy = sess.run([h, q, buffUpAll], feed_dict=feed_dict)
        
    
    t_stim = 0
    mode = 'emb'
    # Train AWL model Layers
    #while (False):
    while ( not sClock.done):
        
        
        # Create stimulus
        newSource = np.mod(t,fixNum)==0
        s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(newSource)
        c_val = idVecs[0]
        f_val = idVecs[1]
        
        # Add more stimuli to batch during reconstructive phase
        if mode=='rec':
            for b in range(0, batchSize-1):                
                newSource = np.mod(t_stim,fixNum)==0
                s_val_b, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(newSource)               
                s_val = np.concatenate((s_val,s_val_b), axis=1)  # Sx1xB
                c_val = np.concatenate((c_val,idVecs[0]), axis=1)
                f_val = np.concatenate((f_val,idVecs[1]), axis=1)
                t_stim= t_stim+1
   
        
        feed_dict = {s:s_val, c:c_val, f:f_val}
        
        # Run Model
        ops_to_eval = sClock.addStageOps( [buffUpAll] )
        vals = sess.run(ops_to_eval, feed_dict=feed_dict)
        
        # Retrieve Values
        obj_val = vals[1]
        
        
        # update model ticker
        t = t+1
        sClock.tPlus1()
        
        mode = sClock.addStageOps([], returnModeStr=True,abrev=True)
        
        if mode=='emb':
            if np.mod(t, 100)==0:
                print('ln_emb('+'t='+str(sClock.t) +') = '+str(obj_val))
        
        else:
            if np.mod(t, 100)==0:
                # Update activity panel
                h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                
                im = stimIm
                actPanel1.plotActivity(im, h_val[:,0,np.newaxis], q_val[:,0,np.newaxis])
                
                print('\n\n\n_______________________________________________AWL')
                print(watch.timeStamp())
                #print('\nStimulus ID '+ID)
                print('\nln(t='+str(sClock.t)+') = '+str(obj_val)+'\n') 
                sClock.displayStageInfo()
                
                # Get current learning layer index, starts at 0,
                cLay, dum1, dum2 = sClock.getStageInfo(t)
                cMod = int(np.floor(cLay/2))                
                
                print('\ntVals:')
                print('Min(h) = '+str(np.min (h_val)))
                print('Avg(h) = '+str(np.mean(h_val)))
                print('Max(h) = '+str(np.max (h_val)))
                print('Min(q) = '+str(np.min (q_val)))
                print('Avg(q) = '+str(np.mean(q_val)))
                print('Max(q) = '+str(np.max (q_val)))
                plt.pause(.01)
    
    
    # Train Read Out Networks
    
    lc_avg = 0
    lf_avg = 0
    cPerc = 0
    fPerc = 0    
    fixNum = 3
    #for i in range(0,0):
    #for i in range(0,5000):
    for i in range(0,1000):
        
        # Get stimulus
        s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(True, randFixJump=fixNum) 
        c_val = idVecs[0] 
        f_val = idVecs[1]
        
        # Batch learning stimuli
        if batch:
            
            # only makes sense during reconstructive phase to use more
            # than one stim
            for b in range(0, batchSize-1):
                #newSource = np.mod(b+1,fixNum)==0
                s_val_b, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(True, randFixJump=fixNum) #, randFixJump=fixNum)              
                s_val = np.concatenate((s_val,s_val_b), axis=1)  # Sx1xB
                c_val = np.concatenate((c_val,idVecs[0]), axis=1)
                f_val = np.concatenate((f_val,idVecs[1]), axis=1)
                t_stim= t_stim+1
    
        
        feed_dict = {s:s_val, c:c_val, f:f_val}
            
            
        # Run Model
        #h2_val, q2_val, pc_val, pf_val, lc_val, lf_val, dumy, dumy = sess.run([h2, q2, pc, pf, lc, lf, lc_train, lf_train], feed_dict=feed_dict)
        pc_val, lc_val, dumy = sess.run([pc, lc, lc_train], feed_dict=feed_dict)
        
        # Check if model is correct
        cGuess = np.argmax(pc_val, axis=0)
        cTrue = np.argmax(np.squeeze(c_val), axis=0)
        cCorrect = np.mean(1*(cGuess==cTrue))
        
        # Running avg to calculate Percent Correct
        leak = .001
        cPerc = cPerc*(1-leak) + leak*(100)*cCorrect
        #fPerc = fPerc*(1-leak) + leak*(100)*fCorrect
        
        lc_avg = lc_avg*(1-leak) + leak*lc_val
        #lf_avg = lf_avg*.99 + .01*lf_val
        
        if np.mod(i,100)==0:
            
            h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
            
            im = stimIm
            actPanel1.plotActivity(im, h_val[:,0,np.newaxis], q_val[:,0,np.newaxis])
                
            print('\n\n\n_____________________________________________READOUT')
            print(watch.timeStamp())
            print('Stimulus ID '+str(cTrue)) #+':'+str(fTrue))
            print('   Guess ID '+str(cGuess)) #+':'+str(fGuess))
            #print('\nln(t='+str(i)+') = '+str(sigdig(lc_val,4))) #+','+str(sigdig(lf_val,4))+'\n')
            print(' Percent Correct Course ID: '+str(sigdig(cPerc,3))+'%')
            #print('   Percent Correct Fine ID: '+str(sigdig(fPerc,3))+'%')
            accPanelC.update(lc_avg, cPerc, i)
            #accPanelF.update(lf_avg, fPerc, i)
            plt.pause(.01)
    

    # Test Read Out Networks
    lc_sum = 0
    lf_sum = 0
    cTotal = 0
    fTotal = 0
    batchSize = 20
    for i in range(0,2000):
        
        # Create stimulus, look at a new object
        s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(True) 
        c_val = idVecs[0] 
        f_val = idVecs[1]
        
        # Batch multiple fixations on that object
        for b in range(0, batchSize-1):                
            s_val_b, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(False)             
            s_val = np.concatenate((s_val,s_val_b), axis=1)  # Sx1xB
            c_val = np.concatenate((c_val,idVecs[0]), axis=1)
            f_val = np.concatenate((f_val,idVecs[1]), axis=1)
            t_stim= t_stim+1
    
        feed_dict = {s:s_val, c:c_val, f:f_val}
        
        # Run Model
        pc_val, lc_val = sess.run([pc, lc], feed_dict=feed_dict)
        
        cGuessAll = np.argmax(pc_val,axis=0)
        
        pc_val = np.mean(pc_val, axis = 1)
        
        # Check if model is correct
        cGuess = np.argmax(pc_val)
        cTrue = np.argmax(c_val[:,0,0]) # Just of the first one
        cCorrect = cGuess==cTrue
        
        # Tabulate totals
        cTotal = cTotal + cCorrect
        cPerc = cTotal/(i+1)*100
        
        if np.mod(i,100)==0:
            print('\n\n\n___________________________________________FIXATION')
            print(watch.timeStamp())
            print('Stimulus ID '+str(cTrue)) #+':'+str(fTrue))
            print('   Guess ID '+str(cGuess)) #+':'+str(fGuess))
            print('  Guess ALL '+str(cGuessAll)) #+':'+str(fGuess))
            #print('\nln(t='+str(i)+') = '+str(sigdig(lc_val,4))) #+','+str(sigdig(lf_val,4))+'\n')
            print(' Percent Correct Course ID: '+str(sigdig(cPerc,3))+'%')
            #print('   Percent Correct Fine ID: '+str(sigdig(fPerc,3))+'%')
            testPanelC.update(lc_avg, cPerc, i)
            plt.pause(.01)
     
    

# Simple parameter search results
# Sheet pooling seems to do Better for classification?
# There does not seem to be a noticible difference yet for biasing the
# readout network



AWEL_Cifar_Deep()


        
"""
_______________________________________________________________________CIFAR_V1
The goal of this model is to see if the AWEL network will create V1 like
features and maps in response to real data. We will also be using the foveal
fixation model from Foveal.py in order to futher sample that dataset.

"""
# Current settings pretty cool

def AWEL_Cifar_V1():
    
    # Determine if batch learning will be used
    batch = True
    batchSize = 2
    
      
    # Create stimulus generator
    baseRez = 50    # resolution for underlying image manipulation
    cycles = 18     # functional resolution
    rez = 20        # actually used by network
    depth = 5       # number of spatial scales
    factor = 1.5    # mangification factor of each scale
    fixNum = 7      # number of fixations per stimulus
    topFov = .75    # .5 is the size of the image, 1 is twice the image size
    cMan = CifarFixManager(baseRez, cycles, depth, factor, rez, topFov, color='lum')
    
    # This is the effective natural neighborhood created just by filtering
    width = rez/cycles
    
    s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(fixNum)
    inSize = s_val.shape[0]
    
    # Test what images look like with these settings
    #cMan.testManagager()
    
    # Define the input to the Network
    s = tf.placeholder(tf.float32, shape = (inSize, None))
    
    
    # Create a StagedAWEL module
    hidSize = round(inSize*3)
    outSize = round(hidSize*.8)
    bufLength = 300
    dims = [80, 80]             # The dimesnionality of the windowed corr space/ seems to need to increase as cortical sheet size increases
    spreadHalfs  = [100, 100]   # How long it takes for the spread size to halve
    spreadMins   = [  0,  0]    # The miniimum spread value/ zero should be a default... # there may be something wrong here?
    spreadStarts = [  1,  1]    # Initial size of the spread
    cutoffs = [0, 0]            # Membership values at which to abreviate the membership matrices
    maxWinds = [25, 25]       # The maximum number of units in the windows and pools
    shapeTs = [2000, 3000]      # How long the embedding takes to go from neighbor biased to normal OBJ
    exclusiveness = True        # Whether the pools are mostly non-overlapping or not
    poolAdj = 1.0000            # Ajustment for how exlusive the pools are
    biasUnits = False           # Whether or not to use bias units| seems to severely distort first level maps
    batchNorm = 'hidden'         # whether to batch norm the input or hidden layers of the module
    poolBySheet = True         # Whether to use AWL learning or create fixed pools based on cortical sheet location 
    lateralInh = ['DIV', 'DIV'] # What kind of local response normalization to use for hidden and pool layers
    inhWidth = [3, 3]           # Width of lateral inhibition
    sheetSpread = .1             # Width of pools/when non-exlusive and poolBysheet=True
    hidUpdaters, poolUpdaters, Lh, Lq, U,V,h,q, buffUp, W_h, W_q = awl.StagedAWEL(s, inSize, hidSize, outSize, bufLength, dims, spreadHalfs, spreadStarts, spreadMins, cutoffs, maxWinds, shapeTs=shapeTs, exclusiveness=exclusiveness, poolAdj=poolAdj, biasUnits=biasUnits, batchNorm=batchNorm, poolBySheet=poolBySheet, lateralInh=lateralInh, inhWidth=inhWidth, sheetSpread=sheetSpread, batch=batch)
    
    # Print net statistics
    print('     in:'+str(inSize))
    print(' hidden:'+str(hidSize))
    print('   pool:'+str(outSize))
    
    
    # Put updaters in a list for layer clock to use
    # AWEL for both layers
    op_all, eOnlys, wOnlys, rOnlys = [],[],[],[]
    if not poolBySheet:
        op_all = [hidUpdaters,poolUpdaters]    
        eOnlys = np.array([2000, 3000])     # steps for layers to learn embedding
        wOnlys = np.array([3000, 4000])     # steps for layers to learn kohonen windows
        rOnlys = np.array([1000, 1000])     # steps for reconstructive learning
    # Sheet Pooling
    else:
        op_all = [hidUpdaters]    
        eOnlys = np.array([2000])     # steps for layers to learn embedding
        wOnlys = np.array([3000])     # steps for layers to learn kohonen windows
        rOnlys = np.array([2000])     # steps for reconstructive learning
        

    lRateE = .5                 # Learning rate for embedding
    lRateW = .1                 # Learning rate for window learning
    lRateF = .5/batchSize       #.01 # Learning rate for feature learning 
    upWPeriod = 1000            # how often to recalculate W membership and indices (Very expensive for larger inputs)
    cleanUp = 200               # Period of time to only update W membership
    # Create stage clock, this automatically creates optimizers for AWEL layers
    # and chooses the correct operations to run during the training phases
    sClock = awl.StageClock( 'AWEL', op_all, eOnlys, wOnlys, rOnlys, lRateE, lRateW, lRateF, upWPeriod=upWPeriod, cleanUp=cleanUp)
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Create Basic Visiualization of h and q and s image
    sizeAdj = [.05, .05]
    plt.show()
    actPanel = lg.ActivityPanel(Lh, Lq, sizeAdj = sizeAdj, raster=False, rasterRez=500)
    topoPanel1 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj, raster=True, rasterRez=500, accDeg=True)
    topoPanel2 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj, raster=True, rasterRez=500)
    topoPanel3 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj, raster=True, rasterRez=500)
    windPanel = lg.WindowPanel()
    
    # Keep track of time, so you can know how long lunch should be...
    watch = StopWatch.StopWatch()
    
    t = 0
    t_stim = 0
    t_buff = 0
    # Fill Buffers
    for i in range(0, bufLength):
        
        # Create Stimulus
        newSource = np.mod(i,fixNum)==0
        s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(newSource)
        
        feed_dict = {s:s_val}
        
        # Run Models
        dummy = sess.run([ buffUp], feed_dict=feed_dict)
    
    mode = 'buffInit'
    
    # Train model
    while( not sClock.done):
        
        # Create stimulus
        newSource = np.mod(t,fixNum)==0
        s_val, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(newSource)
        
        if mode=='rec':
            for b in range(0, batchSize-1):                
                newSource = np.mod(t_stim,fixNum)==0
                s_val_b, stimIm, ID, idVecs, finalFix = cMan.getNextTrain(newSource)
                s_val = np.concatenate((s_val,s_val_b), axis=1)  # Sx1xB
                t_stim= t_stim+1
            
        
        feed_dict = {s:s_val}
        
        # Run Model
        ops_to_eval = sClock.addStageOps( [buffUp] )
        vals = sess.run(ops_to_eval, feed_dict=feed_dict)
        obj_val = vals[1]
        
        # update model ticker
        t = t+1
        sClock.tPlus1()
        
        mode = sClock.addStageOps([], returnModeStr=True,abrev=True)
        
        if mode=='emb':
            if np.mod(t, 100)==0:
                print('ln_emb('+'t='+str(sClock.t) +') = '+str(obj_val))
        
        else:
            if np.mod(t, 100)==0:
                # Update activity panel
                
                # Retrieve Values
                h_val, q_val, Wh_val, Wq_val = sess.run([h, q, W_h, W_q], feed_dict=feed_dict)
                
                
                im = stimIm
                actPanel.plotActivity(im, h_val[:,0,np.newaxis], q_val[:,0,np.newaxis])
                print('\n\n\n______________________________________________V1')
                print(watch.timeStamp())
                print('\nt='+str(sClock.t)) 
                print('ln = '+str(obj_val))
                sClock.displayStageInfo()
                print('\ntVals')
                print('Min(h) = '+str(np.min(h_val)))
                print('Avg(h) = '+str(np.mean(h_val)))
                print('Max(h) = '+str(np.max(h_val)))
                print('Min(q) = '+str(np.min(q_val)))
                print('Max(q) = '+str(np.max(q_val)))
                
                plt.pause(.01)
            
            
            # Check Retonotopy
            if np.mod(t, 1000)==0:
                # Check XY Topology
                if depth == 1:
                    GXY, xVal, yVal, xIm, yIm, dum = fv.StimDogXY(rez, width)
                    H = np.zeros( (hidSize,  GXY.shape[1]) ) 
                    Q = np.zeros( (outSize,  GXY.shape[1]) )
                    for i in range(0, GXY.shape[1], 10):
                        # only calculate forward operations
                        b = min(i+10,GXY.shape[1]-1)
                        s_val = GXY[:, i:b]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        H[:, i:b] = np.squeeze(h_val)
                        Q[:, i:b] = np.squeeze(q_val)
                        
                    topoPanel1.plotTopoXY(rez, H, Q, xVal, yVal, xIm, yIm)
                # Ceck polar accentricity and degree
                else:
                    GACC, accVal, accBlock, diskBlocks, GDEG, degVal, degBlock, lineBlocks = fv.StimDogAcc(rez, depth, width, scaling = 1.5, topFov = .5)
                    accH = np.zeros( (hidSize,  GACC.shape[1]) ) 
                    accQ = np.zeros( (outSize,  GACC.shape[1]) )
                    for i in range(0, GACC.shape[1], 10):
                        # only calculate forward operations
                        b = min(i+10,GACC.shape[1]-1)
                        s_val = GACC[:, i:b]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        accH[:, i:b] = np.squeeze(h_val)
                        accQ[:, i:b] = np.squeeze(q_val)

                    degH = np.zeros( (hidSize,  GDEG.shape[1]) ) 
                    degQ = np.zeros( (outSize,  GDEG.shape[1]) )
                    for i in range(0, GDEG.shape[1], 10):
                        # only calculate forward operations
                        b = min(i+10,GDEG.shape[1]-1)
                        s_val = GDEG[:, i:b]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        degH[:, i:b] = np.squeeze(h_val)
                        degQ[:, i:b] = np.squeeze(q_val)
                        
                    topoPanel1.plotTopoAcc(rez, accH, accQ, accVal, accBlock, degH, degQ, degVal, degBlock)
                        
            # Check Oreintation sensitivity  
            if np.mod(t, 1000)==0:
                GO, oVal, oIm = fv.StimDogOrient(rez, depth, width, 2, phaseSample=10, degreeSample=60)
                H = np.zeros( (hidSize,  GO.shape[1]) ) 
                Q = np.zeros( (outSize,  GO.shape[1]) )
                for i in range(0, GO.shape[1], 10):
                    # only calculate forward operations
                    b = min(i+10,GO.shape[1]-1)
                    s_val = GO[:, i:b]
                    # buffer empty, no need for hypers
                    feed_dict = {s:s_val}
                    h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                    # activations for all stimuli...
                    H[:, i:b] = np.squeeze(h_val)
                    Q[:, i:b] = np.squeeze(q_val)
                    
                topoPanel2.plotTopoOR(rez, H, Q, oVal, oIm, circleMean=True)
                
            # Check magnification/spatial frequency sensitivity
            if np.mod(t, 1000)==0:
                GM, mVal, mBlock, checkBlocks = fv.StimDogMag(rez, depth, width, numSamples=30)
                H = np.zeros( (hidSize,  GM.shape[1]) ) 
                Q = np.zeros( (outSize,  GM.shape[1]) )
                for i in range(0, GM.shape[1], 10):
                    # only calculate forward operations
                    b = min(i+10,GM.shape[1]-1)
                    s_val = GM[:, i:b]
                    # buffer empty, no need for hypers
                    feed_dict = {s:s_val}
                    h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                    # activations for all stimuli...
                    H[:, i:b] = np.squeeze(h_val)
                    Q[:, i:b] = np.squeeze(q_val)
                    
                topoPanel3.plotTopoMag(rez, H, Q, mVal, mBlock)
                
                
            # Examine window membership matrices
            if np.mod(t, 500)==0:
                windPanel.plotHist(Wh_val, Wq_val)

#AWEL_Cifar_V1() 
        
