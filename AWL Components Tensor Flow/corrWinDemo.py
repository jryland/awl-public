# -*- coding: utf-8 -*-



import numpy as np
import ArrayPerlin as ap
import matplotlib.pyplot as plt

def demo():
    
    sampleNum = 800
    rez = 50
    Sim = ap.TestPerl2(rez)
    
    s_buff = np.zeros((Sim.shape[0]*Sim.shape[1], sampleNum))
    
    for i in range(0,100):
        Sim = ap.TestPerl2(rez)
        s_buff[:,i] = Sim.flatten(order='F')
    
    poi = round(rez*rez/2)+round(rez/2)
    svec = Sim.flatten(order='F')
    svec[poi] = 1.1
        
    s_buff = s_buff-np.mean(s_buff,axis=1)[:,np.newaxis]
    s_buff = s_buff/np.linalg.norm(s_buff, axis=1)[:,np.newaxis]

    R2 = np.power(np.cov(s_buff),2)
    
    poiWind = R2[poi,:]
    
    pIm = np.reshape(svec, Sim.shape, order='F')
    wIm = np.reshape(poiWind, Sim.shape, order='F')
    
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax1.imshow(pIm)
    ax1.axis('off')
    ax1.set_title('Neuron in image stream')
    ax2 = fig.add_subplot(122)
    ax2.imshow(wIm)
    ax2.axis('off')
    ax2.set_title('Squared Correlation Window')
    
    
demo()
    