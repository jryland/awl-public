# -*- coding: utf-8 -*-
"""
Created on Mon Aug  1 18:20:27 2016

@author: james

This list of function will deal with basic number formatting.

Implemented
-significant digits

Soon to be implemented
-Standards: Billion, Trillion, Million, K,
-can build off of sig digits

"""

import math
def sigdig(x, n): 
    
    x = float(x)
    n = int(n)    
    
    if x!=0:
        return round(x, int(n - math.ceil(math.log10(abs(x))))) 
    else:
        return 0