#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 17:23:48 2017

@author: james
"""

import scipy as sp
from scipy import ndimage as nd
from scipy import interpolate as inter
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt
import time   

def test():
    
    #testIm = misc.imread('Test_img_Lion.jpg')
    testIm = misc.imread('Test_img_People.jpg')
    testIm = testIm[:,:,0]  # just grab one channel 
    testIm = nd.zoom(testIm, .50) # Lowering the initial reduces the time for making the interp object a lot...
    
    ratio = testIm.shape[1]/testIm.shape[0]
    

    xP = np.linspace(-.5, .5, testIm.shape[0])
    yP =  np.linspace(-.5, .5, testIm.shape[1]) * ratio
    XP, YP = np.meshgrid(yP, xP)
    
    # Center
    xc = .25
    yc = 0
    
    
    print(xP.shape)
    print(yP.shape)
    
    # Making this object is fairly expensive... may need to scale image down
    # Some initially
    imInterFun = inter.RectBivariateSpline( xP, yP, testIm)
    
    scRez = 100
    xnew = np.linspace(-.5, .5, scRez)
    ynew = np.linspace(-.5, .5, scRez)
    
    # make multi-scale sample
    Xnew, Ynew = np.meshgrid(xnew, ynew)
    Xpyr = Xnew[:,:,np.newaxis]
    Ypyr = Ynew[:,:,np.newaxis]
    
    scaleFact = 1.5
    csf = 1
    for sc in range(0,5-1):
        csf = csf*scaleFact
        Xpyr = np.concatenate([ Xpyr,Xnew[:,:,np.newaxis]/csf], axis=2)
        Ypyr = np.concatenate([ Ypyr,Ynew[:,:,np.newaxis]/csf], axis=2)
        
    
    # Estimate how long it takes to do get a sample at a resolution
    start = time.time()
    
    imInterEv = []
    for i in range(0,10):
        imInterFun = inter.RectBivariateSpline( xP, yP, testIm)
        imPyr = imInterFun.ev(Xpyr, Ypyr)
    
    imInterFun = inter.RectBivariateSpline( xP, yP, testIm)
    imPyr = imInterFun.ev(Xpyr+yc, Ypyr+xc)    
    
    stop = time.time()
    interval = (stop-start)/10
    
    print('Sample Computation Time: '+ str(interval)+'secs')
    
    multiSc = np.reshape(imPyr, [imPyr.shape[0], imPyr.shape[1]*imPyr.shape[2] ], order='F') 
    
    # Graphics     
    plt.ion()            
    fig = plt.figure()   
    
    ax1 = fig.add_subplot(131)
    ax1.imshow(testIm, cmap=plt.cm.gray)          
    ax1.axis('off')    
    
    ax2 = fig.add_subplot(132)
    ax2.imshow(np.transpose(multiSc), cmap=plt.cm.gray)          
    ax2.axis('off')
    
    
    
    # Test t se if we can project pyramid slices back onto an image
    
    blankIm = np.zeros_like(testIm)
    pickSc = 2
    xsc = Xpyr[0,:,pickSc]
    ysc = Ypyr[:,0,pickSc]
    scIm = imPyr[:,:,pickSc]
    # Zero out the edges    
    scIm[0,:] = 0
    scIm[:,0] = 0
    scIm[(scRez-1),:] = 0
    scIm[:,(scRez-1)] = 0
    pyrInterFun = inter.RectBivariateSpline( xsc+xc, ysc+yc, imPyr[:,:, pickSc])    
    
    print(XP.shape)
    print(YP.shape)
    

    blankIm = blankIm + pyrInterFun.ev(XP, YP)
    
    
    ax3 = fig.add_subplot(133)
    ax3.imshow(blankIm, cmap=plt.cm.gray)          
    ax3.axis('off')
    
    a =np.array([[1,2,3],[4,6,5]])
    
    maxInd = np.argmax(a.flatten(order='C'))    
    
    inds = np.unravel_index(maxInd, a.shape, order='C')    
    
    print(a[inds])
    
test()