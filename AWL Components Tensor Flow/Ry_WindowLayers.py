# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 00:42:07 2017

@author: James Ryland


WINDOWED LAYERS
        ________________STATIC LAYERS
        static_windowed_inhibit
        static_windowed_pool

        ________________________SHORT
        addDoubleShort          <- this creates window indexing matrices usefull for sparse/gpu accelerated layer calculations
        addDoubleShortUpdate    <- creates updators for double short
        addInputShort           <- creates input for use with mini weights
        addWeightsShort         <- creates abreviated weight matrix
        addRainShort            <- creates Rain Short and its updater
        _____________SHORT LAYER DEFS
        short_windowed_hidden
        short_windowed_pool
        short_windowed_inhibit
        _________________________MINI
        AddWindowCalc           <- this creates window indexing matrices usefull for sparse/gpu accelerated layer calculations
        AddWeightsMini          <- creates abreviated weight matrix
        AddInputsMini           <- creates input for use with mini weights
        ______________MINI LAYER DEFS
        sparse_windowed_hidden
        sparse_windowed_pool
        _________________________FULL        
        windowed_softpool
        windowed_softplus

"""

"""
__________________________________________________________________STATIC_LAYERS
This is a set of functions that define statically windowed layers. The windows
for these layers do not need to be changed and thus will be left out of the 
function graphs in tensor flow. Because of this most of the calculations for
defining the windows will be calculated here, but at least they will only be
calculated once.

varaibles:
    s       The input pattern
    X       The positions of each neuron in a fixed space (usually cortical sheet)
    keep    The maximum size of the windows
    width   The width in neurons of the dog filter 

    beta    The relative stretch of the negative gaussian

"""

def static_windowed_inhibit( s, inSize, X, keep, width=.5, lrnType = 'DIV', batch= False):
    
    # Estimate lattice distance, Assume disk of radius .5
    latDist = 1/inSize
    
    # Create row abreviated distance matrix very similar to mini method
    rD = np.zeros( (X.shape[0], keep) )
    rInds = np.zeros( (X.shape[0], keep), dtype=int)
    for i in range(0,rD.shape[0]):
        
        # Find the parwise distances
        rDrow = np.squeeze( np.sum( np.power(X[i,:]-X, 2), axis=1 ) )
        
        rIndsRow = np.argsort(rDrow, axis=0)
        
        # Sort the distance values
        rDrow = rDrow[rIndsRow] 
        
        rD[i, :] = rDrow[0:keep]
        rInds[i, :] = rIndsRow[0:keep]
        
    # Difference of Gaussain approx
    beta = 2
    beta2 = np.power(2, 2)
    cortD2 = np.power(rD*latDist/width,2)
    
    
    # Map inputs values to windows
    s_mini = tf.gather(s, rInds)
    
    
    sInh = []
    

    # DOG local contrast enhancement
    if lrnType == 'DOG':
        Inh_mini = np.exp(-cortD2)-np.exp(-cortD2/beta2)/beta
        sInh = tf.reduce_sum( Inh_mini[:,:,tf.newaxis]*s_mini, axis=1)
        sInh = sInh/inSize        
        
    # Just applies gaussian smoothing
    elif lrnType == 'GAU':
        w_mini = np.exp(-cortD2)
        sInh = tf.reduce_sum( w_mini[:,:,tf.newaxis]*s_mini, axis=1)
        sInh = sInh/inSize
        
    # Divisive Contrast normalization
    elif lrnType == 'DIV':
        w_mini = np.exp(-cortD2)
        sInh = s/tf.reduce_sum( 2.0+tf.abs(w_mini[:,:,tf.newaxis]*s_mini), axis=1)
            
    return sInh

# Non-exclusive pools for now...
# But can use a trick similar to the rain short matrix or 
# actual sparse matrices to solve this...
def static_windowed_pool(h, hidSize, outSize, X, Y, keep, width=1, batch=False):
    
    # Estimate lattice distance, Assume disk of radius .5
    latDist = 1/hidSize
    
    #___________________________________________________________________FORWARD
    # Create row abreviated distance matrix very similar to mini method
    rD = np.zeros( (Y.shape[0], keep) )
    rInds = np.zeros( (Y.shape[0], keep), dtype=int)
    for i in range(0,rD.shape[0]):
        
        # Find the parwise distances
        rDrow = np.squeeze( np.sum( np.power(Y[i,:]-X, 2), axis=1 ) )
        
        rIndsRow = np.argsort(rDrow, axis=0)
        
        # Sort the distance values
        rDrow = rDrow[rIndsRow] 
        
        rD[i, :] = rDrow[0:keep]
        rInds[i, :] = rIndsRow[0:keep]
        
    
    W_mini = np.exp(-np.power((rD/latDist)/width,2)) 
    #h_mini = tf.gather(tf.squeeze(h), tf.squeeze(rInds) )
    
    h_mini = tf.gather(h, rInds )
    
    q = []
    
    # Does not work with none-dimensions
    #q = tf.reduce_sum(tf.nn.softmax(W_mini*h_mini, 1)*h_mini, axis=1, keep_dims=True) #[:,tf.newaxis]

    # Working softmax with none-dimensions
    # Exact same result, may be slightly less efficient
    Wh = W_mini[:,:,tf.newaxis]*h_mini    
    eWh = tf.exp(Wh-tf.reduce_min(Wh, axis=1,keep_dims=True))
    qMax = eWh/tf.reduce_sum(eWh, axis=1, keep_dims=True)
    q = tf.reduce_sum(qMax*h_mini, axis=1)

    return q



def static_windowed_pool_rec(q, hidSize, outSize, X, Y, keep, width=1, batch=False):

    # Estimate lattice distance, Assume disk of radius .5
    latDist = 1/outSize
    
    #_______________________________________________________________RECONSTRUCT
    # Create Column abreviated matrix for use with mini method
    # done using a row representation
    cD = np.zeros( (X.shape[0], keep) )
    cInds = np.zeros( (X.shape[0], keep), dtype=int)
    for i in range(0,cD.shape[0]):
        
        # Find the parwise distances
        cDrow = np.squeeze( np.sum( np.power(X[i,:]-Y, 2), axis=1 ) )
        
        cIndsRow = np.argsort(cDrow, axis=0)
        
        # Sort the distance values
        cDrow = cDrow[cIndsRow] 
        
        cD[i, :] = cDrow[0:keep]
        cInds[i, :] = cIndsRow[0:keep]
    
    WT_mini = np.exp(-np.power(cD/latDist*width,2)) 
    
    q_mini = tf.gather(q, cInds )
    
    h_prime = tf.reduce_sum( WT_mini[:,:,tf.newaxis]*q_mini, axis=1)
        
    return h_prime

"""
_________________________________________________________________________SHORT
This is a set of functions for creating and working with DoubleShort matrices.
these matrices are never manipulated as whole non-sparse matrices, in a similar
manner to the mini-matrix method the top elements of these matrices are sorted
in a row-wise fashion then a column-wise fashion.  The indices needed to
place the kept elements back into their original positions will also be kept. 
Only the top K elements along rows and columns will be stored, meaning use of 
this matrix will only approximate use of the original matrix, however with 
proper setting of K this will make little difference as these matrices are
expected to be highly and evenly sparse. All updates will work in this format.
Because of this double_short format, matrices will take up (n*k)+(m*k) elements 
instead of n*m elements. One day the TensorFlow people will implement decent
sparse matrix variables and functionality and this will less nescessary, but
formats like this are actually Ideal for receptive window representation anyway
.

when n and m are large integers and k is small:    (n*k)+(m*k) <<< n*m   


Sort Groups of rows of M
Keep top K
    ________
   / /     /  M-full 
  /#/<----/update/sort
 /#/<----/update/sort
/_/_____/
    __
   / /  M with shortened rows + inds
  / /  
 / /  
/_/


Sort Groups of cols of M
Keep top K

    ________
   /___##__/  M-full 
  /   /\  / 
 /   //  /  
/___//__/
   up-sort
 ________
|_______|   M with shortened columns + inds


NOTE: 
    The penalty for all of this nice sparsity is the fact that the row-wise 
    representation of the matrix will become desyncronized from the col-wise
    representation if the source of the full row/col representation is changing
    during the update cycle. It is recommended that you let your algorithm
    perform a cleanup-phase where the update cycle is allowed to run while the
    source does not change in order to bring the row/col reps in line.

NOTE:
    The column-wise representation will be transposed in order to facilitate 
    simple sliced indexing and scatter updates.
    
_____________________________________________________________________RAINMATRIX
Unfortunately, double short matrices cannot have column-wise
functions like soft_max applied to them and have the results appear in the
row-wise short representation and vica-versa. This presents a problem as column
dependent functions are needed to create mutually exclusive pools. The Rain
Short matrix addresses this issue. For this format a column-short
representation will be provided as a source, and only row-wise updates will be
applied as is typically needed. 

      step1:________
           /_#___#_/   P col-short provided
            /   /
           /   /
          /   /
        _/___/__ P-Full    __  step3:
      /\/__\/__/__________/_/    Normal short row update
sort-/_#___#__/__________/#/     with provided full-row
    /        /          / /
   /________/          /_/
    step2:
        Recreate row
        of full P

A block of rows will be selected to update, then the column shortened
representation will use scatter_nd to rain values into a row collector variable
. Importantly through index trickery only the values that belong in the rows to
be updated will be collected in the row collection variable. The values in the
row collector variable will then be sorted and cropped in order to update the 
row shortened representation of the matrix.

"""

import tensorflow as tf
import numpy as np
import Ry_BasicMathTF  as bm


def addDoubleShort( numRows, numCols, keep, upNum):
    
    # Define short row part of matrix
    M_shortR = tf.Variable( tf.zeros( (numRows, keep) ))
    M_indsR =  tf.Variable(tf.zeros( (numRows, keep), dtype=tf.int32)) # <-actually refers to the column indices of the shortRow matrix
    
    # Define short column part of matrix
    M_shortC = tf.Variable( tf.zeros( (numCols, keep) ))
    M_indsC =  tf.Variable( tf.zeros( (numCols, keep), dtype=tf.int32)) # <-actually refers to the row indices of the shortCol matrix
    
    # Define update cycle
    inds = tf.Variable(np.arange(upNum)[:,np.newaxis], dtype=tf.int32)
    inds_up = tf.assign(inds, inds+upNum )
    cRows = tf.to_int32(tf.mod( inds_up, numRows ))
    cCols = tf.to_int32(tf.mod( inds_up, numCols ))
    
    # Pack into list
    M_short = [M_shortR, M_indsR, M_shortC, M_indsC, cRows, cCols, keep]
    
    return M_short, cRows, cCols

def addTransposeShort(M_short):
    MT_short = [[],[],[],[],[],[]]
    MT_short[0] = M_short[2]
    MT_short[1] = M_short[3]
    MT_short[2] = M_short[0]
    MT_short[3] = M_short[1]
    MT_short[4] = M_short[5]
    MT_short[5] = M_short[4]
    
    return MT_short

def addShortInputs(M_short, s):
    M_indsR = M_short[1]
    S_shortR = tf.gather(s, M_indsR)
    return S_shortR 
    # May not work with batch
    #return S_shortR

def addShortWeights(M_short):
    return M_short[0]

# Return full versions of the current rows and columns to be used for 
# Averaging procedures
def getDoubleShortFull(M_short):
    # Unptack M_short
    M_shortR = M_short[0]
    M_indsR  = M_short[1] # <-actually refers to the column indices of the shortRow matrix
    M_shortC = M_short[2]
    M_indsC  = M_short[3] # <-actually refers to the row indices of the shortCol matrix
    cRows    = M_short[4]
    cCols    = M_short[5]
    keep     = M_short[6]
    
    fullR_shape = [cRows.get_shape().as_list()[0], M_shortC.get_shape().as_list()[0]]
    fullC_shape = [cCols.get_shape().as_list()[0], M_shortR.get_shape().as_list()[0]]
    
    # Gather M_short into full rows and columns for currently updating rows/cols
    indsR, dum = np.mgrid[0:(fullR_shape[0]),0:keep]    
    M_indsR_part = tf.gather(M_indsR, tf.squeeze(cRows))
    M_shortR_part = tf.gather(M_shortR, tf.squeeze(cRows))
    
    fullIndsR = tf.concat([tf.to_int32(indsR)[:,:,tf.newaxis], M_indsR_part[:,:,tf.newaxis]], axis=2)
    M_fullR = tf.scatter_nd(fullIndsR, M_shortR_part, fullR_shape)
    
    indsC, dum = np.mgrid[0:(fullC_shape[0]),0:keep]
    M_indsC_part = tf.gather(M_indsC, tf.squeeze(cCols) )
    M_shortC_part = tf.gather(M_shortC, tf.squeeze(cCols) )
    
    fullIndsC = tf.concat([tf.to_int32(indsC)[:,:,tf.newaxis], M_indsC_part[:,:,tf.newaxis],], axis=2)
    M_fullC_T = tf.scatter_nd(fullIndsC, M_shortC_part, fullC_shape)
    M_fullC   = tf.transpose(M_fullC_T) # Needed un-transpose columnwise rep
     
    return M_fullR, M_fullC

def addDoubleShortUpdate(M_short, rowsNew, colsNew):
    
    # Needed in order to update transposed col-rep
    colsNew = tf.transpose(colsNew)
    
    M_shortR = M_short[0]
    M_indsR  = M_short[1] # <-actually refers to the column indices of the shortRow matrix
    M_shortC = M_short[2]
    M_indsC  = M_short[3] # <-actually refers to the row indices of the shortCol matrix
    cRows    = M_short[4]
    cCols    = M_short[5]
    keep     = M_short[6]
    
    # update rows
    rTop, rInds = tf.nn.top_k(rowsNew,keep) 
    M_shortR_up = tf.scatter_nd_update( M_shortR, cRows, rTop )
    M_indsR_up  = tf.scatter_nd_update( M_indsR, cRows, rInds )
    
    cTop, cInds = tf.nn.top_k(colsNew,keep) 
    M_shortC_up = tf.scatter_nd_update( M_shortC, cCols, cTop )
    M_indsC_up  = tf.scatter_nd_update( M_indsC, cCols, cInds )
        
    M_short_up = M_shortR_up[0,0] + tf.to_float(M_indsR_up[0,0]) + M_shortC_up[0,0] + tf.to_float(M_indsC_up[0,0])
    
    return M_short_up

# usefull for applying columnwise function to existing DoubleShort in order
# to feed a RainShort matrix
def getShortCol(M_short):
    return M_short[2], M_short[3]

# Create a rain short matrix
def addRainShort( R_shortC, R_indsC, numRows, numCols, upNum ):
    'Labla'
    keep = R_shortC.get_shape().as_list()[1]

    # Define short row part of matrix
    R_shortR = tf.Variable( tf.zeros( (numRows, keep) ))
    R_indsR =  tf.Variable(tf.zeros( (numRows, keep), dtype=tf.int32),dtype=tf.int32) # <-actually refers to the column indices of the shortRow matrix
    
    # Define update cycle
    # This will be special as we want the row index to go outside the matrix
    # This is to avoid non-increasing row-indeces....
    # This will introduce some wasted-calculations
    startInd = tf.Variable(0, dtype=tf.int32)
    startInd_up = tf.assign(startInd, startInd+upNum)    
    startInd_mod = tf.mod(startInd, numRows)
    cRows = tf.to_int32(startInd_mod+np.arange(upNum)[:,np.newaxis])
    
    # Pack into list very similar structure to M_short
    # except that cCols is left out
    R_rain = [R_shortR, R_indsR, R_shortC, R_indsC, cRows, [], keep]

    # For the rest of this we need to un-transpose the column representation
    R_shortC = tf.transpose(R_shortC) 
    R_indsC  = tf.transpose(R_indsC)
    
    # Define the bounds the current row-slice to update
    minRowInd = tf.reduce_min(cRows) 
    maxRowInd = tf.reduce_max(cRows)
    cropInd = maxRowInd+1
    
    # Push elements outside this range to an extra row to be clipped
    goodInd = tf.to_int32(R_indsC>=minRowInd) * tf.to_int32(R_indsC<=maxRowInd)
    badInd =  1-tf.to_int32(goodInd)
    indsC = R_indsC* tf.to_int32( goodInd )+cropInd*tf.to_int32( badInd )
    
    # Line up with full-row
    indsC = indsC-minRowInd

    dum, inds_base = np.mgrid[0:keep,0:numCols]
    inds = tf.concat([indsC[:,:,tf.newaxis], inds_base[:,:,tf.newaxis]], axis=2) 
    
    fullRow = tf.scatter_nd(inds, R_shortC, (upNum+1,numCols))
    rowsNew = fullRow[0:upNum,:]
    
    # update rows
    rTop, rInds = tf.nn.top_k(rowsNew,keep) 
    # Take care of going out of bounds
    clip = upNum+tf.to_int32( tf.minimum(numRows-1-maxRowInd, 0 ) )
    
    R_shortR_up = tf.scatter_nd_update( R_shortR, cRows[0:clip,:], rTop[0:clip,:]  )
    R_indsR_up  = tf.scatter_nd_update( R_indsR,  cRows[0:clip,:], rInds[0:clip,:] )
    
    # Merge updating triggers for current indices and the block updates themselves
    R_up = R_shortR_up[0,0]+tf.to_float(R_indsR_up[0,0])+tf.to_float(startInd_up)
    
    return R_rain, R_up 
    
 
    
"""
________________________________________________________SHORT LAYER DEFINITIONS

Function Hiarchies:     <- This is the basic pattern for all of these layers 

    
Forward:
            q
           / \
          h  [P]
         / \
        s  [Y*F]

Backward:
              l_rec1
             /
            s_prime
           / \
          h  [YT*U]
         / \
        s  [Y*F]
        
              l_rec2
             /
            h_prime
           / \
          q  [YT*V]
         / \
        h  [Y*P]

Hidden Layer Relavant Variables:
Y       Window/Pool Membership matrix
s       input to hidden layer
h       hidden layer activity
F       Feature weight matrix
U       Reconstructive weight matrix       

b/p     bias units disabled currently

Pool Layer Relavant Variables:
Y       raw Window/Pool Membership matrix
P       Exclusive Membership matrix
h       input to pool layer
q       pool layer activity
P       mutually exclusive pool matrix
U       Reconstructive weight matrix       


short variables
    short varaibles are special re-indexed versions of variables that allow GPU
accelerated multiplication of vectors by sparse/row&col-abridged matrices. This
greatly accelerates how fast layer activations and gradients can be computed. 
Additionally, the short format does not require the storage of the full window
membership matrices at all. See the section outlining the DoubleShort and
RainShort formats for further explaination. Functionally, however this 
representation is used to approximate normal matrix multiplication by vectors.
"""

def short_windowed_hidden(Y_short, s, hidSize, inSize, biasUnits=False, batch=False):

    Yshape = [hidSize, inSize]
    keep = Y_short[6]    
    
    # create full feature weight matrix
    F_shortR = tf.Variable(tf.random_normal([hidSize, keep]))
    
    # Create forward short matrices
    Y_shortR = addShortWeights(Y_short)
    s_shortR = addShortInputs(Y_short, s)
    
    # Compute the linear transformation
    r = []
    YF = tf.stop_gradient(Y_shortR)*F_shortR
    r = tf.reduce_sum( YF[:,:,tf.newaxis]*s_shortR, axis=1)
    
    # Add bias units
    b = []
    if biasUnits:
        b = tf.Variable(tf.zeros([hidSize,1]), name='b') 
        r = r+b
    
    # Compute hidden layer activities
    h = tf.nn.relu(r, name = 'h')
    
    return F_shortR, h
    
        
def short_windowed_hidden_rec(Y_short, h, hidSize, inSize, biasUnits=False, batch=False):
    
    Yshape = [hidSize, inSize]  
    keep = Y_short[6] 
    
    # Create backwards weight matrix
    YT_short = addTransposeShort(Y_short)
    U_shortR = tf.Variable(tf.random_normal([inSize,keep]))
    
    # Create backward short matrices
    YT_shortR = addShortWeights(YT_short)
    h_shortR  = addShortInputs(YT_short, h)
    
    # Calculate reconstructed input
    s_prime = []
    YTU = tf.stop_gradient(YT_shortR)*U_shortR
    s_prime = tf.reduce_sum( YTU[:,:,tf.newaxis]*h_shortR, axis=1)
        
    # Add bias units
    p = []
    if biasUnits:    
        p = tf.Variable(tf.zeros([inSize,1]), name='p')
        s_prime = s_prime+p
    
    return U_shortR, s_prime


def short_windowed_pool(Y_short, h, outSize, hidSize, upNum, poolExclusive=True, poolAdj=1, batch=False):
    
    # should reconstructive gradient adjust the pools
    # with the double short representation weights are
    # not stored for connection outside the top k,
    # meaning that updating the pools will shuffle the
    # the weights around destroying any learned features
    
    Yshape = [outSize, hidSize]
    keep = Y_short[6]     
    
    P = []
    P_up = []
    if poolExclusive:
        Y_shortC, Y_indsC = getShortCol(Y_short)
        P_shortC = tf.nn.softmax(Y_shortC, -1) # Remember that the col shortened rep is still stored rowise
        P_short, P_up = addRainShort(P_shortC, Y_indsC, outSize, hidSize, upNum )    
    else:
        P_short = Y_short
    
    # Create forward mini matrices
    P_shortR = addShortWeights(P_short)
    h_shortR = addShortInputs(P_short, h)
    
    # Compute pooling layer activities
    P_shortR = tf.stop_gradient(P_shortR)
    
    PHmax = []
    # Does not work with well with gathers and none-dimensions
    #PH = tf.multiply(P_shortR ,h_shortR*keep)
    #PHmax = tf.nn.softmax(PH, dim = 1) 
    
    # Working softmax with none-dimensions
    # Exact same result, may be slightly less efficient 
    PH = tf.multiply(P_shortR[:,:,tf.newaxis] ,h_shortR*keep)
    ePH = tf.exp(PH-tf.reduce_min(PH, axis=1,keep_dims=True))
    PHmax = ePH/tf.reduce_sum(ePH, axis=1, keep_dims=True)

    q = tf.reduce_sum(PHmax * h_shortR, axis = 1)
    
    return P_up, P_shortR, q


def short_windowed_pool_rec(Y_short, q, outSize, hidSize, upNum, poolExclusive=True, poolAdj=1, batch=False):
    
    keep = Y_short[6]     
    
    # create backward weight matrix
    V_shortR = tf.Variable(tf.random_normal([hidSize,keep]))
    
    # Create backward mini matrices
    YT_short = addTransposeShort(Y_short)
    YT_shortR = addShortWeights(YT_short)
    q_shortR = addShortInputs(YT_short, q)
    
    # Reconstructive Objective
    YT_shortR = tf.stop_gradient(YT_shortR)
    h_prime = []
    
    YTV = YT_shortR*V_shortR
    h_prime = tf.reduce_sum( YTV[:,:,tf.newaxis]*q_shortR, axis=1)

    return V_shortR, h_prime


def DoubleShort_Test():
    
    W_val = np.random.randint(0, 9, size = (4, 3))
    
    W = tf.Variable(W_val, dtype = tf.float32)
    
    numRows = W.shape.as_list()[0]
    numCols = W.shape.as_list()[1]
    keep = 2
    upNum = 3
    W_short, cRows, cCols = addDoubleShort(numRows, numCols, keep, upNum)
    
    rowNew = tf.gather_nd(W, cRows)
    colNew = tf.transpose( tf.gather_nd(tf.transpose(W), cCols) )

    W_short_up = addDoubleShortUpdate(W_short, rowNew, colNew)
    
    W_fullR, W_fullC = getDoubleShortFull(W_short)
    
    s_val = np.arange(6)[:,np.newaxis]    
    
    s = tf.Variable(np.arange(6)[:,np.newaxis])    
    
    S_shortR = addShortInputs(W_short, s)    
    
    # Remember that these are always on their side
    W_shortC, W_indsC = getShortCol( W_short)
    
    P_shortC = tf.nn.softmax(W_shortC, -1)
    
    P_rain, P_up = addRainShort(P_shortC, W_indsC, numRows, numCols, upNum)
    
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Fill W_short
    for i in range(0, 10):
        sess.run(W_short_up)

    W_shortR = W_short[0]
    W_shortC = W_short[2]
    W_shortR_val, W_shortC_val, W_fullR_val, W_fullC_val, S_shortR_val = sess.run([W_shortR, W_shortC, W_fullR, W_fullC, S_shortR])
    
    P_rows = P_rain[4]
    
    for i in range(0, 10):
        dum , P_rows_val = sess.run([P_up, P_rows])
        #print('\n')
        #print(P_rows_val)
    
    P_shortR = P_rain[0]
    
    P_shortR_val, P_shortC_val, W_indsC_val = sess.run([P_shortR,  P_shortC, W_indsC])
    
    P_shortC_val = np.transpose(P_shortC_val)
    W_indsC_val = np.transpose(W_indsC_val)
    
    
    print('\n__________________________________________________________________')
    print('\nW:')
    print(W_val)
    print('\nS_shortR:')
    print(S_shortR_val)
    print('\nW row shortened:')
    print(W_shortR_val)
    print('\nW col shortened:')
    print(W_shortC_val)
    print('\nW current sub rows full:')
    print(W_fullR_val)
    print('\nW current sub cols full:')
    print(W_fullC_val)

    print('\n__________________________________________________________________')
    print('\nW:')
    print(W_val)
    print('\nP_shortC')
    print(P_shortC_val)
    print('\nP_indsC')
    print(W_indsC_val)
    print('\nP_shortR')
    print(P_shortR_val)
    

#DoubleShort_Test()


def IndexingBatch_Test():
    
    # Batch shape Stimx1xBatch
    s = tf.placeholder(dtype=tf.float32, shape=(5, None))
    
    winds = tf.Variable(np.array([[0,1],[2,3]]),dtype=tf.int32)
    
    S_mini = tf.gather(s, winds)
    #S_mini = tf.squeeze(S_mini)

    W_mini = tf.Variable(np.array([[2,2],[2,2]]), dtype=tf.float32)

    print(W_mini.shape)
    print(S_mini.shape)
    
    # This is all we need to do to make it work YAY !!!!
    # This is practically nothing!!!
    # It may even work fine with the more traditional batch style :)
    r = tf.reduce_sum( W_mini*S_mini, axis=1)
     
    S2 = tf.pow(S_mini,2)
    
    # Working softmax    
    eS2 = tf.exp(S2-tf.reduce_min(S_mini, axis=1,keep_dims=True))
    r_maxA = eS2/tf.reduce_sum(eS2, axis=1, keep_dims=True)

    r_maxB = tf.nn.softmax(tf.transpose(S2,[0,2,1]))
    r_maxB = tf.transpose(r_maxB, [0,2,1])

    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)

    s_val = np.array([[0, 1, 2, 3, 4],[0, 1, 2, 3, 4]])
    s_val = np.transpose( s_val, [1, 0] )
    
    print('\nS batch')
    print(s_val)
    
    S_mini_val, r_val, r_maxA_val, r_maxB_val = sess.run([S_mini, r, r_maxA, r_maxB], feed_dict={s:s_val})
    
    print(S_mini_val.shape)
    
    print('\nRe-indexed S batch')
    print(S_mini_val[:,:,0])
    print(S_mini_val[:,:,1])
    
    print('\nr batch')
    print(r_val)
    
    print('\nr batch r_maxA')
    print(r_maxA_val[:,:,0])
    print(r_maxA_val[:,:,1])
    
    #print('\nr batch r_maxB')
    #print(r_maxB_val[:,:,0])
    #print(r_maxB_val[:,:,1])
    
#IndexingBatch_Test()


"""
__________________________________________________________________________MINI
This format requires the storage a full weight matrix between layers, but when
activations are computed they will be found using multiplication against
abreviated versions of the weight matrices.

Sort Groups of rows of M
Keep top K
    ________
   /#/<----/  M-full 
  /#/<----/
 /#/<----/   
/#/_____/ 
Sort all rows to create:
    __
   / /   M_mini
  / /  
 / /  
/_/

and
    __
   / /  M_inds
  / /  
 / /  
/_/

Which can be used to quickly approximate large matrix multiplication when M
has the expected sparsity of an appropriate receptive window matrix. Further,
this implementaion uses an dadaptive size for how many elements to keep in the
abreviated matrices based on a preset cutoff.

"""

# ITS ALIVE!!!!!! LOL!!!
# Adds an op that assigns window indices to the tf op tree
# When fed W transpose it can calculate approximate inverse inds
def AddCalcWindows( W, Wshape, keepNum, cutoff):
    
    
    # Get sorted indeces
    W_sort, Winds = tf.nn.top_k(W, keepNum, True, name='W_sort')
    
    # Calcualate how many to keep
    # essentially what is the maximum window size
    # given the cutoff
    windSize_cal = tf.maximum(tf.to_int32( tf.reduce_sum( tf.to_float( tf.reduce_max( W_sort , axis=0)>cutoff ) ), name='windSize_cal' ), 10)
    
    # build Weight Indexer [rowNum, Wind], for use with gather!
    rowInd_val, dummy = np.mgrid[0:Wshape[0], 0:keepNum]
    
    # Weight indexer is just full inds tensor with gather_nd
    # Input Indexer is just the squeeze(inds[:,:,1]) with gather
    inds_full_cal = tf.concat( [rowInd_val[:,:,np.newaxis], Winds[:,:,tf.newaxis]], axis=2, name='inds_full_cal')
    
    # Create a matrix to store all of the ordered indeces
    # This is what will be stored each update
    inds_full = tf.Variable( tf.zeros( [Wshape[0], keepNum, 2] , dtype=tf.int32), dtype=tf.int32, name='inds_full')
    inds_full_asgn = tf.assign(inds_full, inds_full_cal, name='inds_full_asgn')
    
    # also store the largest window size
    windSize = tf.Variable(keepNum, dtype=tf.int32, name='windSize')
    windSize_asgn = tf.assign(windSize, windSize_cal, name='windsSize_asgn')
    
    # Convenience op that updates windSize and inds_full
    all_asgn = windSize_asgn + tf.reduce_sum(inds_full_asgn)
    
    # Use windSize to crop the index matrix to the appropriate window size!!
    inds = tf.to_int32( inds_full[:, 0:windSize, :])
    
    return inds, all_asgn, windSize

# Convenience function for easily computing mini weight matrix 
def addMiniWeights(V, winds):
    V_mini = tf.gather_nd(V, winds)
    return V_mini

# Convenience function for easily computing mini input matrix
# for operations with mini-weight matrices
def addMiniInputs(s, winds):
    s_mini = tf.gather(tf.squeeze(s), tf.squeeze(winds[:,:,1]) )
    return s_mini

def AddCalcWindows_Test():
    
    # Make a random matrix
    W = np.random.rand(5,5)
    s = np.array([[0, 1, 2, 3, 4]])
    
    winds, all_asgn, windSize = AddCalcWindows(W, W.shape, 5, .5)
    
    W_mini = addMiniWeights(W, winds)
    s_mini = addMiniInputs(s, winds)
    
    # Setup op that initializes all the variables
    init = tf.global_variables_initializer()
    
    
    # Make new session
    sess = tf.Session()
    sess.run(init)
    
    sess.run(all_asgn)
    print('TESTING GPU-SPARSE WINDOW INDEXING')
    print('\nMax Window Size')
    print(sess.run(windSize) )
    print('\nMembership Values')
    print(W)
    print('\nWindow Indices')
    print(sess.run(winds))
    
    print("\nIndexing W")
    print(sess.run(W_mini))
    
    print("\nIndexing s")
    print(sess.run(s_mini))
    
    print()
    
#AddCalcWindows_Test()

"""
_______________________________________________________SPARSE LAYER DEFINITIONS


Hidden Layer Relavant Variables:
Y       Window/Pool Membership matrix
s       input to hidden layer
h       hidden layer activity
F       Feature weight matrix
U       Reconstructive weight matrix       

b/p     bias units disabled currently

Pool Layer Relavant Variables:
Y       raw Window/Pool Membership matrix
P       Exclusive Membership matrix
h       input to pool layer
q       pool layer activity
P       mutually exclusive pool matrix
U       Reconstructive weight matrix     


mini variables
    mini varaibles are special re-indexed versions of variables that allow GPU
accelerated multiplication of vectors by sparse/row-abridged matrices. This
greatly accelerates how fast layer activations and gradients can be computed.
Additionally, this format allows layers to dynamically choose how many values
to keep in their shortened row representation. To read more about this look in 
the mini format desctription. Functionally, however this representation is used
to approximate normal matrix multiplication by vectors.

"""
# Takes a membership matrix and makes a dynanamic windowed sparse hidden layer
# Using the mini matrix format to substantially speed up computation.
# Remember Y is the full membership matrix
def sparse_windowed_hidden(Y, s, hidSize, inSize, cutoff, maxWind):
    
    Yshape = [hidSize, inSize]
    
    # create full feature weight matrix
    F = tf.Variable(tf.random_normal([hidSize,inSize]))
    b = tf.Variable(tf.zeros([hidSize,1]), name='b')
    # Create forward mini matrices
    winds, for_asgn, winSize = AddCalcWindows( Y, Yshape, maxWind, cutoff)
    Y_mini = addMiniWeights(Y, winds)
    F_mini = addMiniWeights(F, winds) 
    s_mini = addMiniInputs(s, winds)
    
    # Compute hidden layer activities
    r = tf.reduce_sum( Y_mini*F_mini*s_mini, axis=1)[:,np.newaxis]
    h = tf.nn.relu(r, name = 'h')
    
    # Create backwards weight matrix
    U = tf.Variable(tf.random_normal([inSize,hidSize]))
    p = tf.Variable(tf.zeros([inSize,1]), name='p')
    # Create backward mini matrices
    rwinds, rev_asgn, rwinSize = AddCalcWindows(tf.transpose(Y), [Yshape[1], Yshape[0]], maxWind, cutoff)
    YT_mini = addMiniWeights(tf.transpose(Y), rwinds)
    U_mini = addMiniWeights(U, rwinds)
    h_mini = addMiniInputs(h, rwinds)
    
    # Reconstructive objective
    s_prime = tf.reduce_sum( YT_mini*U_mini*h_mini, axis=1)[:,np.newaxis] #+p
    l_rec = tf.reduce_sum( tf.pow(s - s_prime, 2) ) # +  .01*tf.reduce_sum(tf.pow(F,2))+ .01*tf.reduce_sum(tf.pow(U,2))
    
    # Convenience Op to update all windows
    all_asgn = tf.reduce_sum(for_asgn)+tf.reduce_sum(rev_asgn)
    
    return l_rec, all_asgn, F, U, h, s_prime, winSize
    
# Takes a window membership matrix and makes a dynamically windowed sparse pool
# using the mini matrix format to speed up calculations   
def sparse_windowed_pool(Y, h, outSize, hidSize, cutoff, maxWind, exclusiveness=True, poolAdj=1):
    
    Yshape = [outSize, hidSize]

    # create pool matrix
    if exclusiveness:
        P = tf.nn.relu(tf.nn.softmax(Y*outSize*poolAdj, dim = 0))
    else:
        P = Y*poolAdj
        
    # Create forward mini matrices
    pinds, for_asgn, poolSize = AddCalcWindows(P, Yshape, maxWind, cutoff) # Try Y
    Y_mini = addMiniWeights(Y, pinds)
    P_mini = addMiniWeights(P, pinds)
    h_mini = addMiniInputs(h, pinds)
    
    # Compute pooling layer activities
    PH = tf.multiply(P_mini,h_mini*tf.to_float(poolSize))
    PHmax = tf.nn.softmax(PH, dim = -1) # try without /10 and it works, but not as well as it could!
    q = tf.reduce_sum(PHmax * h_mini, axis = 1 )[:,np.newaxis]
    
    # Average pools
    #q = tf.reduce_max(h_mini, axis = 1 )[:,np.newaxis]
    #q = q*0-10
    
    # create backward weight matrix
    V = tf.Variable(tf.random_normal([hidSize,outSize]))
    
    # Create backward mini matrices
    rpinds, rev_asgn, rpoolSize = AddCalcWindows(tf.transpose(P), [Yshape[1], Yshape[0]], maxWind, cutoff)
    V_mini = addMiniWeights(V, rpinds)
    q_mini = addMiniInputs(q, rpinds)
    
    # Reconstructive Objective
    h_prime = tf.reduce_sum( V_mini*q_mini, axis=1)[:,np.newaxis]
    l_rec = tf.reduce_sum( tf.pow(h - h_prime, 2) ) # + .001*tf.reduce_sum(tf.pow(V,2))
    
    # Convenience Op to update all windows
    all_asgn = tf.reduce_sum(for_asgn)+tf.reduce_sum(rev_asgn)
    
    
    return l_rec, all_asgn, P, V, q, h_prime, poolSize



"""
_________________________________________________________FULL LAYER DEFINITIONS
These layer implementations use full non-sparse weight matrices to compute the
output of each layer. This can be thought of as a reference for what the other
methods are to a degree approximating.

Hidden Layer Relavant Variables:
Y       Window/Pool Membership matrix
s       input to hidden layer
h       hidden layer activity
F       Feature weight matrix
U       Reconstructive weight matrix       

b/p     bias units disabled currently

Pool Layer Relavant Variables:
Y       raw Window/Pool Membership matrix
P       Exclusive Membership matrix
h       input to pool layer
q       pool layer activity
P       mutually exclusive pool matrix
U       Reconstructive weight matrix    


"""


# This defines a generic windowed softplus layer that takes a window matrix Y
# This function needs to have name change as it no longer uses the softplus
# function. To match other naming conventions it should be changed to hidden.
def windowed_softplus(Y, s, hidSize, inSize):
    # Basic Stacking Auto-Encoder Softplus Forward
    F = tf.Variable(tf.random_normal([hidSize, inSize]), name='F')
    b = tf.Variable(tf.zeros([hidSize,1]), name='b')
    r = tf.matmul(F* tf.nn.relu(Y-.01), s, name = 'r') #+b
    h = tf.nn.relu(r, name='h')
    
    # Basic Stacking Auto-Encoder Reconstructive
    U = tf.Variable(tf.random_normal([inSize, hidSize]), name='U')
    p = tf.Variable(tf.zeros([inSize,1]), name='p')
    s_prime = tf.matmul(U*tf.transpose(Y), h, name= 's_prime')+p
    ln_rec = tf.reduce_sum(tf.pow(s_prime-s,2), name='ln_rec')

    return ln_rec, F, U, b, p, h, s_prime 

# This defines a generic pooling layer that takes a pool matrix Y
def windowed_softpool(Y, h, poolSize, hidSize ):
    
    # Basic Stacking Auto-Encoder Forward With SOFT POOL Function...
    P = tf.nn.relu(tf.nn.softmax(Y*poolSize/10, dim = 0))# Make it more mutually exclusive
    #q = tf.log( tf.reduce_sum(tf.exp(P*tf.transpose(h)), axis=1) , name = 'q')[:,np.newaxis] - np.log(poolSize)
    #q = (tf.log( tf.reduce_sum(tf.exp(P*tf.transpose(h)), axis=1) , name = 'q')/np.log(poolSize))[:,np.newaxis] # - np.log(poolSize)
    PH = tf.multiply(P,tf.transpose(h))
    PHmax = tf.nn.softmax(PH*hidSize/10, dim = 1)
    q = tf.reduce_sum( PHmax * tf.transpose(h), axis = 1 )[:,np.newaxis]
    
    # Basic Stacking Auto-Encoder Reconstructive
    V = tf.Variable(tf.random_normal([hidSize, poolSize]), name='V')
    h_prime = tf.matmul(V*tf.transpose(P), q, name= 'h_prime')
    ln_rec = tf.reduce_sum(tf.pow(h_prime-h,2), name='ln_rec')
    
    return ln_rec, P, V, q, h_prime






