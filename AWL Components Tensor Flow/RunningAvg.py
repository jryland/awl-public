# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 19:50:14 2016

@author: james
"""
import numpy as np

class RunningAvg:
    
    def __init__( self, span ):
        
        self.window = np.zeros(( span, 1 ) )
    
    def run(self, newVal):
        
        self.window = np.roll(self.window, 1, axis = 0)
        self.window[0] = newVal
                
        return float(self.window.mean(axis=0))
        
 
# Testing       
#myRa = RunningAvg(5)
#
#for i in range(1,10):
#    print(myRa.run(.1))