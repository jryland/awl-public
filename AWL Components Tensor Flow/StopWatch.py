# -*- coding: utf-8 -*-
"""
Created on Wed Sep 21 18:06:31 2016

@author: james
"""
import time
from NumFormat import sigdig

class StopWatch:
    
    def __init__(self):
        
        self.startTime = time.time()  
    
    def start(self):
        
        self.startTime = time.time()        
        
    def stop(self, runs=1, messageType='speedtest'):
        
        self.stopTime = time.time()         
        
        span = (self.stopTime - self.startTime)        
        
        if messageType == 'speedtest':
            print("\n")
            print("Time to run 1 iteration: "+str(sigdig(span/runs,3)) +" Sec")  
            print("  Iterations Per Second: "+str(sigdig(runs/span,3)) +"")
        
        if messageType == 'Runtime':
            print("\n")
            print("Simulation Completed: "+str(sigdig(round(span/60),3))+" Minutes")
        
    def timeStamp(self):
        
        self.curTime = time.time() 
        
        span = (self.curTime - self.startTime)        
        
        tStr = "Elapsed Time: "+str(sigdig(round(span/60),3))+" Minutes"
        
        return tStr 
        