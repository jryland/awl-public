# -*- coding: utf-8 -*-
"""
Created on Sun Jul  2 20:57:37 2017

@author: James Ryland
"""
import tensorflow as tf
import numpy as np
import scipy as sp
import time as t

def sparse_test():
    
    # Test how large a matrix we can make
    size = 10000
    short = 100
    startInds = np.array([[0,0]])
    startVals = np.array([.1])
    
    # Think of a sparse tensor as a conventient window on other data, not its 
    # own thing
    A_short = tf.Variable(tf.random_uniform( (size, short)) )
    r, c = np.meshgrid(np.arange(size), np.arange(short), indexing='ij')
    A_short_rinds = tf.Variable(r, tf.int32)
    A_short_cinds = tf.Variable(c, tf.int32)
    spRowIn = tf.reshape(A_short_rinds, [-1])[:,tf.newaxis]
    spColIn = tf.reshape(A_short_cinds, [-1])[:,tf.newaxis]
    sp_inds = tf.concat( [spRowIn, spColIn], axis =1)
    
    # This can be used for fun things like argsort perhaps?
    # But again its a view on non-sparse data not a variable uggghhh
    
    # Not sure if this thing will be all that usefull....
    # Almost no specialized functions are implemented for SparseTensors in tensor flow
    # Shrug...
    A = tf.SparseTensor(sp_inds, tf.reshape(A_short,[-1]), dense_shape=[size,size])
    
    
    # Cycle through rows updating A_short
    upNum = 200
    rinds = tf.Variable(np.arange(upNum)[:,np.newaxis], dtype=tf.int32)
    rinds_up = tf.assign(rinds, rinds+upNum )
    cints = np.arange(size)
    rinds_mod = tf.to_int32(tf.mod( rinds_up, size ))
    rowIn, colIn = tf.meshgrid(tf.to_int32(rinds_mod), tf.to_int32(np.arange(size)) )
    
    # Make sparse representation for A_rows
    A_rows = tf.random_uniform( (upNum, size), 0, 1 )
    A_rows_short, sortInds = tf.nn.top_k( A_rows, short, True ) 
    
    
    # Update A
    A_short_cinds_up = tf.scatter_nd_update(A_short_cinds, rinds_mod, tf.to_int64(sortInds))
    A_short_up = tf.scatter_nd_update(A_short, rinds_mod, A_rows_short)
    A_up = tf.to_float(A_short_cinds_up[0,0])+A_short_up[0,0]
    
    
    # Not available sadly.... :(
    #A_sortm, bka = tf.nn.top_k( A, short, True ) 
    
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Fill A
    start = t.time()
    numRuns = round(size/upNum)
    for i in range(0, round(size/upNum)):
        sess.run([A_up])
    stop = t.time()
    
    print('Fill Time: '+str((stop-start)/numRuns))
    
    #start = t.time()
    #for i in range(0, round(size/upNum)):
    #    sess.run([A_sort])
    #stop = t.time()
    #print('Sort Time: '+str(stop-start))
    
    # Test top_k
    
    # Test 
    
    # Test how large a correlation we can make and how
    
sparse_test()