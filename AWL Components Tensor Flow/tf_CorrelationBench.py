#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 19:03:28 2017

@author: james

This is a simple function that I have been using to figure out which method of
Caclulating the correlation matrix will scale best for the algorithm.

Results so far
        Run     Norm    Sub     Best
  1000  *       *       *
 10000  *               *   
 50000                  *
100000                  *    
500000                  *         

Note: This suggests that that addCorrelationMatrix_Sub is the only version that will 
      Scale well at all...

Note: We can make a flag for simple kohonen to use randomly sampled addCorrelationMatrix_Sub
      Outputs... 
      
Note: Examine simple kohonen for things that will not scale well....
      Probably need to store a distance matrix from lattice rather than
      constantly recomputing it... which will be really bad for large
      numbers of units.


Kohonen Benchmarks
         AWKL    AWEL   Best
   1000  *       *
   5000  *       *
  10000  X       *
  50000          *       
 100000          *      .03sec/run
1000000          *      .3sec/run

Note: when the kohonen space has a low fixed dimensionality the kohonen
      algorithm scales quite well... But when the dimensionality scales with
      the input space it rapidly runs out of memory...
      
Note: Could set the kohonen algorithm to loop through the dimensions to prevent
      the out of memory error... Lets see if that salvages anything..
      
Note: Looping over dimension did not work, still runs out of memory after 5000  
      shoot

Note: AWL with Correlational Embedding may be the easiest way to address this...
      However the organization of points in AWEL and AWKL may feature strong
      learning differences.

"""



import RylandToolsTF as rtf
import tensorflow as tf
import numpy as np



def KohonenBenchmark(inSize, dim, outSize, samples):
    
    import time
    
    X = tf.constant( np.random.uniform(-.5,.5, (inSize, dim)), dtype=np.float32 )
    spread = tf.placeholder( tf.float32, shape = [1,1], name = 'spread')
    lo, Y, L = rtf.SimpleKohonen(X, outSize, 10, spread )
    
    optimizer = tf.train.GradientDescentOptimizer(.01)
    train = optimizer.minimize(lo)
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    Y_val = sess.run(Y)
    
    start = time.time()
    for i in range(0, samples):
        
        # must be (1,1)
        spread_val = np.array( 1.0/(1.0+i*.05) , dtype=np.float32 )[np.newaxis,np.newaxis]
        lo_val, Y_val = sess.run([lo, Y], feed_dict = {spread:spread_val})
        sess.run(train, feed_dict = {spread:spread_val})
    
    end = time.time()

    runTime=(end-start)/(samples)
    
    print('\nKohonen Runtime')
    print(str(runTime)+'sec')
        

def CorrelationBenchmark( methods, inSize, samples):
    import time

    S_vals = np.random.rand(inSize,samples)
    
    if methods[0]:
        # Compare running version
        s = tf.placeholder(tf.float32, shape = [S_vals.shape[0], 1 ])
        R, R_up= rtf.addRunningCorrelationMatrix(s, 10, diag0 = False)
        Rsqr = tf.pow(R,2)
        
        #print(S_vals[:,0,np.newaxis].shape)
        
        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)
        
        R_val = []
        Rsqr_val = []
        
        start = time.time()
        for i in range(0, S_vals.shape[1]):
            #print(S_vals[:,np.newaxis].shape)
            R_val, Rsqr_val, dummy= sess.run([R, Rsqr, R_up] , feed_dict = {s:S_vals[:,i,np.newaxis] })
        end = time.time()
    
        runTime1=(end-start)/(samples)
    
        print('\nRunning Computation Time:')
        print(  str(runTime1)+'sec')
    
    if methods[1]:
        # Normal Version
        s_buff = tf.placeholder(tf.float32, shape = [None, None ])
        R = rtf.ddCorrelationMatrix(s_buff, diag0 = False)
        Rsqr = tf.pow(R,2)
        
        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)
        
        R_val2 = []
        Rsqr_val2 = []
        
        R_val2, Rsqr_val2 = sess.run([R, Rsqr] , feed_dict = {s_buff:S_vals[:,:]})
        
        # Measure performance
        start = time.time()
        # UPDATE FAR LESS OFTEN 
        for i in range(0, int(S_vals.shape[1]/50) ):
            sess.run([R, Rsqr] , feed_dict = {s_buff:S_vals[:,0:200]})
        end = time.time()
        runTime2=(end-start)/(samples)
    
        print('\nNormal Computation Time:')
        print(  str(runTime2)+'sec')
        
    if methods[2]:
        # Lazy partial Version
        s_buff = tf.placeholder(tf.float32, shape = [None, None ])
        R = rtf.addCorrelationMatrix_Sub(s_buff, np.zeros((10,1), np.int32))
        Rsqr = tf.pow(R,2)
        
        init = tf.global_variables_initializer()
        sess = tf.Session()
        sess.run(init)
        
        
        # Measure performance
        start = time.time()
        for i in range(0, int(S_vals.shape[1]) ):
            R_val = sess.run(R , feed_dict = {s_buff:S_vals[:,0:200]})
        end = time.time()
        runTime3=(end-start)/(samples)
        
        print('\nSub Computation Time:')
        print(  str(runTime3)+'sec')
    
    print('\nUnit Bench Complete')

#CorrelationBenchmark([False, False, True], 500000, 100 )

# AWKL ASSUMPTIONS
inSize = 1000000
dim =    10
KohonenBenchmark(inSize, dim, inSize, 100)

