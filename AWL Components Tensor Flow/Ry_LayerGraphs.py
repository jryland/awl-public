# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 01:05:08 2017

@author: James Ryland

GRAPHICS FUNCTIONS
        CortexGraph
        ActivityPanel
        TopoPanel
        WindowPanel
        cyclicColorMap

"""

import numpy as np
from scipy import interpolate as inter
from scipy import spatial as spatial 
import matplotlib.pyplot as plt
import Foveal as fv

# plot objective function and accuracy
class AccuracyPanel:
    
    def __init__(self):
        
        plt.ion()
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(121)
        self.ax1.set_title('Objective Function')
        self.ax2 = self.fig.add_subplot(122)
        self.ax2.set_title('Accuracy')
        
        self.ln_hist  = []
        self.acc_hist = []
        self.t_hist = []
        
    def update(self, ln_val, acc, t):
        
        self.ln_hist.append(ln_val)
        self.acc_hist.append(acc)
        self.t_hist.append(t)
        
        self.ax1.cla()
        self.ax1.set_title('Objective Function')
        self.ax1.plot(self.t_hist, self.ln_hist)
        
        self.ax2.cla()
        self.ax2.set_title('Accuracy')
        self.ax2.plot(self.t_hist, self.acc_hist)
            
        self.fig.canvas.draw()


# Given an axis creates a scatter plot of a cortical latice, specified by L
# Further it will graph values by assigning colors to particular values
# plt.show may still need to be called to actually update view
class CortexGraph:
    
    def __init__(self, ax, L, sizeAdj = 1, raster=False, rasterRez=800):
        #import matplotlib.pyplot as plt# assumed to already have been loaded
        self.values = np.zeros( (L.shape[0], 1), dtype=np.float32 )
        self.raster = raster
        self.rasterRez = rasterRez        
        self.L = L        
        self.ax = ax
        self.scL = []
        self.cols = np.zeros( (L.shape[0], 3), dtype=np.float32 )        

        if not self.raster:            
            self.scL = ax.scatter(L[:,0], L[:,1], np.round(100*sizeAdj), edgecolors='face' )
            self.ax.set_axis_bgcolor((0, 0, 0))
        else:
            im = np.zeros( (self.rasterRez,self.rasterRez,3), dtype= float )
            self.ax.imshow(im)
            self.ax.axis('off')
        
    def set_to_colormap(self, values, colmap ):
        print('lol')
        
    def set_values(self, values):
        self.values = values
        # Scale to [0 1] range
        scaled_act = (values-np.min(values))/(np.max(values)-np.min(values))
        
        # Assign Colors
        # Basic version for testing right now
        self.cols = np.ones_like(self.cols)*scaled_act
        if not self.raster:        
            self.scL.set_facecolors(self.cols)
        else:
            self.raster_interp()
    
    def set_colors(self, cols):
        
        self.cols = cols
        if not self.raster:        
            self.scL.set_facecolors(self.cols)
        else:
            self.raster_interp()
        
    def addBorder(self, Lpoints, cols):
        phi = np.linspace(-np.pi, np.pi, 200)
        Lbord = np.concatenate( (np.cos(phi)[:,np.newaxis], np.sin(phi)[:,np.newaxis]), 1)*.5
        Cbord = np.zeros((Lbord.shape[0], 3))        
        Lplus = np.concatenate( (Lpoints, Lbord), axis=0) 
        Cplus = np.concatenate( (cols,    Cbord), axis=0)
        return Lplus, Cplus
   
    def raster_interp(self):
        pvec = np.linspace(-.5,.5, self.rasterRez)  
        X,Y = np.meshgrid( pvec, pvec)
        rIm = []        
        gIm = []
        bIm = []
        
        Lplus, Cplus = self.addBorder(self.L, self.cols)
        
        ras = np.concatenate( (X.flatten()[:,np.newaxis], Y.flatten()[:,np.newaxis]), axis=1)
        
        dTree = spatial.cKDTree(Lplus)       
        
        inds = np.zeros( (ras.shape[0], 1), dtype=int )
        par = 10000          
        for i in range(0, ras.shape[0], par):
            pInds = np.mod(np.arange(i, i+par), ras.shape[0] )            
            #D = np.sum( np.power(ras[pInds,np.newaxis,:] - Lplus[np.newaxis,:,:], 2), 2 )
            #inds[pInds] = np.argmin(D, axis=1)[:,np.newaxis]            
            dum, tInds = dTree.query(ras[pInds,:])
            inds[pInds,:] = tInds[:,np.newaxis]
            
            #if np.mod(i,1000)==0:
            #    print('Raster:'+str(round(i/ras.shape[0]*100))+'%')
        
        #D = np.sum( np.power(ras[:,np.newaxis,:] - Lplus[np.newaxis,:,:], 2), 2 ) 
        #inds = np.argmin(D, axis=1)
        rIm = np.reshape( np.squeeze(Cplus[:,0])[inds], X.shape)[:,:,np.newaxis]      
        gIm = np.reshape( np.squeeze(Cplus[:,1])[inds], X.shape)[:,:,np.newaxis]
        bIm = np.reshape( np.squeeze(Cplus[:,2])[inds], X.shape)[:,:,np.newaxis]
        
        im = np.concatenate( (rIm,gIm,bIm), axis=2 )
        self.ax.cla()
        self.ax.imshow(im,interpolation = 'nearest')
        self.ax.axis('off')
        


class WindowPanel:
    
    def __init__(self):
        import matplotlib.pyplot as plt
        plt.ion()
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(121)
        self.ax1.set_title('Histogram of Window Membership')
        self.ax2 = self.fig.add_subplot(122)
        self.ax2.set_title('Histogram of Pool Membership')

        
    def plotHist(self, Y_h, Y_q ):
        
        # Turn to vecs first!!
        self.ax1.cla()
        self.ax1.hist(Y_h.flatten(), 50, normed=1, facecolor='green', alpha=0.75)
        self.ax1.set_title('Histogram of Window Membership')
        self.ax2.cla()
        self.ax2.hist(Y_q.flatten(), 50, normed=1, facecolor='blue', alpha=0.75)
        self.ax2.set_title('Histogram of Pool Membership')
        self.fig.canvas.draw()

# Use to find what stimulus label causes maximum activations
# Good for plotting topological maps and transforms       
def ValByMaxAct( A, vals ):
    # Assign colors using maximum activation
    AmaxInd = np.argmax(A, axis=1)
    aVal = vals[AmaxInd]
    return aVal    


def ValByCircMean(A, vals): # Assumed vals on [0 1] scale, cyclic
    # Assign colors using maximum activation
    Xweight = A*np.cos((vals-.5)*np.pi*2)
    Yweight = A*np.sin((vals-.5)*np.pi*2)
    
    Xmean = np.mean(Xweight, axis=1)
    Ymean = np.mean(Yweight, axis=1)
    
    aVal = (np.arctan2(Xmean, Ymean)+np.pi)/(2*np.pi)
    
    return aVal
    


class ActivityPanel:
    
    def __init__(self, Lh, Lq, sizeAdj = [1, .5], raster=False, rasterRez=800):
        import matplotlib.pyplot as plt
        plt.ion()
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(131)
        self.ax2 = self.fig.add_subplot(132)
        self.cgh = CortexGraph(self.ax2, Lh, sizeAdj[0], raster=raster, rasterRez=rasterRez)
        self.ax3 = self.fig.add_subplot(133)
        self.cgq = CortexGraph(self.ax3, Lq, sizeAdj[1], raster=raster, rasterRez=rasterRez)
        #self.ax1.axis('off')
        #self.ax2.axis('off')
        
        
    def plotActivity(self, im, h, q ):
        
        self.ax1.cla()
        self.ax1.imshow(im, cmap="Greys", interpolation='nearest')
        self.cgh.set_values(h)
        self.cgq.set_values(q)
        self.ax1.set_title('Input Layer Activity')
        self.ax2.set_title('Hidden Layer Activity')
        self.ax3.set_title('Pooling Layer Activity')
        self.fig.canvas.draw()
        

class TopoPanel:    

    def __init__(self, Lh, Lq, sizeAdj = [1, .5], raster=False, rasterRez=800, accDeg=False):
        
        import matplotlib.pyplot as plt
        self.raster=raster
        self.rasterRez=rasterRez
        
        self.fig2  = plt.figure()
        
        # Choose figure format normal or paired
        if not accDeg:
            self.f2ax1 = self.fig2.add_subplot(131)
            self.f2ax2 = self.fig2.add_subplot(132)
            self.f2ax3 = self.fig2.add_subplot(133)
        else:
            self.f2ax1 = self.fig2.add_subplot(231)
            self.f2ax2 = self.fig2.add_subplot(232)
            self.f2ax3 = self.fig2.add_subplot(233)
            self.f2ax4 = self.fig2.add_subplot(234)
            self.f2ax5 = self.fig2.add_subplot(235)
            self.f2ax6 = self.fig2.add_subplot(236)       
            self.cgh_topB = CortexGraph(self.f2ax5, Lh, sizeAdj[0], raster=raster, rasterRez=rasterRez)
            self.cgq_topB = CortexGraph(self.f2ax6, Lq, sizeAdj[1], raster=raster, rasterRez=rasterRez)
            self.f2ax4.axis('off')
            self.f2ax5.axis('off')
            self.f2ax6.axis('off')
            
        self.f2ax1.axis('off')
        self.f2ax2.axis('off')
        self.f2ax3.axis('off')
        self.cgh_top = CortexGraph(self.f2ax2, Lh, sizeAdj[0], raster=raster, rasterRez=rasterRez)
        self.cgq_top = CortexGraph(self.f2ax3, Lq, sizeAdj[1], raster=raster, rasterRez=rasterRez)
        
        
        
    # Graph retonotopy    
    def plotTopoXY(self, rez, H, Q, xVal, yVal, xIm, yIm):
        # Assign colors using maximum activation
        # Turn this into a function Lol
        imMap = np.zeros( (rez, rez, 3))
        imMap[:,:,0] = xIm*.9 + .1
        imMap[:,:,2] = yIm*.9 + .1
        imMap[:,:,1] = .1
        
        self.f2ax1.cla()
        self.f2ax1.imshow(imMap,interpolation='nearest')
        self.f2ax1.set_title('Retinal Position Map')
        self.f2ax1.axis('off')
        self.f2ax2.axis('off')
        
        hCols = np.zeros( (H.shape[0], 3))
        hCols[:,0] = ValByMaxAct(H, xVal)*.9 +.1
        hCols[:,2] = ValByMaxAct(H, yVal)*.9 +.1
        hCols[:,1] = .1
        
        self.cgh_top.set_colors(hCols)
        self.f2ax2.set_title('Hidden Layer Retinotopy')
        
        
        qCols = np.zeros( (Q.shape[0], 3))
        qCols[:,0] = ValByMaxAct(Q, xVal)*.9 +.1
        qCols[:,2] = ValByMaxAct(Q, yVal)*.9 +.1
        qCols[:,1] = .1
        
        self.cgq_top.set_colors(qCols)
        self.f2ax3.set_title('Pooling Layer Retinotopy')
        
        self.fig2.canvas.draw()

    # Graph orientation topography
    def plotTopoOR(self, rez, H, Q, oVal, oIm, circleMean=True):
        
        self.f2ax1.cla()
        #self.f2ax1.imshow(oIm[:,:,np.random.randint(0,oIm.shape[2])],interpolation='nearest')
        X, Y = np.mgrid[0:100, 0:100]    
        X = X/100
        X = X-np.floor(X)    
        xVec = np.reshape(X, ( np.prod( X.shape ),1 ), order = 'F' )
        cols = cyclicColorMap(xVec)
        colIm = np.reshape(cols, (X.shape[0], X.shape[1], 3), order = 'F' )
        self.f2ax1.imshow(colIm, interpolation='nearest')
        self.f2ax1.set_title('Orientation Map')
        self.f2ax1.axis('off')
        self.f2ax2.axis('off')
        
        hCols = np.zeros( (H.shape[0], 3))
        if circleMean:
            hCols = cyclicColorMap( ValByCircMean(H, oVal) )
        else:
            hCols = cyclicColorMap( ValByMaxAct(H, oVal) )
        
        self.cgh_top.set_colors(hCols)
        self.f2ax2.set_title('Hidden Layer Orientation Map')
        
        
        qCols = np.zeros( (Q.shape[0], 3))
        if circleMean:
            qCols = cyclicColorMap( ValByCircMean(Q, oVal) )
        else:
            qCols = cyclicColorMap( ValByMaxAct(Q, oVal) )
        
        self.cgq_top.set_colors(qCols)
        self.f2ax3.set_title('Pooling Layer Orientation Map')
        
        self.fig2.canvas.draw()
        
    # Graph the accentricity or angle topography
    def plotTopoAcc(self, rez, accH, accQ, accVal, accBlock, degH, degQ, degVal, degBlock ):
        
        # PLOT ACC Related maps
        self.f2ax1.cla()
        accImg = fv.scaleBlock_to_img(accBlock, color=False)
        accImg = np.reshape(cyclicColorMap( accImg.flatten(order='F') ), [accImg.shape[0],accImg.shape[1],3], order='F')
        self.f2ax1.imshow(accImg,interpolation = 'nearest')
        self.f2ax1.set_title('Accentricity Map')
        
        hCols = np.zeros( (accH.shape[0], 3))
        hCols = cyclicColorMap( ValByMaxAct(accH, accVal) )
        
        self.cgh_top.set_colors(hCols)
        self.f2ax2.set_title('Hidden Layer Accentricity  Map')
        
        
        qCols = np.zeros( (accQ.shape[0], 3))
        qCols = cyclicColorMap( ValByMaxAct(accQ, accVal))
        
        self.cgq_top.set_colors(qCols)
        self.f2ax3.set_title('Pooling Layer Accentricity Map')
        
        # PLOT DEG Related maps
        self.f2ax4.cla()
        degImg = fv.scaleBlock_to_img(degBlock, color=False)
        degImg = np.reshape(cyclicColorMap( degImg.flatten(order='F') ), [degImg.shape[0],degImg.shape[1],3], order='F')
        self.f2ax4.imshow(degImg,interpolation = 'nearest')
        self.f2ax4.set_title('Polar Degree Map')
        
        hCols = np.zeros( (degH.shape[0], 3))
        hCols = cyclicColorMap( ValByMaxAct(degH, degVal) )
        
        self.cgh_topB.set_colors(hCols)
        self.f2ax5.set_title('Hidden Layer Polar Degree Map')
        
        
        qCols = np.zeros( (degQ.shape[0], 3))
        qCols = cyclicColorMap( ValByMaxAct(degQ, degVal) )
        
        self.cgq_topB.set_colors(qCols)
        self.f2ax6.set_title('Pooling Layer Polar Degree Map')
        
        self.f2ax1.axis('off')
        self.f2ax2.axis('off')
        
        self.fig2.canvas.draw()
        
    # plot the spatial frequency sensitivity topography    
    def plotTopoMag(self, rez, H, Q, mVal, mBlock):
        
        self.f2ax1.cla()
        
        mImg = fv.scaleBlock_to_img(mBlock, color=False)
        mImg = np.reshape(cyclicColorMap( mImg.flatten(order='F') ), [mImg.shape[0],mImg.shape[1],3], order='F')
        self.f2ax1.imshow(mImg,interpolation = 'nearest')
        self.f2ax1.set_title('Spatial Frequency Map')
        self.f2ax1.axis('off')
        self.f2ax2.axis('off')
        
        
        hCols = np.zeros( (H.shape[0], 3))
        hCols = cyclicColorMap( ValByMaxAct(H, mVal) )
        
        self.cgh_top.set_colors(hCols)
        self.f2ax2.set_title('Hidden Layer Frequency  Map')
        
        
        qCols = np.zeros( (Q.shape[0], 3))
        qCols = cyclicColorMap( ValByMaxAct(Q, mVal) )
        
        self.cgq_top.set_colors(qCols)
        self.f2ax3.set_title('Pooling Layer Frequency Map')
        
        
        self.fig2.canvas.draw()


# APPLY CICLIC COLOR MAP        
# vals must be a 1D array [0 1]
# This array could be quite large...
# Values of -1 shoudl be considered errors
def cyclicColorMap( val ):
    
    import colorsys as cls
    
    # Lazy way to avoid discontinuities
    val = val*5.99999999;
    
    # Hexagonal cyclic colormap (HUE)
    colMap = np.array( [ [1,0,0], [1,1,0], [0,1,0], [0,1,1], [0,0,1], [1,0,1], [1,0,0]])
    
    cols = np.zeros( (val.shape[0], 3))
    
    # this could be done better
    for v in range(0, val.shape[0]):
        
        if True:
            for i in range(0, 6):
                if i<=val[v] and val[v]<(i+1):    
                    # lin interp between the correct pair
                    cDir = colMap[i+1] - colMap[i]
                    cols[v,:] = (val[v]-np.floor(val[v]))*cDir + colMap[i]
                
                # Paint error values
                elif val[v]==-1:
                    cols[v,:] = np.array([.1, .1, .1])
        else:            
            cols[v,:] = cls.hsv_to_rgb(val/6.0, 1, 1)
    
    
    return cols

def testCyclicColorMap():
    
    import matplotlib.pyplot as plt
    X, Y = np.mgrid[0:100, 0:100]    
    
    X = X/50
    X = X-np.floor(X)
    
    xVec = np.reshape(X, ( np.prod( X.shape ),1 ), order = 'F' )
    cols = cyclicColorMap(xVec)
    colIm = np.reshape(cols, (X.shape[0], X.shape[1], 3), order = 'F' )
    
    fig = plt.figure()
    ax=fig.add_subplot(111)
    ax.imshow(colIm, interpolation = 'nearest')
    
    
    
def CortexGraph_Test():
    import matplotlib.pyplot as plt 
    import Lattice as lat    
    
    #L = np.random.rand( 1000, 2 )
    #val = np.random.rand( 1000, 1 )
    numP = 16000
    L = lat.makeLattice('hex', 'circle',numP, True)
    val = np.random.rand( numP, 3 )
    print('done')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cg = CortexGraph(ax, L, raster=True, rasterRez=1000)
    cg.set_colors(val)
    plt.show()

#CortexGraph_Test()