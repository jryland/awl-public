# -*- coding: utf-8 -*-
"""
MyPerlinSampler

Created on Tue Apr 26 02:19:38 2016

Adapted from pseudo code
https://en.wikipedia.org/wiki/Perlin_noise

This is the perlin function but rewritten to use arrays instead of loops
I need a better random hash for the vertices


@author: James Ryland
"""
import math
import numpy as np
import random as r



# Function to linearly interpolate between a0 and a1
# Weight w should be in the range [0.0, 1.0]
def lerp(a0, a1, w):
    return (1 - w)*a0 + (w)*a1 # ELEMENT-WISE MULTIPLY

# Linear Conguential Generator
def intToRandChoice(seed, numChoices):
    m = pow(2,32)
    a = 1103515245 
    c = 12345    
    seed = (a * seed + c) % m;
    #seed = (a * seed + c) % m;
    choice = np.floor(seed/m*numChoices)
    return choice;

# Fast Hash
# OH YEA THIS WORKS FINE AND DANDY!!!
# https://briansharpe.wordpress.com/2011/11/15/a-fast-and-simple-32bit-floating-point-hash-function/
def hashToRandChoice(ix, iy, numChoices):
    
    offset = np.array([26.0, 161.0])
    domain = float(71.0)
    largef = 951.135664
    px = ix - np.floor(ix/domain)*domain + offset[0]
    py = iy - np.floor(iy/domain)*domain + offset[1]
    
    px = px*(px + 1) 
    py = py*(py + 1)
        
    [P, dummy1] = np.modf(px*px*py*py / ( largef))
    
    choice = np.floor(P*numChoices)
    return choice;



# Computes the dot product of the distance and gradient vectors
# Estimates the hight of a point, out from a point where we know the gradient 
# and the point is at zero.
def dotGridGradient(ix, iy, x, y):
    # i means int
   
    choices = np.array([[1, 0], [0, 1], [-1, 0], [0, -1]]) 
    
    # Really expensive random method!!    
    seed = ix*1003050+iy*3091

    # no work with matrices    
    #r.seed(seed)        
    #G = choices[r.randint(0,3),:]    
    
    # Not random enough
    #G = choices[intToRandChoice(seed,4).astype(int),:].T  
    
    G = choices[hashToRandChoice(ix, iy,4).astype(int),:].T  
    
    
    # find the distance vector
    dx = x - ix
    dy = y - iy
    
    # This estimates the hight of a surface, out from a point where we
    # Know the gradient
    return (dx*G[0,:]+dy*G[1,:])

# Compute Perlin noise at coordinates    
def perlinBase2D(pos):
    
    # these are row vectors now
    x = pos[0]
    y = pos[1]
           
    # array good
    # Determine grid cell coordinates 
    x0 = np.floor(x);
    x1 = x0 + 1;
    y0 = np.floor(y);
    y1 = y0 + 1;
    
    # array good
    #Determine interpolation weights
    #Could also use higher order polynomial/s-curve here
    sx = x - x0;
    sy = y - y0;
    
    # THIS IS REALLY IMPORTANT: OTHERWISE IT LOOKS LIKE SHIT
    # THIS IS A SIMPLE EASE CURVE
    sx = 6*np.power(sx,5) - 15*np.power(sx,4)+10*np.power(sx,3)    
    sy = 6*np.power(sy,5) - 15*np.power(sy,4)+10*np.power(sy,3)    
    
    # Interpolate between grid point gradients
     
    n0 = dotGridGradient(x0, y0, x, y); # get hight estimated by grad <0,0> to <1,1> 
    n1 = dotGridGradient(x1, y0, x, y); # get hight estimated by grad <0,1> to <1,1> 
    ix0 = lerp(n0, n1, sx);             # Interp hights
      
    n0 = dotGridGradient(x0, y1, x, y);
    n1 = dotGridGradient(x1, y1, x, y);
    ix1 = lerp(n0, n1, sx);             # Interp estimates <0,1> to <1,1>
      
    value = lerp(ix0, ix1, sy);
    
    
    return value;

# collect one rectangular image
def perlin2(dim, scale, rot, octs, lac, pers, offset):
    
    # specify axes rotation
    angle = rot/180*math.pi;
    R = np.array([[np.cos(angle), -np.sin(angle)], 
                  [np.sin(angle),  np.cos(angle)]]);
    # specify scale
    sc = scale
    SC = np.array([[sc, 0 ], 
                   [0 , sc]] )
    T = np.dot(R,SC);
    
    amp = .5
    total = amp    
    p = np.zeros(dim)
    
    for o in range(octs):
        T = np.dot(R,SC);
        
        
        [X, Y] = np.meshgrid(np.arange(dim[0]), np.arange(dim[1]))
        
        X = X+offset[0]
        Y = Y+offset[1]        
        
        # make a 2 by num points array
        pos = np.array([X.reshape( dim[0]*dim[1] , order='F'),
                        Y.reshape( dim[0]*dim[1] , order='F')]) 
        
        # should be the transformed points                        
        Tpos = np.dot(T,pos)
        
        p = p + perlinBase2D(Tpos).reshape(dim, order='F')*amp
        
        total = total+amp;
        sc = sc*lac
        SC = np.array([[sc, 0 ], 
                       [0 , sc]] )
        amp = amp*pers
        
    return p

# collect one foveal pyramid
def perlinPyramid(dim, pyRatio, scale, rot, octs, lac, pers, offset):
    
    # specify axes rotation
    angle = rot/180*math.pi;
    R = np.array([[np.cos(angle), -np.sin(angle)], 
                  [np.sin(angle),  np.cos(angle)]]);
    
    
    # specify initial scale
    sc = scale
    SC = np.array([[sc, 0 ], 
                   [0 , sc]] )
    T = np.dot(R,SC);
    
    amp = .5
    total = amp    
    p = np.zeros(dim)
    
    for o in range(octs):
        T = np.dot(R,SC);
        
        
        [X, Y, Z] = np.meshgrid(np.arange(dim[0]), np.arange(dim[1]), np.arange(dim[2]))
        X = X-dim[0]/2
        Y = Y-dim[1]/2        
        
        X = np.multiply(X, np.power(pyRatio, Z))
        Y = np.multiply(Y, np.power(pyRatio, Z))
        
        X = X+offset[0]
        Y = Y+offset[1]        
        
        # make a 2 by num points array
        pos = np.array([X.reshape( dim[0]*dim[1]*dim[2] , order='F'),
                        Y.reshape( dim[0]*dim[1]*dim[2] , order='F')]) 
        
        # should be the transformed points                        
        Tpos = np.dot(T,pos)
        
        p = p + perlinBase2D(Tpos).reshape(dim, order='F')*amp
        
        total = total+amp;
        sc = sc*lac
        SC = np.array([[sc, 0 ], 
                       [0 , sc]] )
        amp = amp*pers
            
    return p



        
    
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
 

   
def TestPerl():
    imgx = 200; imgy = 200   # image size
    image = Image.new("RGB", (imgx, imgy))
    draw = ImageDraw.Draw(image)
    pixels = image.load()
    
    perl = np.zeros((imgx,imgy))
    
    
    dim = (imgx, imgy)
    np.random.seed()
    perl = perlin2(dim, 1/20, 30, 4, 2, .6, np.random.uniform(-1000,1000, 2))
            
    # Write Image
    for ky in range(imgy):
        for kx in range(imgx):    
            c = int((perl[kx,ky]+1)/2 * 255)
            pixels[kx, ky] = (c, c, c)
            
    
    #label = "Persistence = " + str(persistence)
    #draw.text((0, 0), label, (0, 255, 0)) # write to top-left using green color
    #image.save("PerlinNoise.png", "PNG")
    
    plt.imshow(image,cmap="hot")
    plt.axis('off')  # clear x- and y-axes
    plt.show()
    
    return image  
    
def TestPerlPyr():
    imgx = 100; imgy = 100   # image size
    imgz = 7
    
    perl = np.zeros((imgx,imgy))
    
    dim = (imgx, imgy, imgz)
    np.random.seed()
    perl = perlinPyramid(dim, 1.5, 1/150, 30, 8, 2, .6, np.random.uniform(-1000,1000, 2))
            
    perl = np.reshape(perl, (imgx, imgy*imgz), order='F')            
               
    
    plt.imshow(perl)
    plt.axis('off')  # clear x- and y-axes
    plt.show()
    
    
def GetPerlPyramid(rez, numScales, noiseType = 'perl', scale=1):
    imgx = rez; imgy = rez  # image size
    imgz = numScales           # number of image scales
    
    perl = np.zeros((imgx,imgy,imgz))
    
    topScale = 1/150*scale
    
    dim = (imgx, imgy, imgz)
    np.random.seed()
    perl = perlinPyramid(dim, 1.5, topScale, np.random.uniform(0, 360), 8, 2, .6, np.random.uniform(-1000,1000, 2))
    
    if noiseType == 'edge':
        perl = 2/(1+ np.exp(np.sin(perl*10)*10) )-1
        
    if noiseType == 'sin':    
        perl = np.sin(perl*10)
    
    # Most of the other functions expec largest to smallest FOV order
    perl = np.flip(perl, 2)
    
    return perl
    
    
def TestPerl2(rez):
    imgx = rez; imgy = rez   # image size
    
    perl = np.zeros((imgx,imgy))
    
    dim = (imgx, imgy)
    np.random.seed()
    perl = perlin2(dim, 1/10, 30, 4, 2, .6, np.random.uniform(-1000,1000, 2))
            
    
    return perl  
    

import time

def TestPerlCycle():
    
    for i in range(100):
        TestPerl()
        #time.sleep(.01)
    