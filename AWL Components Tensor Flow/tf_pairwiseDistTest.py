#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 13 18:54:30 2017

@author: james
"""

import tensorflow as tf


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy as sp
import scipy.ndimage as nimg
import mni st as mn
import numpy as np

def pairDist():
    
    # This can be used for both calculating Lattice Distence and
    # Parameter Distance YAY!!!! 
    
    X_val = np.random.rand(200, 3)
    
    inds_val = np.floor( np.random.rand(100,1)*200).astype(np.int32) 
    
    X = tf.placeholder(tf.float32, shape = [None, None ], name='X'  )
    inds = tf.placeholder(tf.int32, shape = [None, 1], name='inds'  )
    
    # This is already parrallelized and could work for kohonen net!!!
    
    # NEED TO USE GATHERND   gather(X_sub, inds) or something to that effect...
    # Basic Slice Indexing  [[row1] [row2]] not [[r1 c2][r1 c2]]
    # Both are 2D arrrays [[]] because the inices need to be wrapped in a top 
    # layer [ [fullindex1] [fullindex2] ] object to 1D array
    # layer [ [[fullindex1] [fullindex2]] [[fullindex3] [fullindex4]] ] object to 2D array
    # Here we only give a partial index which slices
    X_sub = tf.gather_nd(X, inds, name = 'X_sub')
    #X_sub = X[inds,:]
    
    D = tf.reduce_sum(tf.pow(X[:,tf.newaxis,:] - X_sub[tf.newaxis,:,:],2), axis=2)
    
    # Setup op that initializes all the variables
    init = tf.global_variables_initializer()
    
    # Make new session
    sess = tf.Session()
    sess.run(init)
    D_val = sess.run(D, feed_dict= {X:X_val, inds:inds_val})
      
    
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    imshow1 = ax.imshow( D_val , cmap="hot", interpolation = 'nearest' )
    print(D_val.shape )
    
pairDist()