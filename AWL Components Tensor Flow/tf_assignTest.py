# -*- coding: utf-8 -*-

# In this script we are testing to see how the assign function of tf works
# YAY ASSIGNING IS IN FACT A FUNCTION GRAPH OP, CALLING ITS UPDATED REF WILL INFACT
# UPDATE THE ORIGINAL VARIABLE AND ANYTHING ASSOCIATED WITH CALCULATING IT'S 
# VALUE !!!

def test():

    import tensorflow as tf
    import numpy as np
    
    a = tf.Variable(1, name='a')
    
    a_new = a*4
    
    asgn_a = tf.assign(a, a_new)
    
    b = tf.Variable(1, name='a')
    
    b_new = b*4
    
    asgn_b = tf.assign(b, b_new)
    
    asgn_ab = tf.reduce_sum(asgn_a)+tf.reduce_sum(asgn_b)
    
    # Setup op that initializes all the variables
    init = tf.global_variables_initializer()
    
    # Make new session
    sess = tf.Session()
    sess.run(init)
    
#    print(sess.run(a))
#    print(sess.run(asgn_a))
#    print(sess.run(a))
#    print(sess.run(asgn_a))
#    print(sess.run(a))

    print('a='+str(sess.run(a)))
    print('b='+str(sess.run(b)))
    
    print('Combined Update')
    sess.run(asgn_ab)
    
    print('a='+str(sess.run(a)))
    print('b='+str(sess.run(a)))
    
test()

# This will test if the multiple values are returned in a list
# AWESOME SAUCE WE CAN DO THIS!!!!!!!!
def multiReturn():
    
    return 1, 2, 3, 4, 5

def unpack():
    
    l = multiReturn()
    
    print(l)
    
unpack()
    
    