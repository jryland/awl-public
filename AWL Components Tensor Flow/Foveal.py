# -*- coding: utf-8 -*-
"""
Created on Fri Jul 22 18:26:04 2016

@author: james


This files contains helper functions and classes for adjusting and processing 
foveated data streams. It additionally hase functions for generating test data
to better understand how higher level neural representations map visual
features such as oreintation, retinal position, and spatial frequency.


"""


#from scipy import ndimage
import numpy as np
import scipy as sp
from scipy import ndimage as nd
from scipy import interpolate as inter
from scipy import misc
import matplotlib.pyplot as plt


"""
Class for pulling multi-scale/constant rez blocks
These blocks are designed to loosely mimick the relation between spatial 
frequency sensitivity, sensor density, and field of view found in mamalian 
vision systems.
    X
   /______
  /      /
 /______/__ Y
 |     |  
 |     | /
 |_____|/
 | 
Scale/FoV

All images paired with pixel locations to standardize the nescessary
Interpolations. This is just an example.
               Image
  -.5 |------------------| +.5
            Interp
              Pyramid
    -1/4 |-----------| +1/4    
       -1/8 |----| +1/8
        -1/6 |-| +1/16
              |
             \/
             c

Additionally this class will maintain a fixation history in order to mimick
the return inhibition behavior found in humans and other mamals. Essentially
the currenlty sampled scale pyramid/block will be used to generate a saliency
map for fairly typical blob detection to direct the next fixation, biased by 
previous fixations.

--Currently only designed to work in one channel, Color will be added soon

"""
class FixationManager:
    
    def __init__(self, rez, spatialCycles, numScales, scaleFactor, scaleShift, returnInhibitDecay, col="full", scaleDown = [], topFov = .75):
        
        self.col = col  # "lum", "op", "lum+op", "full"
        
        print(self.col)
        
        # Make dummy image
        self.sourceIm = ColorProcess(np.random.rand(500,500, 3), setting=self.col)
        self.colNum = self.sourceIm.shape[2]
        
        
        # Fill out dummy values
        self.numScales = numScales
        self.rez = rez
        self.scaleDown = scaleDown
        self.cycles = spatialCycles
        self.scaleShift = scaleShift
        self.attSmooth = 5
        self.boundry = np.ones( (self.sourceIm.shape[0], self.sourceIm.shape[1]) )
        self.scaleBlock = np.random.rand(rez,rez,numScales, self.colNum)    # This is the current multi-scale view of the image
        self.boundBlock = np.ones_like(self.scaleBlock)                     # This is the interpolated boundry
        self.scaleBlockCyc = np.zeros_like(self.scaleBlock)                 # restricted by cycles per image
        self.scaleBlockMsk = np.zeros_like(self.scaleBlock)                 # restricted by scale focus
        self.saliBlock = np.zeros_like(self.scaleBlock)                     # This is a saliency representation
        self.saliBlockInh = np.zeros_like(self.scaleBlock)
        
        self.focusScale = 0
        self.returnInhibitDecay = returnInhibitDecay
        
        # Alternate return inhibition method
        # Store inhibition maps at different scales
        # Image shape X scales
        self.inhibMap = []
        
        self.fixCent = [0,0]        
        
        # input image space coordinates
        self.xImCoords = np.linspace(-.5, .5, 500)
        self.yImCoords = np.linspace(-.5, .5, 500)
        self.XimCoords, self.YimCoords = np.meshgrid(self.xImCoords,self.yImCoords)
        
        # multi-scale coordinates
        xBaseCoords = np.linspace(-topFov, topFov, rez)
        yBaseCoords = np.linspace(-topFov, topFov, rez)
        Xnew, Ynew = np.meshgrid(xBaseCoords, yBaseCoords)
        self.Xpyr = Xnew[:,:,np.newaxis]
        self.Ypyr = Ynew[:,:,np.newaxis]
        self.Spyr = np.ones_like(self.Xpyr)
        self.Snew = np.ones_like(Xnew)
        
        self.scaleFact = scaleFactor
        self.scales = [1]
        csf = 1
        for sc in range(0,self.numScales-1):
            csf = csf*self.scaleFact
            self.scales.append(csf)
            self.Xpyr = np.concatenate([ self.Xpyr,Xnew[:,:,np.newaxis]/csf], axis=2)
            self.Ypyr = np.concatenate([ self.Ypyr,Ynew[:,:,np.newaxis]/csf], axis=2)
        self.scales = np.array(self.scales)
        self.Spyr = np.ones_like(self.Xpyr)*np.arange(self.numScales)[np.newaxis,np.newaxis,:]        
        
        # Setup the first interpolation function
        self.imInterFun = []
        self.setInterpFuns()
        self.interpEval()


    # Set a new source image and setup new coordinates for the source image        
    def newSource(self, newIm):
        
        if self.scaleDown!=[]:
            newIm = ScaleIm(newIm, self.scaleDown)
        
        self.sourceIm = ColorProcess(newIm, setting=self.col)
        self.boundry = ZeroBorder(np.ones_like(self.sourceIm))[:,:,0]
        self.inhibMap = np.zeros( (self.sourceIm.shape[0],self.sourceIm.shape[1], self.numScales) )
        
        ratio = newIm.shape[1]/newIm.shape[0]
        
        self.fixCent = np.array([0,.0])
        
        self.xImCoords = np.linspace(-.5, .5, newIm.shape[0])/ratio
        self.yImCoords =  np.linspace(-.5, .5, newIm.shape[1])
        self.XimCoords, self.YimCoords = np.meshgrid(self.yImCoords,self.xImCoords)
        
        self.setInterpFuns()
        
        # Required so that nextFixation will work
        self.calcScaleBlock()
        
    # Set the interpolation functions for the source
    def setInterpFuns(self):
        
        self.imInterFun = []
        for c in range(0, self.colNum):
            self.imInterFun.append(inter.RectBivariateSpline( self.xImCoords, self.yImCoords, self.sourceIm[:,:,c]))
        self.bdInterFun = inter.RectBivariateSpline( self.xImCoords, self.yImCoords, self.boundry)
    
    # Grab a resampling using the interp functions
    def interpEval(self):
        
        for c in range(0, self.colNum):
            self.scaleBlock[:,:,:,c]    = self.imInterFun[c].ev(self.Xpyr+self.fixCent[1], self.Ypyr+self.fixCent[0])
            
        self.boundBlock = self.bdInterFun.ev(self.Xpyr+self.fixCent[1], self.Ypyr+self.fixCent[0])
    
    
    # update scaleblock at current position
    # no modifiers applied
    def calcScaleBlock(self):
        
        # Calculate the raw scale-block
        self.interpEval()
        
        # Calculate the un-masked
        self.scaleBlockCyc = fovealDOG( self.scaleBlock, self.rez/self.cycles, self.scaleFact)
        self.scaleBlockCyc = self.scaleBlockCyc*(self.boundBlock[:,:,:,np.newaxis]>.1)
    
        #   Calculate the finished product that should be returned
        scaleShroud = self.getScaleShroud(self.focusScale, shift=self.scaleShift)
        self.scaleBlockMsk = self.scaleBlockCyc*scaleShroud[np.newaxis,np.newaxis,:,np.newaxis]
    
        #self.saliBlock = np.power(self.scaleBlockCyc, 2)
        sigma=self.rez/self.cycles*self.attSmooth
        order = 0
        self.saliBlock = np.abs(np.sum(self.scaleBlockCyc, axis=3))
        self.saliBlock = nd.gaussian_filter(self.saliBlock, (sigma, sigma, 0), order, mode = 'nearest')
        self.saliBlock = self.saliBlock*(self.boundBlock>.1)
        
    # Find the next location of fixation
    def nextFixation(self, noiseLevel = 0):
        
        # Assumed that calc scaleBlock has already been called...
        
        # Project current focus down to inhibMap
        D = np.power(np.power(self.Xpyr,2) + np.power(self.Ypyr,2), .5)
        inhGau = np.exp( -np.power(D*self.scales*self.cycles/self.attSmooth, 2)*1 ) #>.1)*2.0
               
        #np.ones( (self.rez,self.rez,self.numScales))
        self.inhibMap[:,:,self.focusScale] = self.inhibMap[:,:,self.focusScale] + self.projectBlock(inhGau[:,:,:,np.newaxis], noShift=True)[:,:,0]
        
        # Create inhibition block from inhibMap
        self.inhiBlock = np.zeros_like(self.saliBlock)
        for i in range(0, self.numScales):
            
            self.inhMapScFun = inter.RectBivariateSpline( self.xImCoords, self.yImCoords, self.inhibMap[:,:,i])
            self.inhiBlock[:,:,i] = self.inhMapScFun.ev(self.Xpyr[:,:,i]+self.fixCent[1], self.Ypyr[:,:,i]+self.fixCent[0])
            
        # Bias SaliBlock with return inhibition...
        self.saliBlockInh = self.saliBlock/(.1+self.inhiBlock)*self.getScaleShroud(self.focusScale)[np.newaxis,np.newaxis,:]
        
        # Multiply by masking noise
        self.saliBlockInh = self.saliBlockInh*(1-noiseLevel)+(noiseLevel)*np.random.uniform(0,1,self.saliBlockInh.shape)
        
        # Find the maximum salience location
        maxInd = np.argmax(self.saliBlockInh.flatten(order='C'))
        maxInds = np.unravel_index(maxInd, self.saliBlockInh.shape, order='C')
        
        
        # Set the new center of fixation
        self.fixCent[0] = self.Ypyr[maxInds]+self.fixCent[0]
        self.fixCent[1] = self.Xpyr[maxInds]+self.fixCent[1]
        self.focusScale = int(self.Spyr[maxInds])
        
        self.calcScaleBlock() #<-Required to generate saliency map
        
        
        
    # This returns a vector that can be used to modify a scale block by a
    # scale mask
    def getScaleShroud(self,scale, shift=0, widthAdj=1):
        # With scale attention applied
        x = np.linspace(0,self.numScales,self.numScales)
        gau = np.exp(-np.power((x-(self.focusScale+shift))/(2*widthAdj),2))       
        return gau
     
    # Returns one of the scale blocks              
    def getScaleBlock(self,blockType = []):
        
        if   blockType=='Raw':
            return self.scaleBlock
        elif blockType=='NoScaleMask':
            return self.scaleBlockCyc
        elif blockType=='Salience':
            return self.saliBlock[:,:,:,np.newaxis]
        elif blockType=='Inhibited':
            return self.saliBlockInh
        else:
            return self.scaleBlockMsk
        
    # this scales down to the cycle frequency
    def getScaleBlockRez(self, targRez, blockType=[], diskMask=False):
        
        ratio = targRez/self.rez
        rezed = ScaleBlockRez(self.getScaleBlock(blockType), ratio)
        return rezed


    # Projects a scale block back onto the image coordinates
    # could be used to help understand direction of attention
    # or for neural net image creation purposes
    def projectBlock(self, someBlock, noShift=False, fullBlock = False, fullShroud = False, fullAttNorm = True):
        
        # Get only scale of interest
        if not fullBlock:
            blankIm = np.zeros_like(self.sourceIm)
            
            # only project current of interest
            pickSc = np.min([self.focusScale+self.scaleShift,  (self.numScales-1)])
            if noShift:
                pickSc = self.focusScale
            xsc = self.Xpyr[0,:,pickSc]+self.fixCent[0]
            ysc = self.Ypyr[:,0,pickSc]+self.fixCent[1]
            
            
            scIm = someBlock[:,:,pickSc,:]
            # Zero out the edges to avoid weird edge effects..
            # should improve to be a pad eventually
            scIm = ZeroBorder(scIm)
            for c in range(0, someBlock.shape[3]):
                pyrInterFun = inter.RectBivariateSpline( xsc, ysc, someBlock[:,:, pickSc, c])        
                blankIm[:,:,c] = blankIm[:,:,c] + pyrInterFun.ev(self.XimCoords, self.YimCoords)
            
            return blankIm
        # Get full block
        else:
            blankIm = np.zeros_like(self.sourceIm)            
            # only project current scale of interest
            scShroud = self.getScaleShroud(self.focusScale, self.scaleShift, widthAdj=.5)
            if not fullShroud:
                scShroud = np.ones_like(scShroud)
            for i in range(0, self.numScales):                
                pickSc = i
                xsc = self.Xpyr[0,:,pickSc]+self.fixCent[0]
                ysc = self.Ypyr[:,0,pickSc]+self.fixCent[1]
                scIm = someBlock[:,:,pickSc]
                # Zero out the edges to avoid weird edge effects..
                # should improve to be a pad eventually
                scIm = ZeroBorder(scIm)
                
                attNorm = 1
                # Account for higher likelyhood of repeating larger scales
                if fullAttNorm: 
                    attNorm = np.power(self.scales[i], 2)
                    
                # Do projections back onto image across color channels
                for c in range(0, someBlock.shape[3]):
                    pyrInterFun = inter.RectBivariateSpline( xsc, ysc, someBlock[:,:, pickSc, c])        
                    blankIm[:,:,c] = blankIm[:,:,c] + pyrInterFun.ev(self.XimCoords, self.YimCoords)*scShroud[i]*attNorm
            
            return blankIm
        
    
        
def FixationManager_test():
    
    rez = 50
    cycles = 20
    scales = 8
    factor = 1.5
    shift = 3
    inhDecay = 1
    
    # Create a fixation manager
    fixMan = FixationManager(rez, cycles, scales, factor, shift, inhDecay, scaleDown = .50)
    
    
    #testIm = misc.imread('../../Hyper Cortical Dev Personal/Image_Sets/Basic_Tests/Test_img_Solo_3.jpg')
    testIm = misc.imread('Test_img_People.jpg')
    
    print(testIm.shape)
    
    testIm = MaxRez(testIm, maxRez=500)         # enforce maximum resolution
    #testIm = np.mean(testIm[:,:,:], axis=2)     # just grab one channel 
    
    print(testIm.shape)
    
    # Set a new image source
    fixMan.newSource(testIm)
    fixMan.nextFixation(noiseLevel=1)          # Throw out first fixation
    createIm = np.zeros_like(fixMan.sourceIm)   # This one has been scaled down    
    
    # Get current fixation
    scaleBlock = fixMan.getScaleBlock('NoScaleMask')
    saliBlock = fixMan.getScaleBlock('Salience')
    maskBlock = fixMan.getScaleBlock()
    
    # Graphics     
    plt.ion()            
    
    figA = True
    figB = True
    if (figA):
        fig1 = plt.figure()   
    
        ax2 = fig1.add_subplot(131)
        ax2.imshow(scaleBlock_to_img(scaleBlock))          
        ax2.axis('off')
        
        ax3 = fig1.add_subplot(132)
        ax3.imshow(scaleBlock_to_img(maskBlock))          
        ax3.axis('off')
        
        ax4 = fig1.add_subplot(133)
        ax4.imshow(scaleBlock_to_img(saliBlock), cmap=plt.cm.gray)          
        ax4.axis('off')
        
    if (figB):
        fig2 = plt.figure()    
        
        ax1 = fig2.add_subplot(311)
        ax1.imshow(testIm)          
        ax1.axis('off')    
        
        ax5 = fig2.add_subplot(312)
        ax5.imshow(createIm[:,:,0])          
        ax5.axis('off')
        
        ax6 = fig2.add_subplot(313)
        ax6.imshow(fixMan.inhibMap[:,:,0], cmap=plt.cm.gray)          
        ax6.axis('off')

    plt.draw()    
    
    dispInterval = 1    
    
    # Multiple fixations YAY!!!
    for i in range(0,50):
        
        # Get current fixation info
        scaleBlock = fixMan.getScaleBlock('NoScaleMask')
        saliBlock = fixMan.getScaleBlock('Inhibited')
        maskBlock = fixMan.getScaleBlock()
        inhibMap = fixMan.inhibMap[:,:,0:3]+fixMan.inhibMap[:,:,3:6]
        createIm = createIm + fixMan.projectBlock(scaleBlock, fullBlock=True, fullShroud=False, fullAttNorm=True)
        
        # Add current fixation into the new creation image
        #createIm = createIm*.9 + .1*fixMan.projectBlock(scaleBlock)
        if np.mod(i,dispInterval)==0:                
            if(figA):
                # Diplay the current fixation
                ax2.cla()
                ax3.cla()
                ax4.cla()                
                ax2.imshow(scaleBlock_to_img(scaleBlock))
                ax3.imshow(scaleBlock_to_img(maskBlock)) 
                ax4.imshow(scaleBlock_to_img(saliBlock,False)) 
            
            if(figB):
                ax5.cla()
                ax6.cla()
                createImShow = (createIm-np.min(createIm))/(np.max(createIm)-np.min(createIm))
                ax5.imshow(createImShow)
                #ax5.imshow(createIm) 
                ax6.imshow(inhibMap/np.max(inhibMap) ) 
                # Find next fixation
            plt.pause(.1)
            
        fixMan.nextFixation(noiseLevel=.1)
                    

def scaleBlock_to_img(scaleBlock, color=True):
    
    if color:
        
        colIm = np.zeros([scaleBlock.shape[0],scaleBlock.shape[1]*scaleBlock.shape[2], 3])
        convert = np.reshape(scaleBlock, [scaleBlock.shape[0],scaleBlock.shape[1]*scaleBlock.shape[2], scaleBlock.shape[3]], order='F')
        colIm[:,:,0:convert.shape[2]] = convert
        
        colIm = (colIm-np.min(colIm))/(np.max(colIm)-np.min(colIm))
        
        return np.transpose(colIm, [1, 0 ,2])
        
    else:
        return np.transpose(np.reshape(scaleBlock, [scaleBlock.shape[0],scaleBlock.shape[1]*scaleBlock.shape[2] ], order='F'))

# Zero Border makes the border of an image zero (Can be very usefull)
def ZeroBorder(im):
    im[0,:,:] = 0
    im[:,0,:] = 0
    im[(im.shape[0]-1),:,:] = 0
    im[:,(im.shape[1]-1),:] = 0
    return im

# Reduce image size automatically for speed
def MaxRez(im, maxRez):
    
    largestDim = np.max(im.shape[0:1])
    ratio = maxRez/largestDim
    
    if ratio<1.0:
        imNew = nd.zoom(im[:,:,0], ratio)[:,:,np.newaxis]
        for c in range(1,im.shape[2]):
            imNew = np.concatenate( [imNew, nd.zoom(im[:,:,c], ratio)[:,:,np.newaxis] ], axis=2)
        print('SIZED DOWN')
        
        return imNew
    else:
        return im
    
# Scale image across color channels
def ScaleIm(im, ratio):
    
    imNew = nd.zoom(im[:,:,0], ratio)[:,:,np.newaxis]
    for c in range(1,im.shape[2]):
        imNew = np.concatenate( [imNew, nd.zoom(im[:,:,c], ratio)[:,:,np.newaxis] ], axis=2)
    
    return imNew

# Scale image across color channels
def ScaleBlockRez(im, ratio):
    
    test = nd.zoom(im[:,:,0,0], ratio)
    imNew = np.zeros((test.shape[0],test.shape[1], im.shape[2], im.shape[3]))
    
    for sc in range(0,im.shape[2]):
        for c in range(0,im.shape[3]):
            imNew[:,:,sc,c] = nd.zoom(im[:,:,sc,c], ratio)
    
    return imNew
    
    
    
# Choose a method of color representation
def ColorProcess(im, setting="lum+op"):
    
    # ASSUMED RGB
    #{"lum":1, "op":2, "lum+op":3, "lum+full":4}
    
    if setting=="lum":
        im2 = np.mean(im, axis=2)[:,:,np.newaxis]
    elif setting=="op":
        im2 = np.zeros( (im.shape[0], im.shape[1], 2) )
        im2[:,:,0] =  im[:,:,0] + im[:,:,2] - im[:,:,1] # R+B-G
        im2[:,:,1] =  im[:,:,0] + im[:,:,1] - im[:,:,2] # R+G-B
    elif setting=="lum+op":
        im2 = np.zeros( (im.shape[0], im.shape[1], 3) )
        im2[:,:,0] =  np.mean(im, axis=2)                 # Luminance
        im2[:,:,1] =  im[:,:,0] + im[:,:,1] - im[:,:,2] # R+G-B   
        im2[:,:,2] =  im[:,:,0] + im[:,:,2] - im[:,:,1] # R+B-G
    elif setting=="full":
        im2=im
    
    mins = np.min(np.min(im2,0),0)[np.newaxis,np.newaxis,:]
    maxs = np.max(np.max(im2,0),0)[np.newaxis,np.newaxis,:]
    
    im2 = (im2-mins)/(maxs-mins)
    
    return im2

# This performes a difference of gaussians filter on a foveal stack of data
def fovealDOG( fovealStack, width, scaling, scaleNorm = True):
    
    # fovealStack is a 3D matrix where every plane is an image at a particular
    # fov.
    
    
    ''' using two step 1D gaussian filtering: Very fast'''
    
    sigma = 1*width    
    order = 0
    fovealStackFilt1 = nd.gaussian_filter(fovealStack, (sigma, sigma, 0, 0), order, mode = 'nearest')
    
    sigma = 2*width
    fovealStackFilt2 = nd.gaussian_filter(fovealStack, (sigma, sigma, 0, 0), order, mode = 'nearest')
    
    fovDog = fovealStackFilt1-fovealStackFilt2

    depth = fovDog.shape[2]
      
    if scaleNorm:
        #area^scaleNum    
        magNorm = np.power(pow(scaling,.75), np.linspace(1,depth,depth)) 
        magNorm = magNorm[np.newaxis,np.newaxis,:,np.newaxis]
        magNorm = np.flip(magNorm,2)
        #Broadcast division
        fovDog = fovDog/magNorm
        
    return fovDog

def diskMaskedFovealDOG( fovealStack, width, scaling, scaleNorm=False  ):
    
    # Add color channel if nescessary
    if len(fovealStack.shape)==3:    
        fovealStack = fovealStack[:,:,:,np.newaxis]    
    
    fovDog = fovealDOG(fovealStack, width, scaling, scaleNorm=scaleNorm)
    
    # restrict view to disk
    #inDisk, maskInds = makeDiskMask(fovDog.shape[0], fovDog.shape[2])
    
    # values not in disk set to 0
    #fovDog = fovDog*inDisk
    
    # only the values in the disk
    #fovDogVec = np.reshape(fovDog , (np.prod(fovDog.shape),1), order = 'F' )
    #maskFovDog = fovDogVec[maskInds]
    
    fovDog, maskFovDog = applyDisk(fovDog)    
    
    return fovDog, maskFovDog #[:,np.newaxis]

def applyDisk(fovDog):
    
    # restrict view to disk
    inDisk, maskInds = makeDiskMask(fovDog.shape[0], fovDog.shape[2])
    
    # values not in disk set to 0
    if len(fovDog.shape)==3:
        fovDog*inDisk
        
        # only the values in the disk
        fovDogVec = np.reshape(fovDog , (np.prod(fovDog.shape),1), order = 'F' )
        maskFovDog = fovDogVec[maskInds]
            
        return fovDog, maskFovDog[:,np.newaxis]
        
    else:
        fovDog*inDisk[:,:,:,np.newaxis]
        
        # only the values in the disk
        fovDogVec = np.reshape(fovDog , (np.prod(fovDog.shape),1), order = 'F' )
        
        maskInds = np.repeat( maskInds, fovDog.shape[3])
        
        maskFovDog = fovDogVec[maskInds]
            
        return fovDog, maskFovDog
    

def makeDiskMask(rez, depth):
    
    X,Y,D = np.mgrid[ 0:rez, 0:rez, 0:depth]
    
    rad = rez/2+.01
    cent = np.floor(rez/2)
    
    inDisk = (np.power(X-cent,2)+np.power(Y-cent,2))<pow(rad,2)
    
    inDiskVec = np.reshape(inDisk, (rez*rez*depth,1), order='F' )
    
    return inDisk, inDiskVec

import ArrayPerlin as ap
import matplotlib.pyplot as plt


# Test the fovealDog thingy
def TestFovealDog():
    
    rez = 100
    depth = 7    
    scaling = 1.5
    pImg = ap.GetPerlPyramid(rez,depth, noiseType='edge')     
    
    print(pImg.shape)
    
    width = 1.5
    #fDog = fovealDOG(fImg,width, scaling)
    
    dImg, fDogMasked = GetFovealPerlinDog(rez, depth, width = width, noiseType = 'perl', topScale = 1 ) 
    
    dImg = np.reshape(dImg, (rez, rez*depth), order='F')   
    pImg = np.reshape(pImg, (rez, rez*depth), order='F')            
    
    fig = plt.figure()
           
    ax1 = fig.add_subplot(121)
    ax1.imshow(dImg)
    ax1.axis('off')  # clear x- and y-axes
    
    ax2 = fig.add_subplot(122)
    ax2.imshow(pImg)
    ax2.axis('off')  # clear x- and y-axes
    plt.show()
    
    
# Usefull for generating test stimuli or pre-training stimuli    
def GetFovealPerlinDog(rez, depth, width = 1.7, noiseType = 'perl', topScale = 1 ):

    fImg = ap.GetPerlPyramid(rez,depth, scale=topScale, noiseType = noiseType )
    
    scaling = 1.5
    fDog, fDogMasked = diskMaskedFovealDOG(fImg,width, scaling, scaleNorm=True )
    
    #fDogMasked = fDogMasked/(np.max(fDogMasked)-np.min(fDogMasked))
    
    fDogMasked = fDogMasked/np.linalg.norm(fDogMasked)
    
    return fDog, fDogMasked


# Returns a series of colors representing the positions of points
# on the retina
def getMaskedXY(rez, depth):
    
    Xval, Yval = np.mgrid[0:rez,0:rez]
    
    Xval = Xval[:,:,np.newaxis]/rez
    Yval = Yval[:,:,np.newaxis]/rez

    inDisk, maskInds = makeDiskMask(rez, depth)

    Xvec = np.reshape(Xval , (np.prod(Xval.shape),1), order = 'F' )[maskInds,np.newaxis]
    Yvec = np.reshape(Yval , (np.prod(Yval.shape),1), order = 'F' )[maskInds,np.newaxis]
    zro = np.zeros_like(Xvec)
    col = np.squeeze(np.concatenate((Xvec,zro,Yvec), axis=1))
        
    return col

# Create a set of dog stimuli at different xy coords
# this will also contain the associated x and y values
# normed to 0 and 1
# for this method we will automatically assume the use of a disk mask
def StimDogXY( rez, width ):
    
    dogIm, dogVec = diskMaskedFovealDOG( np.zeros((rez, rez, 1, 1) ), 2, width)
    
    GXY = np.zeros( (dogVec.shape[0], rez*rez*2) )
    xVal = np.zeros( (rez*rez)*2 )
    yVal = np.zeros( (rez*rez)*2 )
    xIm = np.zeros( (rez, rez))
    yIm = np.zeros( (rez, rez))
    pointIms = np.zeros( (rez, rez, rez*rez*2),)
    
    i = 0
    
    pol = np.array([1, -1])
    
    
    for x in range(0,rez):
        for y in range(0,rez):
            for p in range(0,2):
                pointIm = np.zeros((rez,rez))
                pointIm[x,y] = 1.0*pol[p]
                dogIm, dogVec = diskMaskedFovealDOG(pointIm[:,:,np.newaxis], width, 1)
                GXY[:,i] = dogVec[:,0]
                xVal[i] = float(x)/float(rez)
                yVal[i] = float(y)/float(rez)
                xIm[x,y] = float(x)/float(rez)
                yIm[x,y] = float(y)/float(rez)
                pointIms[:,:,i] = dogIm[:,:,0,0]
                i = i+1
        
    inDisk, dum = makeDiskMask(rez, 1)
    
    xIm = xIm*inDisk[:,:,0]
    yIm = yIm*inDisk[:,:,0]
    
    return GXY, xVal, yVal, xIm, yIm, pointIms

def testStimDogXY():
    
    GXY, xVal, yVal, xIm, yIm, pointIms = StimDogXY(20, 2)
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    for i in range(0, pointIms.shape[2]):
        ax.cla()
        ax.imshow(pointIms[:,:,i], interpolation = 'nearest')
        plt.pause(.0001)

# Generate bar gratings rotated over 180 degrees
def StimDogOrient( rez, depth, width, bands, phaseSample = 30, degreeSample = 90 ):
    
    dogBlock, dogVec = diskMaskedFovealDOG( np.zeros((rez, rez, depth, 1) ), width, 1)
    
    numSample = degreeSample # 180 for smooth 8 for rough
    numPhase = phaseSample
    
    GO = np.zeros( (dogVec.shape[0], numSample*numPhase+1) )
    oVal = np.zeros( (numSample*numPhase+1) )
    barBlocks = np.zeros( (rez, rez, depth, numSample*numPhase+1) )
    
    i = 0
    
    # Create error value test img
    GO[:,i] = np.zeros( dogVec.shape[0])
    oVal[i] = -1
    i = i + 1
    
    X, Y = np.mgrid[0:rez,0:rez]
    XY = np.concatenate((X[:,:,np.newaxis], Y[:,:,np.newaxis]), axis=2)
        
    # Loop through actual values
    for o in range(0,numSample):
        
        for p in range(0, numPhase):
            
            # Repeated bars
            
            # 1 bar with no nd interpolation applied
            deg = o/numSample*np.pi*1
            dDir = np.array([np.cos(deg), np.sin(deg)])
            
            normDir = dDir/np.linalg.norm(dDir)
            thetaIm = np.sum(XY*normDir[np.newaxis,np.newaxis,:], axis=2)
            
            # band spaced according to width of filter
            thetaIm = thetaIm/(width*4)-(p)/numPhase*(width*1)  #*rez
            barIm = 1/(1+np.exp(np.sin(thetaIm*np.pi)*5))
            
            # Duplicate accross spatial frequency levels
            barBlock = barIm[:,:,np.newaxis]*np.ones((1,1,depth))
            
            #freq = rez/width/4
            #thetaIm = thetaIm/rez-(p*2)/numPhase
            #barIm = np.sin(thetaIm*freq*np.pi)
            
            # Scaling does not matter
            dogBlock, dogVec = diskMaskedFovealDOG(barBlock, width, 1)
            barBlocks[:,:,:,i] = np.squeeze(dogBlock)
            GO[:,i] = dogVec[:,0]
            #oVal[i] = float(o)/float(numSample)  # 
            oVal[i] = np.mod(float(o)/float(numSample)*1, 1)
            i = i+1
    
    inDisk, dum = makeDiskMask(rez, depth)
    
    return GO, oVal, barBlocks

def testStimDogOrient():
    
    GO, oVal, barBlocks = StimDogOrient(21, 5, 2, 2, phaseSample = 10, degreeSample=60)
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    for i in range(0, barBlocks.shape[3]):
        ax.cla()
        ax.imshow(scaleBlock_to_img(barBlocks[:,:,:,i], color = False), interpolation = 'nearest')
        plt.pause(.1)

# Generate stimuli for testing Accentricity and deg sensitivity
# Think of this as a polar version of topology testing with
# the ability to be extended to more than one spatial frequncy 
# To use with perlin synthetic stimuli leave scaling at default
# and topFOV at default
def StimDogAcc(rez, depth, width, scaling = 1.5, topFov = .5):
    
    accSamples = 50
    degSamples = 50
    
    # Figure out what the input is like shape wise
    fImg = ap.GetPerlPyramid(rez,depth, noiseType = 'perl' )
    dogBlock, dogVec = diskMaskedFovealDOG(fImg,width, scaling )
    
    # Get scale-block to img coordinates
    fixMan = FixationManager(rez, rez, depth, scaling, 0, 0, topFov=topFov)
    Xpyr = fixMan.Xpyr # should be centered at 0,0
    Ypyr = fixMan.Ypyr
    
    # Input battery
    GACC = np.zeros( (dogVec.shape[0], rez*rez*2*depth) )
    GDEG = np.zeros( (dogVec.shape[0], rez*rez*2*depth) )
    
    # Val used visualizing neuronal sensitivites later
    accVal = np.zeros( (rez*accSamples)*2 )
    degVal = np.zeros( (rez*degSamples)*2 )
    #These just store imageBlocks to help visualize the input
    #in terms of these variables
    accBlock = np.zeros( (rez, accSamples, depth))
    degBlock = np.zeros( (rez, degSamples, depth))
    
    diskBlocks = np.zeros( (rez, rez, depth, accSamples*2))
    lineBlocks = np.zeros( (rez, rez, depth, degSamples*2))
    
    inDisk, dum = makeDiskMask(rez, depth)
    accBlock = np.sqrt(np.power(Xpyr, 2) + np.power(Ypyr, 2))*2
    accBlock = accBlock*inDisk    
    
    # Convert to polar degree!
    degBlock = np.arctan2(Xpyr,Ypyr)
    
        
    i = 0
    
    pol = np.array([1, -1])
    
    # Create the accentricity test stimuli
    for a in range(0,accSamples):
        for p in range(0,2):
                
            # Make a disk at radius a/rez*.5
            diskBlock = np.sqrt(np.power(Xpyr, 2) + np.power(Ypyr, 2))
            diskBlock = 1/ ( 1+np.exp( (diskBlock-(a/accSamples*.5))*200 ) )
            diskBlock = diskBlock*pol[p]
            
            dogBlock, dogVec = diskMaskedFovealDOG(diskBlock, width, scaling)
            GACC[:,i] = dogVec[:,0]
            accVal[i] = float(a)/float(accSamples)
            
            # use for testing
            diskBlocks[:,:,:,i] = np.squeeze(dogBlock)
            i = i+1
    
    # Create the degree test stimuli
    i = 0
    for d in range(0,degSamples):
        for p in range(0,2):
                
            # Make line
            phiBlock = (degBlock+d/degSamples*2*np.pi)/2
            lineBlock = np.power(np.sin(phiBlock),128)-np.power(np.sin(phiBlock+.5),128)
            lineBlock = lineBlock*pol[p]
            
            dogBlock, dogVec = diskMaskedFovealDOG(lineBlock, width, scaling)
            GDEG[:,i] = dogVec[:,0]
            degVal[i] = float(d)/float(degSamples)
            
            # use for testing
            lineBlocks[:,:,:,i] = np.squeeze(dogBlock)
            i = i+1
    
    # Scale deg block for display
    degBlock = (np.arctan2(Xpyr,Ypyr)+np.pi)/(2*np.pi)
    
    return GACC, accVal, accBlock, diskBlocks, GDEG, degVal, degBlock, lineBlocks

def test_stimDogAcc():
    
    rez = 30
    depth = 5
    width = 2
    GACC, accVal, accBlock, diskBlocks, GDEG, degVal, degBlock, lineBlocks = StimDogAcc(rez, depth, width, scaling = 1.5, topFov = .5)
    
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    
    ax1.imshow(scaleBlock_to_img(accBlock, color=False),interpolation = 'nearest')
    
    for i in range(0, diskBlocks.shape[3]):
        ax2.cla()
        ax2.imshow(scaleBlock_to_img(diskBlocks[:,:,:,i], color=False), interpolation = 'nearest')
        plt.pause(.1)
        
def test_stimDogDeg():
    
    rez = 30
    depth = 5
    width = 2
    GACC, accVal, accBlock, diskBlocks, GDEG, degVal, degBlock, lineBlocks = StimDogAcc(rez, depth, width, scaling = 1.5, topFov = .5)
    
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    
    ax1.imshow(scaleBlock_to_img(degBlock, color=False),interpolation = 'nearest')
    
    for i in range(0, lineBlocks.shape[3]):
        ax2.cla()
        ax2.imshow(scaleBlock_to_img(lineBlocks[:,:,:,i], color=False), interpolation = 'nearest')
        plt.pause(.1)

# Generate test stimuli for spatial frequency sensitivity
def StimDogMag( rez, depth, width, numSamples=30):
        
    dogBlock, dogVec = diskMaskedFovealDOG( np.zeros((rez, rez, depth) ), width, 1)
     
    GM = np.zeros( (dogVec.shape[0], numSamples*2) )
    mVal = np.zeros( (numSamples*2) )
    checkBlocks = np.zeros( (rez, rez, depth,  numSamples*2) )
    mBlock = np.ones( (rez, rez, depth) )
    mBlock = mBlock*np.arange(depth)[np.newaxis,np.newaxis,:]/depth
    
    i = 0
    
    pol = np.array([1, -1])
    
    X, Y = np.mgrid[0:rez,0:rez]
        
    # Loop through actual values
    for m in range(0,numSamples):
        
        for p in range(0, 2):
            
            # Create checker bars 
            checkerIm = np.sin(X/width)+np.sin(Y/width)
            
            # place checker bars at scale level
            gau = np.exp( -np.power( (np.linspace(0, 1, depth)-(m/numSamples))*3, 2) )
            checkBlock =  checkerIm[:,:,np.newaxis] * gau[np.newaxis,np.newaxis,:]*pol[p]
            
            # Scaling does not matter
            dogBlock, dogVec = diskMaskedFovealDOG(checkBlock, width, 1)
            checkBlocks[:,:,:,i] = checkBlock
            GM[:,i] = dogVec[:,0]
            mVal[i] = float(m)/float(numSamples)*1

            i = i+1
    
    inDisk, dum = makeDiskMask(rez, depth)
    
    return GM, mVal, mBlock, checkBlocks

def test_stimDogMag():
    
    rez = 30
    depth = 5
    width = 2
    GM, mVal, mBlock, checkBlocks = StimDogMag(rez, depth, width, numSamples = 30)
    
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    
    ax1.imshow(scaleBlock_to_img(mBlock, color=False),interpolation = 'nearest')
    
    for i in range(0, checkBlocks.shape[3]):
        ax2.cla()
        ax2.imshow(scaleBlock_to_img(checkBlocks[:,:,:,i], color=False), interpolation = 'nearest')
        plt.pause(.1)

#testStimDogXY()
#testStimDogOrient()
#TestFovealDog()
#FixationManager_test()
#test_stimDogAcc()
#test_stimDogDeg()
#test_stimDogMag()