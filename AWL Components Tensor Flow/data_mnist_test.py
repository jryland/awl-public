# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import mnist as mn


def display():
    
    mnistPath = "../Basic Data Sets/MNIST/" 
    
    
    for i in range(0,10):    
        
        
        imgStack_i = mn.load_mnist('training',path = mnistPath, digits=[i], selection=slice(0, 100), return_labels=False)
        
        avgImg_i = imgStack_i.mean(axis = 0)
        
        # Awww yeas Nixie Tubes 
        imgplot = plt.imshow(avgImg_i, cmap="hot")
        plt.axis('off')  # clear x- and y-axes
        plt.show()
        
        print(i) 
    
    
display()