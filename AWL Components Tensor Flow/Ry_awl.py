# -*- coding: utf-8 -*-
"""

Created on Fri Apr 14 15:12:20 2017

@author: James Ryland 

Examples:
    StagedAWEL_Test()


                                SUMMARY
Appropirate Window Learning (AWL) models are a family of deep neural networks
that learn domain appropriate sparsity structures (Windows and Pools) in a
fully data driven fashion. This is done to mimick the developmental stages of
human learning where the gross connection scheme between cortical layers is
greedily learned in a layer by layer fashion. These windows and pools restrict
feature representation to be local (in a sense that is abstracted from the
environment).
    Further, AWL can use self organizing maps such as the kohonen map to better
distribute windows and pools accross an ideal window/pool candidate space.
These models produce feature maps which directly imitate the cortical maps
found in human cortex, while still remaining functional models for inference 
and reconstruction. For an example where an AWL model is used to create a model
of V1 type "StagedAWEL_Test()" after running this file.


                             IDEAL WINDOWS
The AWL framework is built on the theory that windows and pools in human
cortex are largely learned from the envirnment as an additional architectural
constraint to learning. In vision, audition, and haptics, members of a
receptive feild tend to be related to one another in a domain specific way.
For instance input neurons to an A1 receptive window will respond to
similar frequencies/location/temporal-offset .etc. Further, these cortical
inputs are densly sampled accross relavant stimulus parameters. AWL leverages
the fact that neurons in a window will tend to have somewhat related response
properties. One method of crudely measuring this relatedness is to simply
correlate all of the inputs with one another. The rows of such a pairwise 
correlation matrix can actually directly serve as effective receptive windows,
where 0 elements are treated as fully absent connections. When this approach
is applied to images it produces receptive windows that are remarkably similar
to those found in convolutional neural networks which loosely imitate features
of early visual cortex. However this new approach can also be stacked in deep 
neural nets, where each layer decides it's window and pool structure based on 
the statistics of previous layer's representation. This cannot be reproduced 
in any other existing model's currently. Further, this ability seems to be 
central to the flexability of representation found in human cortex. AWL models 
could open a new chapter of research for better understanding the human brain,
and adapting our machine learning algorithms to emulate it better.  



                                MODELS
Staged AWEL: E-AWL: Embedded Kohonen Map AWL
    This model embeds the rows of the pairwise correlation matrix on the input
    units in a low-dimensional space that preserves neighbor relations. A
    Kohonen map is learned on the points in this low-dimensional space in
    order to distribute windows evenly accross the window/pool candidate space.
    Think of this as and advanced application of AWL that can scale linearly
    in terms of layer size. Currently this is the only model that makes use of
    the double short format for representing it's membership matrices. This is
    the recommended AWL model to use in most cases.
    
Staged AWKL: K-AWL: Kohonen Map AWL
    This models learns a kohonen map directly on the rows of the pairwise 
    correlation matrix on the inputs. The points learned will be evenly
    distributed accross the candidate space of ideal windows/pools. Think of
    this as a simple but full application of AWL principles, but that does not
    scale well in terms of the number of allowable units in a model.
    
Simple AWKL: K-AWL: Kohonen Map AWL
    This models learns a kohonen map directly on the rows of the pairwise 
    correlation matrix on the inputs. The points learned will be evenly
    distributed accross the candidate space of ideal windows/pools. However
    this implementation is naive and uses full weight and membership matrices
    for both storage and activation calculations. It is quite slow and scales
    incredibly poorly with layer size. This is largely included as a reference
    not for any serious use. 
    
Staged AWRL: R-AWL: R-Correlation AWL
    This model pulls rows directly from the pairwise correlation matrix on the
    input units in order to create it's window and pool membership matrices. 
    This model does not produce simulated cortical maps. Think of this is a
    very basic application of AWL principles. The current implementation does
    not scale well, but hopefully that will be fixed soon.
    
    
   

List of current functions and classes:
    
    AWEL FUNCTIONS
        StagedAWEL_Test
        StagedAWEL
        StagedAWEL_hidden
        StagedAWEL_pool       
           
    AWKL FUNCTIONS
        StagedAWKL_Test
        StagedAWKL
            StagedAWKL_hidden
            StagedAWKL_pool
        StageClock
            
        SimpleAWKL_Test
        SimpleAWKL
            SimpleAWKL_softplus
            SimpleAWKL_softpool
        LayerClock
        
    AWRL FUNCTIONS
        StagedAWRL_Test
        StagedAWRL
        StagedAWRL_hidden
        StagedAWEL_pool
        awrlClock
       
"""



# Imports for all functions and classes
import tensorflow as tf
import numpy as np
import Lattice as lat
import matplotlib.pyplot as plt

import Ry_BasicMathTF  as bm
import Ry_SOM          as sm
import Ry_WindowLayers as wl
import Ry_LayerGraphs  as lg



"""
_______________________________________________________________________________
_______________________________________________________________________________
_______EMBEDED KOHONEN MAP AWL____________________________________________E-AWL
_______________________________________________________________________________
_______________________________________________________________________________
"""
def StagedAWEL_Test():
    
    # foveal uses array perlin to generate noise stimuli at multiple scales
    # and then creates a foveated representation to input to the network
    import Foveal as fv
    import matplotlib.pyplot as plt
    
    # Define the stimulus generators size and dimensions
    rez = 20 #41
    depth = 5
    width = 1.2
    topScale = 1
    # Find the size 
    fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, width, topScale=topScale)
    inSize = fDogMasked.shape[0]
    
    # Define the input to the Network
    s = tf.placeholder(tf.float32, shape = (inSize, 1))
    
    
    # Create a StagedAWEL module
    hidSize = round(inSize*3)
    outSize = round(hidSize*.8)
    bufLength = 300
    dims = [80, 80]             # The dimesnionality of the windowed corr space/ seems to need to increase as cortical sheet size increases
    spreadHalfs  = [200, 200]   # How long it takes for the spread size to halve
    spreadMins   = [  0,  0]    # The miniimum spread value/ zero should be a default... # there may be something wrong here?
    spreadStarts = [  1,  1]    # Initial size of the spread
    cutoffs = [0, 0]            # Membership values at which to abreviate the membership matrices
    maxWinds = [100, 100]       # The maximum number of units in the windows and pools
    shapeTs = [2000, 3000]      # How long the embedding takes to go from neighbor biased to normal OBJ
    exclusiveness = True        # Whether the pools are mostly non-overlapping or not
    poolAdj = .0001    #.0001   # Ajustment for how exlusive the pools are
    hidUpdaters, poolUpdaters, Lh, Lq, U,V,h,q, buffUp, W_h, W_q = StagedAWEL(s, inSize, hidSize, outSize, bufLength, dims, spreadHalfs, spreadStarts, spreadMins, cutoffs, maxWinds, shapeTs=shapeTs, exclusiveness=exclusiveness, poolAdj=poolAdj)
    
    # Print net statistics
    print('     in:'+str(inSize))
    print(' hidden:'+str(hidSize))
    print('   pool:'+str(outSize))
    
    
    # Put updaters in a list for layer clock to use
    #op_all = [hidUpdaters,poolUpdaters]
    #eOnlys = np.array([2000, 3000]) # steps for layers to learn embedding
    #wOnlys = np.array([6000, 6000]) # steps for layers to learn kohonen windows
    #rOnlys = np.array([2000, 0   ]) # steps for reconstructive learning
    
    # Pool by sheet
    op_all = [hidUpdaters]
    eOnlys = np.array([2000]) # steps for layers to learn embedding
    wOnlys = np.array([6000]) # steps for layers to learn kohonen windows
    rOnlys = np.array([2000]) # steps for reconstructive learning

    lRateE = .5      # Learning rate for embedding
    lRateW = .1      # Learning rate for window learning
    lRateF = .01     # Learning rate for feature learning 
    upWPeriod = 1000 # how often to recalculate W membership and indices (Very expensive for larger inputs)
    cleanUp = 200    # Period of time to only update W membership
    # Create stage clock, this automatically creates optimizers for AWEL layers
    # and chooses the correct operations to run during the training phases
    sClock = StageClock( 'AWEL', op_all, eOnlys, wOnlys, rOnlys, lRateE, lRateW, lRateF, upWPeriod=upWPeriod, cleanUp=cleanUp)
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Create Basic Visiualization of h and q and s image
    sizeAdj = [.05, .05]
    plt.show()
    actPanel = lg.ActivityPanel(Lh, Lq, sizeAdj = sizeAdj, raster=False, rasterRez=500)
    topoPanel1 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj, raster=True, rasterRez=500, accDeg=True)
    topoPanel2 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj, raster=True, rasterRez=500)
    topoPanel3 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj, raster=True, rasterRez=500)
    windPanel = lg.WindowPanel()
    
    
    # Noise type
    noiseType = 'perl'
    
    t = 0
    # Fill Buffers
    for i in range(0, bufLength):
        
        # Create Stimulus
        fDog, s_val = fv.GetFovealPerlinDog(rez, depth, width, noiseType = noiseType, topScale=topScale)
        
        feed_dict = {s:s_val}
        
        # Run Models
        h_val, q_val, Wh_val, Wq_val, dummy = sess.run([h, q, W_h, W_q, buffUp], feed_dict=feed_dict)
        
    
    # Train model
    while( not sClock.done):
        
        # Create stimulus
        fDog, s_val = fv.GetFovealPerlinDog(rez, depth, width, noiseType = noiseType, topScale=topScale)
        
        feed_dict = {s:s_val}
        
        # Run Model
        ops_to_eval = sClock.addStageOps( [h, q, W_h, W_q, buffUp] )
        vals = sess.run(ops_to_eval, feed_dict=feed_dict)
        
        # Retrieve Values
        h_val = vals[0]
        q_val = vals[1]
        obj_val = vals[5]
        Wh_val = vals[2]
        Wq_val = vals[3]
        
        # update model ticker
        t = t+1
        sClock.tPlus1()
        
        mode = sClock.addStageOps([], returnModeStr=True,abrev=True)
        
        if mode=='emb':
            if np.mod(t, 100)==0:
                print('ln_emb('+'t='+str(sClock.t) +') = '+str(obj_val))
        
        else:
            if np.mod(t, 100)==0:
                # Update activity panel
                im = np.reshape(fDog, (rez, rez*depth), order='F')
                actPanel.plotActivity(im, h_val, q_val)
                print('\nt='+str(sClock.t)) 
                print('ln = '+str(obj_val))
                sClock.displayStageInfo()
                print('\ntVals')
                print('Min(h) = '+str(np.min(h_val)))
                print('Avg(h) = '+str(np.mean(h_val)))
                print('Max(h) = '+str(np.max(h_val)))
                print('Min(q) = '+str(np.min(q_val)))
                print('Max(q) = '+str(np.max(q_val)))
                
                plt.pause(.01)
            
            
            # Check Retonotopy
            if np.mod(t, 1000)==0:
                # Check XY Topology
                if depth == 1:
                    GXY, xVal, yVal, xIm, yIm, dum = fv.StimDogXY(rez, width)
                    H = np.zeros( (hidSize,  GXY.shape[1]) ) 
                    Q = np.zeros( (outSize,  GXY.shape[1]) )
                    for i in range(0, GXY.shape[1]):
                        # only calculate forward operations
                        s_val = GXY[:, i, np.newaxis]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        H[:, i] = h_val[:,0]
                        Q[:, i] = q_val[:,0]
                        
                    topoPanel1.plotTopoXY(rez, H, Q, xVal, yVal, xIm, yIm)
                # Ceck polar accentricity and degree
                else:
                    GACC, accVal, accBlock, diskBlocks, GDEG, degVal, degBlock, lineBlocks = fv.StimDogAcc(rez, depth, width, scaling = 1.5, topFov = .5)
                    accH = np.zeros( (hidSize,  GACC.shape[1]) ) 
                    accQ = np.zeros( (outSize,  GACC.shape[1]) )
                    for i in range(0, GACC.shape[1]):
                        # only calculate forward operations
                        s_val = GACC[:, i, np.newaxis]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        accH[:, i] = h_val[:,0]
                        accQ[:, i] = q_val[:,0]

                    degH = np.zeros( (hidSize,  GDEG.shape[1]) ) 
                    degQ = np.zeros( (outSize,  GDEG.shape[1]) )
                    for i in range(0, GACC.shape[1]):
                        # only calculate forward operations
                        s_val = GDEG[:, i, np.newaxis]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        degH[:, i] = h_val[:,0]
                        degQ[:, i] = q_val[:,0]
                        
                    topoPanel1.plotTopoAcc(rez, accH, accQ, accVal, accBlock, degH, degQ, degVal, degBlock)
                        
            # Check Oreintation sensitivity  
            if np.mod(t, 1000)==0:
                GO, oVal, oIm = fv.StimDogOrient(rez, depth, width, 2, phaseSample=10, degreeSample=60)
                H = np.zeros( (hidSize,  GO.shape[1]) ) 
                Q = np.zeros( (outSize,  GO.shape[1]) )
                for i in range(0, GO.shape[1]):
                    # only calculate forward operations
                    s_val = GO[:, i, np.newaxis]
                    # buffer empty, no need for hypers
                    feed_dict = {s:s_val}
                    h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                    # activations for all stimuli...
                    H[:, i] = h_val[:,0]
                    Q[:, i] = q_val[:,0]
                    
                topoPanel2.plotTopoOR(rez, H, Q, oVal, oIm, circleMean=True)
                
            # Check magnification/spatial frequency sensitivity
            if np.mod(t, 1000)==0:
                GM, mVal, mBlock, checkBlocks = fv.StimDogMag(rez, depth, width, numSamples=30)
                H = np.zeros( (hidSize,  GM.shape[1]) ) 
                Q = np.zeros( (outSize,  GM.shape[1]) )
                for i in range(0, GM.shape[1]):
                    # only calculate forward operations
                    s_val = GM[:, i, np.newaxis]
                    # buffer empty, no need for hypers
                    feed_dict = {s:s_val}
                    h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                    # activations for all stimuli...
                    H[:, i] = h_val[:,0]
                    Q[:, i] = q_val[:,0]
                    
                topoPanel3.plotTopoMag(rez, H, Q, mVal, mBlock)
                
                
            # Examine window membership matrices
            if np.mod(t, 500)==0:
                windPanel.plotHist(Wh_val, Wq_val)
                

                
                
"""
___AWEL MODEL OUTLINE_________________________________________________________

Function Hiarchies:   

Forward:
            q
           / \
          h  [P]
         / \
        s  [W*F]

Backward:
              l_rec1
             /
            s_prime
           / \
          h  [WT*U]
         / \
        s  [W*F]
        
              l_rec2
             /
            h_prime
           / \
          q  [WT*V]
         / \
        h  [W*P]

Windows:
               
                           W
                   l_wind /
                  /     \/ 
          l_emb  /      Y
         /     \/
        R2     X


Embedded Kohonen AWL - Relavant Model Variables 
    
    s           input activation pattern
    s_prime     reconstruction of s
    S_buff      buffered activites of s
    
    h           hidden layer activity
    h_prime     hidden layer reconstruction
    H_buff      buffered activities of h
    
    R2          squared pearsond correlation matrix
                one for R2(s) and one for R2(h)          
    
    X           These are the rows of R2 embedded using Correllational Neighbor
                Embedding CNE or C-SNE
    
    spread      the range of influence of neighbors on the kohonen sheet
                must constantly decrease as the sheet is learned
    Y           are the points in the embedding space learned by the kohonen map           
    L           the points on the cortical sheet (these do not change)
    
    W           is a window membership matrix
    W_h         membership used to generate h
    W_q         membership used to generate q
    
    F           Forward feature weight matrix (used to calculate activity)
    U           Backward feature weight matrix (used to calculate reconstruct)
    b           currently disabled
    p           currently disabled

    P           Forward pool matrix (used to calculate pool output)
    V           Backward pool weight matrix (used to calculate reconstruction of pool input)
                    
    l_emb       embedding objective function, one for both layers      
    l_wind      kohonen window objective function, one for both layers
    l_rec       reconstructive objective function, one for both layers
    
    buffUp      this aggregates the update calls for both the s and h buffers


Note: 
    this layer definition has a lot of customization options please look
through them carefully to decide exactly which version you want. Some of the
options can produce radically different behavior. At this moment it is
recommended that you use lateralInhibition, but when using lateral inhibition
potentially avoid using batchNorm this can poduce strange behavior when there
is a large difference in varainces between input units as can happen with raw 
data. Additionally lateral inhibition largely serves a similar purpose to batch
norm.
"""
def StagedAWEL(s, inSize, hidSize, outSize, bufLength, dims, spreadHalfs, spreadStarts, spreadMins, cutoffs, maxWinds, shapeTs=[2000, 2000], exclusiveness=True, poolAdj=.0001, biasUnits=False, batchNorm='none', magNorm=False, lateralInh=('DIV','DIV'), inhWidth = (2,2), poolBySheet=True, sheetSpread = 2, batch=False):
    
    # HIDDEN layer using AWEL learning
    #__________________________________________________________________________
    lo_wind1, l_rec1, ind_asgn1, l_emb1, Lh, F, U, h, s_prime, s_up, W_h = StagedAWEL_hidden(s, inSize, hidSize, bufLength, dims[0], spreadHalfs[0], spreadStarts[0], spreadMins[0], cutoffs[0], maxWinds[0], shapeTs[0], biasUnits, batchNorm, magNorm, batch=batch)
    
    # Lateral Inhibition on the cortical sheet for the hidden units
    if lateralInh[0] == 'DIV' or lateralInh[0] == 'DOG':
        # Only requires a one time calculation of windows
        h = wl.static_windowed_inhibit(h, hidSize, Lh, 25, inhWidth[0], lateralInh[0], batch=batch)
    elif lateralInh[0] == 'DOGDIV':
        h = wl.static_windowed_inhibit(h, hidSize, Lh, 25, inhWidth[0], 'DOG', batch=batch)
        h = wl.static_windowed_inhibit(h, hidSize, Lh, 25, inhWidth[0], 'DIV', batch=batch)
    
    # Make a list objective functions and assignment variables for use with StageClock 
    hidUpdaters = [lo_wind1, l_rec1, ind_asgn1, l_emb1]
    
    #POOL using AWEL learning
    #__________________________________________________________________________
    if not poolBySheet:
        
        # create awel pooling layer
        lo_wind2, l_rec2, ind_asgn2, l_emb2, Lq, P, V, q, h_prime, h_up, W_p = StagedAWEL_pool(h, hidSize, outSize, bufLength, dims[1], spreadHalfs[1], spreadStarts[1], spreadMins[1], cutoffs[1], maxWinds[1], shapeTs[1], exclusiveness, poolAdj, batchNorm, magNorm, batch=batch)
        
        # Lateral Inhibition on the cortical sheet for the pools
        if lateralInh[1] == 'DIV' or lateralInh[1] == 'DOG':
            # Only requires a one time calculation
            q = wl.static_windowed_inhibit(q, outSize, Lq, 25, inhWidth[1], lateralInh[1], batch=batch)
            
        # List of objective function and assignment vairables
        poolUpdaters = [lo_wind2, l_rec2, ind_asgn2, l_emb2]
        
        # Tigger buffer filling
        buffUp = tf.reduce_sum(s_up) + tf.reduce_sum(h_up)
        
        # Return updaters, reverse weights, activations,
        return hidUpdaters, poolUpdaters, Lh, Lq,  U, V, h, q, buffUp, W_h, W_p
    
    #POOL using CORTICAL SHEET
    #__________________________________________________________________________
    else:
        
        # Create sheet pooling layer
        q, Lq, h_up, l_rec2 = Sheet_pool(h, hidSize, outSize, bufLength, Lh, 50, sheetSpread, batchNorm=batchNorm, magNorm=magNorm, batch=batch)
        
        # Lateral Inhibition on the cortical sheet for the pools
        if lateralInh[1] == 'DIV' or lateralInh[1] == 'DOG':
            # Only requires a one time calculation
            q = wl.static_windowed_inhibit(q, outSize, Lq, 25, inhWidth[1], lateralInh[1], batch=batch)
        
        # Add pooling layer reconstruction objective to hidden layer reconstruction
        # This will only update the feature layer... currently
        hidUpdaters[1] = hidUpdaters[1]+l_rec2 
        
        # Tigger buffer filling
        buffUp = tf.reduce_sum(s_up) + tf.reduce_sum(h_up)
        
        # Return duplicates of h variables for var related to q no longer calculated
        # For better integration with code written for standard AWEL
        # Return updaters, reverse weights, activations,
        # Change this later because it is confusing af..
        return hidUpdaters, hidUpdaters, Lh, Lq,  U, U, h, q, buffUp, W_h, W_h


# Setup reconstructive AWEL module
# Fairly basic honestly
# Only using batch
def Staged_AWEL_rec(q, U, V, Lh, Lq, poolBySheet=True, sheetSpread = 2):

    # Use AWEL learned pools
    if not poolBySheet:
        'Glibble'        
    
    # Use sheet defined pools
    else:
        'Globble'
    
    
    return 'lol'
    
        
    
def Sheet_pool(h, hidSize, outSize, bufLength, Lh, keep, width, batchNorm=False, magNorm=False, batch=False):
    
    # Create buffered hidden representation purely for batchNorming
        h_up, H_buff, H_buff_zero = bm.addActivityBuffer(h, [hidSize, 1], bufLength, batch=batch)
        
        # Create option to batch norm s using S_buff add the updater to s_up
        if batchNorm=='hidden' or batchNorm=='both':
            h, bnorm_up = bm.bufferNorm(h, [hidSize, 1], H_buff, .99)
            h=h/hidSize
            #s=s/np.sqrt(inSize)
            h_up = tf.reduce_sum(h_up)+tf.reduce_sum(bnorm_up)
    
        # Create option center and magnitude normalize by current activity
        if magNorm:
            h_cent, h_mag = tf.nn.moments(h, axes=[0])
            h = (h-h_cent[tf.newaxis])/h_mag[tf.newaxis]
            
            
        # Create cortical lattice for the pooling layer
        Lq = lat.makeLattice('hex', 'circle', outSize, True)
        
        # Create pooling layer based on cortical sheets
        q = wl.static_windowed_pool(h, hidSize, outSize, Lh, Lq, 50, 1, batch=batch)
        
        # Create reconstructive layer
        h_prime = wl.static_windowed_pool_rec(q, hidSize, outSize, Lh, Lq, 50, 1, batch=batch)
        
        l_rec_h = tf.reduce_sum(h_prime-h)
        
        return q, Lq, h_up, l_rec_h
        

    
def StagedAWEL_hidden(s, inSize, hidSize, bufLength, dim, spreadHalf, spreadStart, spreadMin, cutoff, maxWind, shapeT, biasUnits=False, batchNorm=False, magNorm=False, batch=False):
    
    # Create buffered input
    s_up, S_buff, S_buff_zero = bm.addActivityBuffer(s, [inSize, 1], bufLength, batch=batch)
    
    # Create option to batch norm s using S_buff add the updater to s_up
    if batchNorm=='input' or batchNorm=='both':
        s, bnorm_up = bm.bufferNorm(s, [inSize, 1], S_buff, .99)
        s=s/inSize
        #s=s/np.sqrt(inSize)
        s_up = s_up+tf.reduce_sum(bnorm_up)
    
    # Create option center and magnitude normalize by current activity
    if magNorm:
        s_cent, s_mag = tf.nn.moments(s, axes=[0])
        s = (s-s_cent[tf.newaxis])/s_mag[tf.newaxis]
        
    # Determine rows to update
    upNum = 200
    rowInds = tf.random_uniform( (upNum, 1), minval=0, maxval=inSize, dtype=tf.int32)
    
    # Calculate rows of R2
    R2_rows = tf.pow( bm.addCorrelationMatrix_Sub( S_buff, rowInds)  , 2)
    
    # Define Correlational Embedding
    nPow = 1      # conversion power on R squared 
    l_emb, X, D, Q = sm.SimpleWindowCorEmbed(R2_rows, rowInds, inSize, dim, nPow=nPow, shapeT=shapeT)
    
    # Create simple Kohonen Mapper w/t auto updating spread
    t = tf.Variable(0.0)
    t_up = tf.assign(t, t+1)
    spread    = tf.maximum( spreadStart/(1.0+(t/spreadHalf) ), spreadMin)
    spread_up = tf.maximum( spreadStart/(1.0+(t_up/spreadHalf) ), spreadMin)
    #       Y: the positions learned by kohonen mapping    
    #       L: the positions on the cortical lattice
    # lo_wind: objective function for kohonen window mapping
    lo_wind, Y, L = sm.SimpleKohonen(tf.stop_gradient(X), hidSize, 10, tf.stop_gradient(spread_up), center = 0)
    
    # Choose representational method for W
    # (mini) still requires a dense W
    # (short) does not require dense W 
    method = 'short'
    if method=='mini':   
        # Method 2 - Partial membership update
        W = tf.Variable( tf.zeros( [hidSize, inSize] ), dtype=tf.float32, name='W_hidden' )
        
        WupNum = 200
        # Rand Update
        #WrowInds = tf.random_uniform( (WupNum, 1), minval=0, maxval=hidSize, dtype=tf.int32)
        # Sequential Update
        inds = tf.Variable(np.arange(WupNum)[:,np.newaxis], dtype=tf.int32)
        inds_up = tf.assign(inds, inds+WupNum )
        WrowInds = tf.to_int32(tf.mod( inds_up, hidSize ))
        
        # Calculate the membership matrices which for this algorithm are called W
        Wcalc = tf.pow(tf.exp( -tf.pow( tf.transpose( bm.addDistanceMatrix_SubY(X,Y, WrowInds) ),2 )), 1/nPow)
        Wasgn = tf.scatter_nd_update(W, WrowInds, Wcalc)
    
        # Create hidden layer
        l_rec, inds_asgn, F, U, h, s_prime, winSize = wl.sparse_windowed_hidden(tf.stop_gradient(W), s, hidSize, inSize, cutoff, maxWind)
        
        inds_asgn = inds_asgn + tf.to_int32( tf.reduce_sum(Wasgn))
        
        return lo_wind, l_rec, inds_asgn, l_emb, L, F, U, h, s_prime, s_up, W[1:30,:] 
    
    else: # method =='short' 
        
        WupNum = 100
        
        # Create a special sparse representation of W
        W_short, cRows, cCols  = wl.addDoubleShort(hidSize, inSize, maxWind, WupNum)
        
        # Calculate updates for double short representation of membership matrix W
        WrowCalc = tf.pow(tf.exp( -tf.pow( tf.transpose( bm.addDistanceMatrix_SubY(X,Y, cRows) ),2 )), 1/nPow)
        WcolCalc = tf.pow(tf.exp( -tf.pow( bm.addDistanceMatrix_SubY(Y,X, cCols),2 )), 1/nPow)
        
        # Remove values below cutoff
        WrowCalc = WrowCalc*tf.to_float(WrowCalc>cutoff)
        WcolCalc = WcolCalc*tf.to_float(WcolCalc>cutoff)
        
        W_short_up = wl.addDoubleShortUpdate(W_short, WrowCalc, WcolCalc)
        
        # Create hidden layer
        F, h = wl.short_windowed_hidden(W_short, s, hidSize, inSize, biasUnits=biasUnits, batch=batch)
        
        # Create hidden layer reconstruction
        U, s_prime = wl.short_windowed_hidden_rec(W_short, h, hidSize, inSize, biasUnits=biasUnits, batch=batch)
        
        # Reconstructive objective
        l_rec = tf.reduce_sum( tf.pow(s - s_prime, 2) ) # + .3*tf.reduce_sum(tf.pow(b,2)) + .3*tf.reduce_sum(tf.pow(p,2))
                    
        inds_asgn = W_short_up
        
        return lo_wind, l_rec, inds_asgn, l_emb, L, F, U, h, s_prime, s_up, WrowCalc 


# REMEMBER THE ERROR YOU MADE LAST TIME DUMMY!!    
def StagedAWEL_pool(h, hidSize, outSize, bufLength, dim, spreadHalf, spreadStart, spreadMin, cutoff, maxWind, shapeT, exclusiveness, poolAdj, batchNorm=False, magNorm=False, batch=False):

    # Create buffered hidden
    h_up, H_buff, H_buff_zero = bm.addActivityBuffer(h, [hidSize, 1], bufLength, batch=batch)
    
    # Create option to batch norm s using S_buff add the updater to s_up
    if batchNorm=='hidden' or batchNorm=='both':
        h, bnorm_up = bm.bufferNorm(h, [hidSize, 1], H_buff, .99)
        h=h/hidSize
        #s=s/np.sqrt(inSize)
        h_up = h_up+tf.reduce_sum(bnorm_up)
    
    # Create option center and magnitude normalize by current activity
    if magNorm:
        h_cent, h_mag = tf.nn.moments(h, axes=[0])
        h = (h-h_cent[tf.newaxis])/h_mag[tf.newaxis]
    
    
     # Determine rows to update
    upNum = 200
    rowInds = tf.random_uniform( (upNum, 1), minval=0, maxval=hidSize, dtype=tf.int32)
    
    # Calculate rows of R2
    R2_rows = tf.pow( bm.addCorrelationMatrix_Sub( H_buff, rowInds)  , 2)
    
    # Define Windowed Correlational Embedding
    nPow = 1      # conversion power on R squared 
    l_emb, X, D, Q = sm.SimpleWindowCorEmbed(R2_rows, rowInds, hidSize, dim, nPow=nPow, shapeT=shapeT)
    
    # Create simple Kohonen Mapper w/t auto updating spread
    t = tf.Variable(0.0)
    t_up = tf.assign(t, t+1)
    spread    = tf.maximum( spreadStart/(1.0+(t/spreadHalf) ), spreadMin)
    spread_up = tf.maximum( spreadStart/(1.0+(t_up/spreadHalf) ), spreadMin)
    #       Y: the positions learned by kohonen mapping    
    #       L: the positions on the cortical lattice
    # lo_wind: objective function for kohonen window mapping    
    lo_wind, Y, L = sm.SimpleKohonen(tf.stop_gradient(X), outSize, 10, tf.stop_gradient(spread_up), center = 0)
    
    # Choose representational method for W
    # (mini) still requires a dense W
    # (short) does not require dense W 
    method = 'short'
    if method == 'mini':
        # Method 2 - Partial membership update
        W = tf.Variable( tf.zeros( [outSize, hidSize] ), dtype=tf.float32, name='W_hidden' )
        WupNum = 200
        # Random Update
        #WrowInds = tf.random_uniform( (WupNum, 1), minval=0, maxval=outSize, dtype=tf.int32)
        # Sequential Update
        inds = tf.Variable(np.arange(WupNum)[:,np.newaxis], dtype=tf.int32)
        inds_up = tf.assign(inds, inds+WupNum )
        WrowInds = tf.to_int32(tf.mod( inds_up, outSize ))
        
        Wcalc = tf.pow(tf.exp( -tf.pow( tf.transpose( bm.addDistanceMatrix_SubY(X,Y, WrowInds) ),2 )), 1/nPow)
        Wasgn = tf.scatter_nd_update(W, WrowInds, Wcalc)
        
        # Create pool layer
        l_rec, inds_asgn, P, V, q, h_prime, poolSize = wl.sparse_windowed_pool(tf.stop_gradient(W), h, outSize, hidSize, cutoff, maxWind, exclusiveness=exclusiveness, poolAdj = poolAdj)
        
        inds_asgn = inds_asgn + tf.to_int32( tf.reduce_sum(Wasgn))
        
        return lo_wind, l_rec, inds_asgn, l_emb, L, P, V, q, h_prime, h_up, W[1:30,:] 
    else:
        WupNum = 100
        
        W_short, cRows, cCols  = wl.addDoubleShort(outSize, hidSize, maxWind, WupNum)
        
        # Calculate updates for double short
        WrowCalc = tf.pow(tf.exp( -tf.pow( tf.transpose( bm.addDistanceMatrix_SubY(X,Y, cRows) ),2 )), 1/nPow)
        WcolCalc = tf.pow(tf.exp( -tf.pow( bm.addDistanceMatrix_SubY(Y,X, cCols),2 )), 1/nPow)
        
        # Remove values below cutoff
        WrowCalc = WrowCalc*tf.to_float(WrowCalc>cutoff)
        WcolCalc = WcolCalc*tf.to_float(WcolCalc>cutoff)
        
        
        W_short_up = wl.addDoubleShortUpdate(W_short, WrowCalc, WcolCalc)
        
        # Create pooling layer
        P_up, P, q = wl.short_windowed_pool(W_short, h, outSize, hidSize, WupNum, exclusiveness, poolAdj, batch=batch)
        
        # Create Pooling layer reconstruction
        V, h_prime = wl.short_windowed_pool_rec(W_short, q, outSize, hidSize, WupNum, exclusiveness, poolAdj, batch=batch)
        
        # Create reconstructive objective function        
        l_rec = tf.reduce_sum( tf.pow(h - h_prime, 2) ) # + .001*tf.reduce_sum(tf.pow(V,2))        
        
        inds_asgn = W_short_up
        if P_up!=[]:
            inds_asgn = inds_asgn+P_up
        
        return lo_wind, l_rec, inds_asgn, l_emb, L, P, V, q, h_prime, h_up, WrowCalc 







"""
_______________________________________________________________________________
_______________________________________________________________________________
_______KOHONEN MAP AWL____________________________________________________K-AWL
_______________________________________________________________________________
_______________________________________________________________________________
"""

def StagedAWKL_Test():
    
    # import array perlin to generate noise stimuli at multiple scales
    import Foveal as fv
    import matplotlib.pyplot as plt
    
    # Define the stimulus generators size and dimensions
    rez = 15 #41
    depth = 1
    width = 1
    topScale = 1
    # Find the size 
    fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, width, topScale=topScale)
    inSize = fDogMasked.shape[0]
    
    # Define the input to the Network
    s = tf.placeholder(tf.float32, shape = (inSize, 1))
    
    
    # Create a StagedAWKL module
    hidSize = round(inSize*1.2)
    outSize = round(hidSize*.8)
    bufLength = 200
    spreadHalfs = [200, 200] # How long it takes for the spread size to halve
    spreadStarts = [1, 1] # Initial size of spread
    cutoffs = [.03, .03] # Rsqr values at which to abreviate the membership matrices
    maxWinds = [100, 100] # The maximum number of units in the windows and pools
    hidUpdaters, poolUpdaters, Lh, Lq, U,V,h,q, buffUp, Y_h, Y_q = StagedAWKL(s, inSize, hidSize, outSize, bufLength, spreadHalfs, spreadStarts, cutoffs, maxWinds)
    
    # Print net statistics
    print('     in:'+str(inSize))
    print(' hidden:'+str(hidSize))
    print('   pool:'+str(outSize))
    
    
    # Put updaters in a list for layer clock to use
    op_all = [hidUpdaters,poolUpdaters] 
    wOnlys = np.array([2000, 2000])
    rOnlys = np.array([1000, 1000])    
    lRateW = .05
    lRateF = .005 # Maybe need to do both at the same time no? maybe?
    upR2Period = 50 # how often to recalculate R-squared input statistics (Very expensive for larger inputs)
    # Create stage clock, this automatically creates optimizers for AWKL layers
    sClock = StageClock( 'AWKL', op_all, 0, wOnlys, rOnlys, 0, lRateW, lRateF, upR2Period)
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Create Basic Visiualization of h and q and s image
    sizeAdj = [.1, .2]
    plt.show()
    actPanel = lg.ActivityPanel(Lh, Lq, sizeAdj = sizeAdj)
    topoPanel1 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj)
    topoPanel2 = lg.TopoPanel(Lh, Lq, sizeAdj = sizeAdj)
    windPanel = lg.WindowPanel()
    
    print("\nAdjust Windows")
    plt.pause(5)    
    
    t = 0
    # Train model
    for i in range(1, 12000):
        
        # Create stimulus
        fDog, s_val = fv.GetFovealPerlinDog(rez, depth, width, noiseType = 'edge', topScale=topScale)
        
        feed_dict = {s:s_val}
        
        # add the current op to evaluate given the time
        h_val = []
        q_val = []
        obj_val = []
        Yh_val = []
        Yq_val = []
        
        # Let the buffer fill before performing updates
        if i<bufLength:
            h_val, q_val, Yh_val, yq_val, dummy = sess.run([h, q, Y_h, Y_q, buffUp], feed_dict=feed_dict)
        
        # Normal operation
        else:
            ops_to_eval = sClock.addStageOps( [h, q, Y_h, Y_q, buffUp] )
            vals = sess.run(ops_to_eval, feed_dict=feed_dict)
            h_val = vals[0]
            q_val = vals[1]
            obj_val = vals[5]
            Yh_val = vals[2]
            Yq_val = vals[3]
            t = t+1
            sClock.tPlus1()
            
            if np.mod(t, 100)==0:
                print('\nt='+str(sClock.t)) 
                print('ln = '+str(obj_val))
                sClock.displayStageInfo()
                print('\ntVals')
                print('Min(h) = '+str(np.min(h_val)))
                print('Avg(h) = '+str(np.mean(h_val)))
                print('Max(h) = '+str(np.max(h_val)))
                print('Min(q) = '+str(np.min(q_val)))
                print('Max(q) = '+str(np.max(q_val)))
                
                im = np.reshape(fDog, (rez, rez*depth), order='F')
                
                actPanel.plotActivity(im, h_val, q_val)
                
                plt.pause(.01)
                
            
            # Check Retonotopy
            if True :
                if np.mod(t, 1000)==0:
                    # Check XY Topology
                    GXY, xVal, yVal, xIm, yIm, dum = fv.StimDogXY(rez, width)
                    H = np.zeros( (hidSize,  GXY.shape[1]) ) 
                    Q = np.zeros( (outSize,  GXY.shape[1]) )
                    for i in range(0, GXY.shape[1]):
                        # only calculate forward operations
                        s_val = GXY[:, i, np.newaxis]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        H[:, i] = h_val[:,0]
                        Q[:, i] = q_val[:,0]
                        
                    topoPanel1.plotTopoXY(rez, H, Q, xVal, yVal, xIm, yIm)
                    
            if True:
                # Check Oreintation maps
                if np.mod(t, 1000)==0:
                    GO, oVal, oIm = fv.StimDogOrient(rez, width, 2, phaseSample=10, degreeSample=60)
                    H = np.zeros( (hidSize,  GO.shape[1]) ) 
                    Q = np.zeros( (outSize,  GO.shape[1]) )
                    for i in range(0, GO.shape[1]):
                        # only calculate forward operations
                        s_val = GO[:, i, np.newaxis]
                        # buffer empty, no need for hypers
                        feed_dict = {s:s_val}
                        h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                        # activations for all stimuli...
                        H[:, i] = h_val[:,0]
                        Q[:, i] = q_val[:,0]
                        
                    topoPanel2.plotTopoOR(rez, H, Q, oVal, oIm)
             
            if True:       
                # Examine window membership matrices
                if np.mod(t, 500)==0:
                    windPanel.plotHist(Yh_val, Yq_val)

        

"""
___AWKL MODEL OUTLINE_________________________________________________________

Function Hiarchies:   

Forward:
            q
           / \
          h  [P]
         / \
        s  [W*F]

Backward:
              l_rec1
             /
            s_prime
           / \
          h  [YT*U]
         / \
        s  [Y*F]
        
              l_rec2
             /
            h_prime
           / \
          q  [PT*V]
         / \
        h  [P]

Windows:
                     W 
             l_wind /
             /    \/
           R2     Y

Kohonen AWL - Relavant Model Variables 
    
    s           input activation pattern
    s_prime     reconstruction of s
    S_buff      buffered activites of s
    
    h           hidden layer activity
    h_prime     hidden layer reconstruction
    H_buff      buffered activities of h
    
    R2          squared pearsond correlation matrix
                one for R2(s) and one for R2(h)          
    
    spread      the range of influence of neighbors on the kohonen sheet
                must constantly decrease as the sheet is learned
    Y           are the points in R2 space learned by the kohonen map           
                Y is used directly as the window membership matrix
    L           the points on the cortical sheet (these do not change)
    
    F           Forward feature weight matrix (used to calculate activity)
    U           Backward feature weight matrix (used to calculate reconstruct)
    b           currently disabled
    p           currently disabled

    P           Forward pool matrix (used to calculate pool output)
    V           Backward pool weight matrix (used to calculate reconstruction of pool input)
        
                
    l_emb       embedding objective function, one for both layers      
    l_wind      kohonen window objective function, one for both layers
    l_rec       reconstructive objective function, one for both layers
    
    buffUp      this aggregates the update calls for both the s and h buffers

"""      
# Add a staged AWKL module
def StagedAWKL(s, inSize, hidSize, outSize, bufLength, spreadHalfs, spreadStarts, cutoffs, maxWinds):
    
    # Create the hidden layer
    lo_wind1, l_rec1, ind_asgn1, R2_asgn1, Lh, F, U, h, s_prime, s_up, Y_h = StagedAWKL_hidden(s, inSize, hidSize, bufLength, spreadHalfs[0], spreadStarts[0], cutoffs[0], maxWinds[0])
    
    # Make a list objective functions and assignment variables for use with StageClock 
    hidUpdaters = (lo_wind1, l_rec1, ind_asgn1, R2_asgn1)
    
    # Create the pool layer
    lo_wind2, l_rec2, ind_asgn2, R2_asgn2, Lq, P, V, q, h_prime, h_up, Y_p = StagedAWKL_pool(h, hidSize, outSize, bufLength, spreadHalfs[1], spreadStarts[1], cutoffs[1], maxWinds[1])
    
    # List of objective function and assignment vairables
    poolUpdaters = (lo_wind2, l_rec2, ind_asgn2, R2_asgn2) #,indR_asgn2)
    
    # Tigger buffer filling
    buffUp = tf.reduce_sum(s_up) + tf.reduce_sum(h_up)
    
    # Return updaters, reverse weights, activations,
    return hidUpdaters, poolUpdaters, Lh, Lq,  U, V, h, q, buffUp, Y_h, Y_p
    
    
    
def StagedAWKL_hidden(s, inSize, hidSize, bufLength, spreadHalf, spreadStart, cutoff, maxWind):
    
    # Create buffered input
    s_up, S_buff, S_buff_zero = bm.addActivityBuffer(s, [inSize, 1], bufLength)
    
    # Create R2 calculations/assignment
    R2      = tf.Variable(tf.zeros( (inSize, inSize)))
    R2_calc = tf.pow(bm.addCorrelationMatrix(S_buff, diag0=True), 2)
    R2_asgn = tf.assign(R2, R2_calc)
    
    # Create simple Kohonen Mapper w/t auto updating spread
    t = tf.Variable(0.0)
    t_up = tf.assign(t, t+1)
    spread    = spreadStart/(1.0+(t/spreadHalf) )
    spread_up = spreadStart/(1.0+(t_up/spreadHalf) )
    lo_wind, Y, L = sm.SimpleKohonen(tf.stop_gradient(R2), hidSize, 10, tf.stop_gradient(spread_up), center = 0)
    
    # Create hidden layer
    l_rec, inds_asgn, F, U, h, s_prime, winSize = wl.sparse_windowed_hidden(tf.stop_gradient(Y), s, hidSize, inSize, cutoff, maxWind)
    
    
    return lo_wind, l_rec, inds_asgn, R2_asgn, L, F, U, h, s_prime, s_up, Y 
    
def StagedAWKL_pool(h, hidSize, outSize, bufLength, spreadHalf, spreadStart, cutoff, maxWind):
    
    # Create buffered hidden
    h_up, H_buff, H_buff_zero = bm.addActivityBuffer(h, [hidSize, 1], bufLength)
    
    # Create R2 calculations/assignment
    R2      = tf.Variable(tf.zeros( [hidSize, hidSize] ))
    R2_calc = tf.pow(bm.addCorrelationMatrix(H_buff, diag0=True), 2)
    R2_asgn = tf.assign(R2, R2_calc)
    
    # Create simple Kohonen Mapper w/t auto updating spread
    t = tf.Variable(0.0)
    t_up = tf.assign(t, t+1)
    spread    = spreadStart/(1.0+(t/spreadHalf) )
    spread_up = spreadStart/(1.0+(t_up/ spreadHalf) )
    lo_wind, Y, L = sm.SimpleKohonen(tf.stop_gradient(R2), outSize, 10, tf.stop_gradient(spread_up), center = 0)
    
    # Create pool layer
    l_rec, inds_asgn, P, V, q, h_prime, poolSize = wl.sparse_windowed_pool(tf.stop_gradient(Y), h, outSize, hidSize, cutoff, maxWind)
    
    
    return lo_wind, l_rec, inds_asgn, R2_asgn, L, P, V, q, h_prime, h_up, Y 




# Manage which type of update will be called and schedule
# window index re-calcs
# Later add a sleep functionality

# Give stage clock the list of forward ops you want to calculate and it will
# dynamically add learning ops depending on what stage of learning the network
# is in!!!
# This way the sess.run() call doesn't have to change in the main loops making
# for much simpler code to read.
# Though this high-level baby-sitter class may make it a little hard for
# first time AWKL and AWEL users to get a handle on what is happening.

class StageClock:
    
    # Give some of these default values
    def __init__(self, typeStr, op_all, layEmbPeriods, layWindPeriods, layFeatPeriods, lRateE, lRateW, lRateF, upR2Period = 50, upWPeriod = 1000, cleanUp = 200):
        # op_all <-
        # AWKL 
        # [ [l_wind1, l_rec1, ind_asgn1, R2_asgn1]
        #   [l_wind2, l_rec2, ind_asgn2, R2_asgn2]
        #
        # AWEL 
        # [ [l_wind1, l_rec1, ind_asgn1, l_emb1]    <-- order is a little different to maximize code reuse
        #   [l_wind2, l_rec2, ind_asgn2, l_emb1]
        
        # check to see that a valid layer type has been specified
        if not (typeStr=='AWKL' or typeStr=='AWEL'):
            print('LAYER TYPE INVALID')
        
        self.typeStr = typeStr
        self.op_all = op_all    
        self.t = 0                      # Global Time
        self.lRateE = lRateE            # learning rate embedding
        self.lRateW = lRateW            # learning rate windows
        self.lRateF = lRateF            # learning rate features
        self.upR2Period = upR2Period    # How often to update R^2 matrices and indices
        self.upWPeriod = upWPeriod      # How often to update W matrices and indices
        self.cleanUp = cleanUp
        
        # Setup clock stages
        self.layEmbPeriods = layEmbPeriods
        self.layWindPeriods = layWindPeriods
        self.layFeatPeriods = layFeatPeriods
        if self.typeStr == 'AWKL':
            self.layEmbPeriods = np.zeros_like(layWindPeriods)
        
        # Determine layer learning end times
        self.layStarts = np.zeros_like(layWindPeriods) 
        t_cur = 0
        self.doneTime = 0
        for lay in range(0, layWindPeriods.shape[0]):
            t_cur = t_cur + self.layEmbPeriods[lay]+self.layWindPeriods[lay]+self.layFeatPeriods[lay]
            if lay<(layWindPeriods.shape[0]-1):
                self.layStarts[lay+1] = t_cur
            else:
                self.doneTime = t_cur
                print(' Steps To Complete: '+str(self.doneTime))
                #print(self.layStarts)
        
        
        # These will hold each version of each optimizer type
        self.embOpts = []
        self.windOpts = []
        self.featOpts = []
        self.windAsgns = [] # <- may need to contain sub lists
        self.R2Asgns = []
        self.basicOpts()
        
        self.done = False
        
    # Set up basic op schedule lists
    def basicOpts(self):
        
        optimizer = tf.train.GradientDescentOptimizer(1)
        
        # Make window optimizer list
        for i in range(0, self.layStarts.shape[0]):
            train = optimizer.minimize( self.lRateW*self.op_all[i][0] )
            self.windOpts.append(train)
            
        # Make the feature optimizer list
        for i in range(0, self.layStarts.shape[0]):
            train = optimizer.minimize( self.lRateF*self.op_all[i][1] )
            self.featOpts.append(train)
            
        # Make the window assignment op list
        for i in range(0, self.layStarts.shape[0]):
            self.windAsgns.append(self.op_all[i][2])
        
        if self.typeStr == 'AWKL':
            # Make the R2 assignment op list for AWKL
            for i in range(0, self.layStarts.shape[0]):
                self.R2Asgns.append(self.op_all[i][3])
        
        else:   # AWEL case
            # Make the Embedding optimizer list
            for i in range(0, self.layStarts.shape[0]):
                train = optimizer.minimize( self.lRateE*self.op_all[i][3] )
                self.embOpts.append(train)
        
   
    
    # Get the current optimizer or assignment ops to be performed this step
    def addStageOps(self, opsToEval, returnModeStr=False, abrev=False):
        
        # Current layer being learned
        cLay, cLayStart, cLayTime = self.getStageInfo(self.t)
        
        modeStr = 'done'
        modeAbr = 'done'
        
        #AWKL VERSION
        
        
        if self.typeStr == 'AWKL':
            
            # Initialize R2 targets and windows before learning
            if cLayTime == 0:
                opsToEval.append( self.op_all[cLay][0] ) # ln_w
                opsToEval.append( self.R2Asgns[cLay])    # re-assign R2      
                opsToEval.append(self.windAsgns[cLay])
                modeStr = 'Layer_init'
                modeAbr = 'wind'
            else:
                
                # Window Learning Phase
                if cLayTime<(self.layWindPeriods[cLay]):
                    
                    # Cycle between adjusting windows and updating R2
                    # This only makes sense for AWKL
                    if not(np.mod(cLayTime,self.upR2Period)==0):
                        opsToEval.append( self.op_all[cLay][0] ) # ln_w
                        opsToEval.append( self.windOpts[cLay] )  # train ln_w
                        modeStr = 'Window Stage - (Grad)'
                    else:
                        opsToEval.append( self.op_all[cLay][0] ) # ln_w
                        opsToEval.append( self.R2Asgns[cLay])    # re-assign R2
                        opsToEval.append( self.windAsgns[cLay])  # re-assign Inds
                        modeStr = 'Window Stage - (R2),(Inds)'
                    modeAbr = 'wind'                         
                
                # Reconstructive Learning Phase
                else:
                    opsToEval.append( self.op_all[cLay][1] ) # ln_rec
                    opsToEval.append( self.featOpts[cLay] )  # train ln_rec
                    modeStr = 'Reconstruct Stage - (Grad)'    
                    modeAbr = 'rec'                         
                
        
        # AWEL method
        else:
            if cLayTime == 0:
                opsToEval.append( self.op_all[cLay][0] ) # ln_w  
                opsToEval.append(self.windAsgns[cLay])
                modeStr = 'Layer_init'
                modeAbr = 'emb'
            else:
                
                # Embedding Phase
                if cLayTime<self.layEmbPeriods[cLay]:
                    opsToEval.append( self.op_all[cLay][3] ) # ln_emb
                    opsToEval.append( self.embOpts[cLay])    # train ln_emb
                    modeStr = 'Embed - (Grad)'
                    modeAbr = 'emb'
                
                # Window Learning Phase
                elif cLayTime<(self.layWindPeriods[cLay]+self.layEmbPeriods[cLay]):    
                    
                    # Normal Window Learning
                    if cLayTime<(self.layWindPeriods[cLay]+self.layEmbPeriods[cLay]-self.cleanUp):
                        
                        # Alternate between window learning and index updating
                        if np.mod(cLayTime,self.upWPeriod)<(self.upWPeriod-self.cleanUp):
                            opsToEval.append( self.op_all[cLay][0] ) # ln_w
                            opsToEval.append( self.windOpts[cLay] )  # train ln_w 
                            modeStr = 'Window - (Grad),(Inds)'
                            modeAbr = 'wind'
                        else:
                            # Cleanup indices
                            opsToEval.append( self.op_all[cLay][0] ) # ln_w
                            opsToEval.append( self.windAsgns[cLay])  # re-assign Inds
                            modeStr = 'Window Cleanup - (Inds)'
                            modeAbr = 'wind'  
                    # Final Cleanup Phase
                    else:
                        # Cleanup indices
                        opsToEval.append( self.op_all[cLay][0] ) # ln_w
                        opsToEval.append( self.windAsgns[cLay])  # re-assign Inds
                        modeStr = 'Window Cleanup - (Inds)'
                        modeAbr = 'wind'
            
                # Reconstructive Phase
                else:
                    opsToEval.append( self.op_all[cLay][1] ) # ln_rec
                    opsToEval.append( self.featOpts[cLay] )  # train ln_rec
                    modeStr = 'Reconstruct - (Grad)'
                    modeAbr = 'rec'
        
        if self.done:
            modeStr = 'done'
        
        if not returnModeStr:
            return opsToEval
        else:
            if not abrev:
                return modeStr
            else:
                return modeAbr
        
    
    def tPlus1(self):
        self.t = self.t + 1
        self.done = not (self.t<=self.doneTime)
            
    
    def getStageInfo(self, t):
        cLay = np.argmax(  self.layStarts* (self.layStarts<=t) )
        cLayStart = self.layStarts[cLay] 
        cLayTime = t-cLayStart
        
        return cLay, cLayStart, cLayTime
    
    # Not correct anymore
    def displayStageInfo(self):
        cLay, cLayStart, cLayTime = self.getStageInfo(self.t)
        print('     Current Layer: '+ str(cLay))
        print('  Layer Start Time: '+ str(cLayStart))
        print('Layer Elapsed Time: '+ str(cLayTime))
        modeStr = self.addStageOps([], returnModeStr = True)
        modeAbr = self.addStageOps([], returnModeStr = True, abrev=True)
        print('        Layer Mode: '+modeStr)
        print('        Abrev Mode: '+modeAbr)

            
        
        
def StageClock_Test():
    
    d = tf.Variable(tf.random_normal([1, 1]), name='dummy')
    
    op_all = [[d,d,d,d],[d,d,d,d], [d,d,d,d]]
    eOnlys = np.array([2, 2, 2])    
    wOnlys = np.array([3, 3, 3])
    rOnlys = np.array([1, 1, 1])    
    #eOnlys = np.array([1, 1, 1])    
    #wOnlys = np.array([1, 1, 1])
    #rOnlys = np.array([1, 1, 1])    
    lRateE = .5
    lRateW = .5
    lRateF = .5
    
    sClock = StageClock('AWKL', op_all, eOnlys, wOnlys, rOnlys, lRateE, lRateW, lRateF, upR2Period = 2)
    
    while not sClock.done:
        
        sClock.addStageOps([])
        
        sClock.displayStageInfo()
        
        sClock.tPlus1()
        print('')
        
    sClock = StageClock('AWEL', op_all, eOnlys, wOnlys, rOnlys, lRateE, lRateW, lRateF)
    
    while not sClock.done:
        
        sClock.addStageOps([])
        
        sClock.displayStageInfo()
        
        sClock.tPlus1()
        print('')
 
        
"""
_______________________________________________________________________________
_____________Simple Kohonen AWL________________________________________________
_______________________________________________________________________________
This is a very basic implementation of the Kohonen AWL model. All parameter
sets are learned at the same time. It requires hyperparameter manager to change
parameters as the model learns called LayerClock. Because of the naive
implementation this model will not scale to large layer sizes. But it will
be a good resource for people simply trying to understand how these models work
without complicating effeciency concerns.
    Further this version uses a lot of methods implemented in numpy rather than
tensor flow. This leads to rather complicated calls to the tensor flow session,
that will grow quite rapidly in complexity as you add more layers to this model
. I strongly recommend you only use the SimpleAWKL function as a tutorial
reference. 

"""

# a reconstructive test of a one layer AWKL net
# also good a reference for how to use AWKL for later
# requires ArrayPerlin to work
def SimpleAWKL_Test():
    
    # import array perlin to generate noise stimuli at multiple scales
    #import ArrayPerlin as ap
    import Foveal as fv
    import matplotlib.pyplot as plt
    
    rez = 11
    depth = 1
    
    # Find the size 
    fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, 1)
    
    inSize = fDogMasked.shape[0]
    
    # Define the Network
    s = tf.placeholder(tf.float32, (inSize, 1))
    
    lClock = LayerClock(1, 2000, 1000, .005, poolSpreadAdj = 1)  # 1 works well for first layer
    hypers = lClock.makeHypers() #initialize placeholder for AWKL hypers
    
    # One AWKL module
    hSize = round(inSize*20)
    qSize = round(hSize*.4)
    ln, q, h, Lq, Lh, h_buff, s_buff = SimpleAWKL(s, qSize , hSize, inSize, hypers, 0)
    
    # Define buffer objects
    buff_lim = 200
    sBuffO = bm.ActivationBuffer(inSize,   buff_lim)
    hBuffO = bm.ActivationBuffer(hSize,    buff_lim)
    
    # Basic tensor flow initializations
    optimizer = tf.train.GradientDescentOptimizer(.05)
    train = optimizer.minimize(ln)
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Create Basic Visiualization of h and q and s image
    
    plt.show()
    sizeAdj = [.1, .4]
    actPanel =   lg.ActivityPanel(Lh, Lq, sizeAdj=sizeAdj)
    topoPanel1 = lg.TopoPanel(Lh, Lq, sizeAdj=sizeAdj)
    topoPanel2 = lg.TopoPanel(Lh, Lq, sizeAdj=sizeAdj)
    
    
    # Train model
    for i in range(1, 24000): #10000):
        
        t = i-buff_lim
        
        # DGP Loop
        fDog, s_val = fv.GetFovealPerlinDog(rez, depth, 1)
        
        feed_dict = {s:s_val, s_buff:sBuffO.getBuff() , h_buff:hBuffO.getBuff() , hypers:lClock.getHypers(t) }
        
        # Compute values
        h_val = []
        q_val = []
        ln_val = []
        if t>buff_lim:
            h_val, q_val, ln_val, dum = sess.run([h, q, ln, train], feed_dict=feed_dict)
        else: 
            h_val, q_val, ln_val = sess.run([h, q, ln], feed_dict=feed_dict)
        
        # Feed Buffers
        sBuffO.push(s_val)
        hBuffO.push(h_val)
        
        if np.mod(t, 100)==0:
            print('\nt='+str(t)) 
            print(ln_val)
            lClock.printStage(t)
            
            im = np.reshape(fDog, (rez, rez*depth), order='F')
            
            actPanel.plotActivity(im, h_val, q_val)
            
            print('\ntVals')
            print('Min(h) = '+str(np.min(h_val)))
            print('Avg(h) = '+str(np.mean(h_val)))
            print('Max(h) = '+str(np.max(h_val)))
            print('Min(q) = '+str(np.min(q_val)))
            print('Max(q) = '+str(np.max(q_val)))
            
            #plt.show()
            #print('\n')
            #print(lClock.getHypers(t))
            plt.pause(.01)
            
        
        # Check Retonotopy
        if True :
            if np.mod(t, 1000)==0:
                # Check XY Topology
                GXY, xVal, yVal, xIm, yIm, dum = fv.StimDogXY(rez, 1)
                H = np.zeros( (h.shape[0],  GXY.shape[1]) ) 
                Q = np.zeros( (q.shape[0],  GXY.shape[1]) )
                for i in range(0, GXY.shape[1]):
                    'print'
                    # only calculate forward operations
                    s_val = GXY[:, i, np.newaxis]
                    # buffer empty, no need for hypers
                    feed_dict = {s:s_val, s_buff:sBuffO.getEmpty() , h_buff:hBuffO.getEmpty() }
                    h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                    # activations for all stimuli...
                    H[:, i] = h_val[:,0]
                    Q[:, i] = q_val[:,0]
                    
                topoPanel1.plotTopoXY(rez, H, Q, xVal, yVal, xIm, yIm)
        
        if (True) :   
            # Check Oreintation  
            if np.mod(t, 1000)==0:
                GO, oVal, oIm = fv.StimDogOrient(rez, 1, 2, phaseSample = rez)
                H = np.zeros( (h.shape[0],  GO.shape[1]) ) 
                Q = np.zeros( (q.shape[0],  GO.shape[1]) )
                for i in range(0, GO.shape[1]):
                    'print'
                    # only calculate forward operations
                    s_val = GO[:, i, np.newaxis]
                    # buffer empty, no need for hypers
                    feed_dict = {s:s_val, s_buff:sBuffO.getEmpty() , h_buff:hBuffO.getEmpty() }
                    h_val, q_val = sess.run([h, q], feed_dict=feed_dict)
                    # activations for all stimuli...
                    H[:, i] = h_val[:,0]
                    Q[:, i] = q_val[:,0]
                    
                topoPanel2.plotTopoOR(rez, H, Q, oVal, oIm)
        

"""
Function Hiarchies:   

Forward:
            q
           / \
          h  [P]
         / \
        s  [W*F]

Backward:
              l_rec1
             /
            s_prime
           / \
          h  [YT*U]
         / \
        s  [Y*F]
        
              l_rec2
             /
            h_prime
           / \
          q  [PT*V]
         / \
        h  [P]

Windows:
          W  l_wind
          \ / \
           Y  R2
Simple Kohonen AWL- Relevant parameters
    
    s           input activation pattern
    s_prime     reconstruction of s
    S_buff      buffered activites of s
    
    h           hidden layer activity
    h_prime     hidden layer reconstruction
    H_buff      buffered activities of h
    
    R2          squared pearsond correlation matrix
                one for R2(s) and one for R2(h)          
    
    spread      the range of influence of neighbors on the kohonen sheet
                must constantly decrease as the sheet is learned
    Y           are the points in R2 space learned by the kohonen map           
                Y is used directly as the window membership matrix
    L           the points on the cortical sheet (these do not change)
                
    
    F           Forward feature weight matrix (used to calculate activity)
    U           Backward feature weight matrix (used to calculate reconstruct)
    b           currently disabled
    p           currently disabled

    P           Forward pool matrix (used to calculate pool output)
    V           Backward pool weight matrix (used to calculate reconstruction of pool input)
              
    
    
    l_emb       embedding objective function, one for both layers      
    l_wind      kohonen window objective function, one for both layers
    l_rec       reconstructive objective function, one for both layers
    
    buffUp      this aggregates the update calls for both the s and h buffers



"""
    
# This is the non-temporal variant of AWKL
def SimpleAWKL(s, poolSize, hidSize, inSize, hypers, moduleInd):
    rec_mod = .1
    
    # Values that need to be unpacked from gammas/spreads/vectors
    hInd = moduleInd*2
    qInd = hInd+1
    g_wp_h      = tf.gather_nd( hypers, [[hInd,0]])
    g_rec_h     = tf.gather_nd( hypers, [[hInd,1]])
    h_spread    = tf.gather_nd( hypers, [[hInd,2]])
    g_wp_q      = tf.gather_nd( hypers, [[qInd,0]])
    g_rec_q     = tf.gather_nd( hypers, [[qInd,1]])
    q_spread    = tf.gather_nd( hypers, [[qInd,2]])
    
    # Define hidden layer    
    ln_rec_h, lo_wind, W, F, U, b, p, h, s_prime, s_buff, Lh = SimpleAWKL_softplus(s, hidSize, inSize, h_spread)
    
    # Define pooling layer
    ln_rec_p, lo_pool, G, P, V, q, h_prime, h_buff, Lq     = SimpleAWKL_softpool(h, poolSize, hidSize, q_spread)
    
    # Global penalty terms
    ln_rec = g_rec_h * (ln_rec_h + .01*tf.reduce_sum(tf.pow(F,2)) + .001*tf.reduce_sum(tf.pow(U,2)) )  +  g_rec_q * (ln_rec_p + .001*tf.reduce_sum(tf.pow(V,2)))
    lo_wp  = g_wp_h*lo_wind  + g_wp_q*lo_pool
    
    # temporarily lowering rec contribution to error
    ln = tf.reduce_sum(ln_rec*rec_mod + lo_wp)
    
    # Need to label all of the returned Values
    return ln, q, h, Lq, Lh, h_buff, s_buff

# Make a non-temporal AWKL hidden layer
def SimpleAWKL_softplus(s, hidSize, inSize, spread ):
    
    # Make s_buff placeholder
    s_buff = tf.placeholder(tf.float32, shape = (s.shape[0], None), name = 's_buff'  )
    
    # Basic Correlation
    Rsqr = tf.pow( bm.addCorrelationMatrix(s_buff, diag0 = True), 2 )
    
    # Basic AWKL Constraint
    lo_wind, Y, L = sm.SimpleKohonen(Rsqr, hidSize, 10, spread, center = 0)
    
    # define windowed softplus layer
    ln_rec, F, U, b, p, h, s_prime  = wl.windowed_softplus(tf.stop_gradient(Y), s, hidSize, inSize)
    
    
    return ln_rec, lo_wind, Y, F, U, b, p, h, s_prime, s_buff, L  

# Make a non-temporal AWKL pooling layer
def SimpleAWKL_softpool(h, poolSize, hidSize, spread):
    
    # make h_buff placeholder
    h_buff = tf.placeholder(tf.float32, shape = (h.shape[0], None), name = 'h_buff'  )
    
    # Basic Correlation
    Rsqr = tf.pow( bm.addCorrelationMatrix(h_buff, diag0 = True), 2 )
    
    # Basic AWKL Constraint
    lo_pool, Y, L = sm.SimpleKohonen(Rsqr, poolSize, 10, spread, center = 0)
    
    # defined a generic pooling layer
    ln_rec, P, V, q, h_prime = wl.windowed_softpool(tf.stop_gradient(Y), h, poolSize, hidSize)
    
    return ln_rec, lo_pool, Y, P, V, q, h_prime, h_buff, L
    


# Keep track of gamma/learning rates the need to change according to developmental time
# Also use to make timed khohnen constraints!
# Will need to make a version of this that works with a map...
# Actually not hard to do just need to make an alternate init structure where
# layer awakening times are explicitly given then a rec time
class LayerClock:
    
    def __init__(self, num_modules, windTime, recTime, inflDecayRate, poolSpreadAdj = 10):
        
        self.numLayers = num_modules*2
        self.inflDecayRate =  np.zeros( (self.numLayers,1), dtype=np.float32 )
        self.trigTimesWind  = np.zeros( (self.numLayers,1), dtype=np.float32 )
        self.trigTimesRec   = np.zeros( (self.numLayers,1), dtype=np.float32 )
    
        t = 0
        for i in range(0, self.numLayers):
            
            self.trigTimesWind[i] = t
            self.trigTimesRec[i]  = t+windTime
            self.inflDecayRate[i] = inflDecayRate
            if np.mod(i,2)!=0:
                self.inflDecayRate[i] = inflDecayRate*poolSpreadAdj
            t = t+windTime+recTime
    
    #def __init__(self, layerWakeTimes, recTime, inflDecayRate)
        # We will define this later fairly simple really     
       
    def getWind(self,t):
        
        gammas_wind = self.trigTimesWind<=t
        #gammas_wind_sm = 1/(1+np.exp(-(t-self.trigTimesWind)/100))
        
        return gammas_wind.astype(np.float32) #, gammas_wind_sm
        
    def getRec(self,t):
        gammas_rec = self.trigTimesRec<=t
        #gammas_rec_sm = 1/(1+np.exp(-(t-self.trigTimesRec)/100))
        
        return gammas_rec.astype(np.float32) #, gammas_rec_sm
    
    # CHECK THIS OUT AND SEE IF IT WORKS    
    def timeAfterWindTrig(self,t):
        tAfterWind = (t-self.trigTimesWind)*( (t-self.trigTimesWind)>0 ) 
        return tAfterWind
    
    def getSpread(self,t):
        spread = 1.0/(1.0+self.inflDecayRate*self.timeAfterWindTrig(t))
        return spread
    
    # Obsolete
#    def makeGammas(self):
#        
#        hyperParams = tf.placeholder(tf.float32, (self.numLayers,1) )
#        gammaRecs = tf.placeholder(tf.float32, (self.numLayers,1) )
#        spreads = tf.placeholder(tf.float32, (self.numLayers,1) )
#        return hyperParams

    # Can keep adding too
    def makeHypers(self):
        # ind 1: Layer
        # ind 2: hyperparam type
        
        hypers = tf.placeholder(tf.float32, [self.numLayers,3] )
        return hypers
    
    def getHypers(self, t):
        #     gam_wind gam_recs spread 
        # L1 
        # L2
        hypers = np.zeros( (self.numLayers, 3 ), dtype = np.float32 )
        hypers[:,0] = self.getWind(t)[:,0]
        hypers[:,1] = self.getRec(t)[:,0]
        hypers[:,2] = self.getSpread(t)[:,0]
        
        return hypers
        
    
    def printStage(self,t):
        
        gammas_wind = self.getWind(t)
        gammas_rec = self.getRec(t)
        spreads = self.getSpread(t)
        
        print('\nActive Window Learning')
        print(gammas_wind.transpose())
        print('\nActive Reconstruction Learning')
        print(gammas_rec.transpose())
        print('\nKhonen spread values')
        print(spreads.transpose())    
        
def LayerClock_Test():
    print('Testing layer clock')
    lc = LayerClock(2, 2000, 2000, .001 )
    
    intr = 1000
    for i in range(0,20):
        t = i*intr
        print('\n\nTIME STEP ='+str(t))
        lc.printStage(t)



"""
_______________________________________________________________________________
_______________________________________________________________________________
_______R-CORELLATION AWL__________________________________________________R-AWL
_______________________________________________________________________________
_______________________________________________________________________________

"""

def StagedAWRL_Test():
    
    # import array perlin to generate noise stimuli at multiple scales
    import Foveal as fv
    import matplotlib.pyplot as plt
    
    # Define the stimulus generators size and dimensions
    rez = 15 #41
    depth = 2
    width = 1.7
    topScale = 1
    # Find the size 
    fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, width, topScale=topScale)
    inSize = fDogMasked.shape[0]
    
    # Define the input to the Network
    s = tf.placeholder(tf.float32, shape = (inSize, 1))
    
    # Create a StagedAWEL module
    hidSize = round(inSize*1.5)
    outSize = round(hidSize*.75)
    bufLength = 300
    cutoffs = [.1, .1]          # Rsqr values at which to abreviate the membership matrices
    maxWinds = [100, 100]       # The maximum number of units in the windows and pools
    exclusiveness = True        # Whether the pools are mostly non-overlapping or not
    poolAdj = .0001             # Ajustment for how exlusive the pools are
    hidUpdaters, poolUpdaters, h, q, s_prime, h_prime, buffUp, W_h, W_q = StagedAWRL(s, inSize, hidSize, outSize, bufLength, cutoffs, maxWinds, poolExclusive=exclusiveness, poolAdj=poolAdj)
    
    # Print net statistics
    print('     in:'+str(inSize))
    print(' hidden:'+str(hidSize))
    print('   pool:'+str(outSize))
    
    
    # Put updaters in a list for layer clock to use
    op_all = [hidUpdaters,poolUpdaters]
    layTimes = np.array([4000, 4000])     # steps for layers to learn embedding
    
    lRate = .01     # Learning rate for embedding
    windUpPer = 20  # how often to recalculate W membership and indices (Very expensive for larger inputs)
    # Create stage clock, this automatically creates optimizers for AWEL layers
    # and chooses the correct operations to run during the training phases
    sClock = awrlClock( op_all, layTimes, lRate, windUpPer)
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    # Create Basic Visiualization of window membership histograms
    #windPanel = WindowPanel()
    
    # Noise type
    noiseType = 'perl'
    
    t = 0
    # Fill Buffers
    for i in range(0, bufLength):
        
        # Create Stimulus
        fDog, s_val = fv.GetFovealPerlinDog(rez, depth, width, noiseType = noiseType, topScale=topScale)
        
        feed_dict = {s:s_val}
        
        # Run Models
        h_val, q_val, Wh_val, Wq_val, dummy = sess.run([h, q, W_h, W_q, buffUp], feed_dict=feed_dict)
        
    
    # Train model
    while( not sClock.done):
        
        # Create stimulus
        fDog, s_val = fv.GetFovealPerlinDog(rez, depth, width, noiseType = noiseType, topScale=topScale)
        
        feed_dict = {s:s_val}
        
        # Run Model
        ops_to_eval = sClock.addStageOps( [h, q, buffUp, s_prime] )
        vals = sess.run(ops_to_eval, feed_dict=feed_dict)
        
        # Retrieve Values
        h_val = vals[0]
        q_val = vals[1]
        obj_val = vals[4]
        #Wh_val = vals[2]
        #Wq_val = vals[3]
        s_prime_val = vals[3]
        
        # update model ticker
        t = t+1
        sClock.tPlus1()
        
        mode = sClock.addStageOps([], returnModeStr=True,abrev=True)
        
        if np.mod(t, 100)==0:
            print('\nt='+str(sClock.t)) 
            print('ln = '+str(obj_val))
            print('')
            print('max(s) = '+str(np.max(s_val)))
            print('max(s!)= '+str(np.max(s_prime_val)))
            print('Min(h) = '+str(np.min(h_val)))
            print('Avg(h) = '+str(np.mean(h_val)))
            print('Max(h) = '+str(np.max(h_val)))
            print('Min(q) = '+str(np.min(q_val)))
            print('Max(q) = '+str(np.max(q_val)))
            print('')
            sClock.displayStageInfo()
            plt.pause(.01)
        
        # Examine window membership matrices
        #if np.mod(t, 500)==0:
        #    windPanel.plotHist(Wh_val, Wq_val)


"""
AWRL MODEL OUTLINE:

Function Hiarchies:   

Forward:
            q
           / \
          h  [P]
         / \
        s  [W*F]

Backward:
              l_rec1
             /
            s_prime
           / \
          h  [WT*U]
         / \
        s  [W*F]
        
              l_rec2
             /
            h_prime
           / \
          q  [WT*V]
         / \
        h  [W*P]
        
Windows:
         W
        /
      R2


AWRL - Relavant Model Variables:         
    s           input activation pattern
    s_prime     reconstruction of s
    S_buff      buffered activites of s
    
    h           hidden layer activity
    h_prime     hidden layer reconstruction
    H_buff      buffered activities of h
    
    R2          squared pearsond correlation matrix
                one for R2(s) and one for R2(h)          
    
    W           is a window membership matrix
    W_h         membership used to generate h
    W_q         membership used to generate q
                for this model W is simple randomly pulled rows from R2
    
    F           Forward feature weight matrix (used to calculate activity)
    U           Backward feature weight matrix (used to calculate reconstruct)
    b           currently disabled
    p           currently disabled

    P           Forward pool matrix (used to calculate pool output)
    V           Backward pool weight matrix (used to calculate reconstruction of pool input)
        
    l_rec       reconstructive objective function, one for both layers
    

"""
def StagedAWRL(s, inSize, hidSize, outSize, buffLength, cutoffs, maxWinds, poolExclusive=True, poolAdj=.0001):
    
    # Define the hidden layer
    l_rec1, windAsgn1, h, s_prime, s_up, W_h = StagedAWRL_hidden(s, inSize, hidSize, buffLength, cutoffs[0], maxWinds[0])
    
    # Pack reconstructive objective and window update variables together
    hidUpdates = [l_rec1, windAsgn1]
    
    # Define the pooling layer
    l_rec2, windAsgn2, q, h_prime, h_up, W_q = StagedAWRL_pool(h, hidSize, outSize, buffLength, cutoffs[1], maxWinds[1], poolExclusive=poolExclusive, poolAdj=poolAdj)
    
    # Pack reconstructive objective and window update variables together
    poolUpdates = [l_rec2, windAsgn2]
    
    # Merge updates for s and h buffers
    buff_ups = s_up[0,0]+h_up[0,0]
    
    return hidUpdates, poolUpdates, h, q, s_prime, h_prime, buff_ups, W_h, W_q


def StagedAWRL_hidden(s, inSize, hidSize, buffLength, cutoff, maxWind):
    # Instantiate s_buff
    s_up, S_buff, S_buff_zero  = bm.addActivityBuffer(s, (inSize, 1), buffLength)
    
    method='short'
    if method=='mini':
        # Calculate and set rows of R2 sequentially
        R2 = tf.Variable(tf.zeros( (inSize, inSize) ), name='R2_hid')
        RupNum = 200
        inds = tf.Variable(np.arange(RupNum)[:,np.newaxis], dtype=tf.int32)
        inds_up = tf.assign(inds, inds+RupNum )
        rowInds = tf.to_int32(tf.mod( inds_up, inSize ))
        R2_rows = tf.pow( bm.addCorrelationMatrix_Sub( S_buff, rowInds)  , 2)
        rowRun = R2_rows*.1+.9*tf.gather(R2,tf.squeeze(rowInds))
        R2_up = tf.scatter_nd_update(R2, rowInds, rowRun)
        
        # Index R2 to make hidden windows
        baseInds = np.random.permutation(inSize)[:,np.newaxis]
        repNum = np.ceil(hidSize/inSize)
        WrowInds = np.repeat(baseInds, repNum, axis=0)[0:hidSize,:]
        
        # Create membership matrix based on randomly pulled rows of R2
        W = tf.gather_nd(R2, WrowInds, name='W_hid')
        
        # Define hidden layer
        l_rec, inds_asgn, F, U, h, s_prime, winSize = wl.sparse_windowed_hidden(tf.stop_gradient(W), s, hidSize, inSize, cutoff, maxWind)
        
        l_rec = l_rec+0.0*R2_up[0,0]
        
        return l_rec, inds_asgn, h, s_prime, s_up, W
    
    # This is not ready to use at all...
    else: # Method 'Short'
        
        upNum = 100        
        R2_short, cRows, cCols = wl.addDoubleShort(hidSize, inSize, maxWind, upNum)        
        
        # Index R2 to make random hidden windows 
        baseInds = np.random.permutation(inSize)[:,np.newaxis]
        repNum = np.ceil(hidSize/inSize)
        WrowInds = np.repeat(baseInds, repNum, axis=0)[0:hidSize,:]
        
        # Calc updates for R2_short row-wise
        permInds = tf.gather(WrowInds, cRows)        
        R2_fullR = tf.pow( bm.addCorrelationMatrix_Sub( S_buff, permInds)  , 2)
        permBuff = tf.gather(S_buff, WrowInds)
        R2_fullC = tf.pow( bm.addCorrelationMatrix_Sub( permBuff, cCols)  , 2)
        
        # Create membership matrix based on randomly pulled rows of R2
        R2_short_up = wl.addDoubleShortUpdate(R2_short, R2_fullR, R2_fullC)
        
        return l_rec, R2_short_up, h, s_prime, s_up, R2_fullR
    
    
def StagedAWRL_pool(h, hidSize, outSize, buffLength, cutoff, maxWind, poolExclusive=True, poolAdj=.0001):
    # Instantiate s_buff
    h_up, H_buff, H_buff_zero  = bm.addActivityBuffer(h, (hidSize, 1), buffLength)
    
    # Calculate and set rows of R2 sequentially
    R2 = tf.Variable(tf.zeros( (hidSize, hidSize) ), name='R2_pool')
    RupNum = 200
    inds = tf.Variable(np.arange(RupNum)[:,np.newaxis], dtype=tf.int32)
    inds_up = tf.assign(inds, inds+RupNum )
    rowInds = tf.to_int32(tf.mod( inds_up, hidSize ))
    R2_rows = tf.pow( bm.addCorrelationMatrix_Sub( H_buff, rowInds)  , 2)
    rowRun = R2_rows*.1+.9*tf.gather(R2,tf.squeeze(rowInds))
    R2_up = tf.scatter_nd_update(R2, rowInds, rowRun)
    
    # Index R2 to make hidden windows
    baseInds = np.random.permutation(hidSize)[:,np.newaxis]
    repNum = np.ceil(outSize/hidSize)
    WrowInds = np.repeat(baseInds, repNum, axis=0)[0:outSize,:]
    
    # Create membership matrix based on randomly pulled rows of R2    
    W = tf.gather_nd(R2, WrowInds, name='W_pool')
    
    # Define pool layer
    l_rec, inds_asgn, P, V, q, h_prime, poolSize = wl.sparse_windowed_pool(tf.stop_gradient(W), h, outSize, hidSize, cutoff, maxWind, exclusiveness=poolExclusive, poolAdj = poolAdj)
    
    # Merge calls of reconstructive objective with updating R2
    l_rec = l_rec+0.0*R2_up[0,0]
    
    return l_rec, inds_asgn, q, h_prime, h_up, W

class awrlClock:

    def __init__(self, op_all, layTimes, lRate, windUpPer=50):
        # Ops_all
        #          [[train1, windUp1],
        #           [train1, windUp2]]
        self.op_all = op_all
        self.windUpPer = windUpPer 
        self.lRate = lRate
        self.t = 0
        
        #   Determine layer learning start times
        self.layStarts = np.zeros_like(layTimes) 
        t_cur = 0
        self.doneTime = 0
        for lay in range(0, layTimes.shape[0]):
            t_cur = t_cur + layTimes[lay]
            if lay<(layTimes.shape[0]-1):
                self.layStarts[lay+1] = t_cur
            else:
                self.doneTime = t_cur
                print(' Steps To Complete: '+str(self.doneTime))

        # These will hold each version of each optimizer type
        self.trainOpts = []
        self.windAsgns = []
        self.basicOpts()
        self.done = False
        
    # Set up basic op schedule lists
    def basicOpts(self):
        
        optimizer = tf.train.GradientDescentOptimizer(1)
        # Make window optimizer list
        for i in range(0, self.layStarts.shape[0]):
            train = optimizer.minimize( self.lRate*self.op_all[i][0] )
            self.trainOpts.append(train)
            
        # Make the window assignment op list
        for i in range(0, self.layStarts.shape[0]):
            self.windAsgns.append(self.op_all[i][1])
            
    def addStageOps(self, opsToRun, returnModeStr=False, abrev=False):
        
        cLay, cLayStart, cLayTime = self.getStageInfo(self.t)
        
        modeStr = 'done'
        modeAbr = 'done'
        
        if not self.done:
            
            if not np.mod(cLayTime, self.windUpPer)==0:
                opsToRun.append(self.op_all[cLay][0])
                opsToRun.append(self.trainOpts[cLay])
                modeStr = 'Feature and R2 update'
                modeAbr = 'feat'
            else:
                opsToRun.append(self.op_all[cLay][1])
                opsToRun.append(self.windAsgns[cLay])
                modeStr = 'Window Update'
                modeAbr = 'wind'
            
        if returnModeStr:
            if abrev:
                return modeAbr
            else:
                return modeStr
        else:
            return opsToRun


    def tPlus1(self):
        self.t = self.t + 1
        self.done = not (self.t<=self.doneTime)
            
    
    def getStageInfo(self, t):
        cLay = np.argmax(  self.layStarts* (self.layStarts<=t) )
        cLayStart = self.layStarts[cLay] 
        cLayTime = t-cLayStart
        
        return cLay, cLayStart, cLayTime
    
    def displayStageInfo(self):
        cLay, cLayStart, cLayTime = self.getStageInfo(self.t)
        print('     Current Layer: '+ str(cLay))
        print('  Layer Start Time: '+ str(cLayStart))
        print('Layer Elapsed Time: '+ str(cLayTime))
        modeStr = self.addStageOps([], returnModeStr = True)
        print('        Layer Mode: '+modeStr)


def awrlClock_Test():

    d = tf.Variable(tf.random_normal([1, 1]), name='dummy')
    
    op_all = [[d,d],[d,d], [d,d]]
    
    layTimes = np.array([5])
    
    lRate = .5

    sClock = awrlClock(op_all, layTimes, lRate, windUpPer = 3)
    
    while not sClock.done:
        
        sClock.addStageOps([])
        
        sClock.displayStageInfo()
        
        sClock.tPlus1()
        print('')
        
 
# CURRENT TESTING FUNCTION TO AUTO CALL
# THIS NEEDS TO BE AFTER ANY FUNCTIONS YOU ARE CHANGING OR CHANGES WILL NOT
# TAKE EFFECT AT ALL
#SimpleAWKL_Test()
#StagedAWKL_Test()
#StagedAWEL_Test()
#StageClock_Test()
#awrlClock_Test()
#StagedAWRL_Test()

