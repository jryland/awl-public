#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 12 17:17:11 2017

@author: james
"""

import tensorflow as tf


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy as sp
import scipy.ndimage as nimg
import mnist as mn
import numpy as np

def corrTest():
    
    # not too bad...
    S, O = getData()
    S=S/np.max(S)
    
    #S = np.random.rand(784, 2000)
    
    s_buff = tf.placeholder(tf.float32, shape = [None, None ], name='s_buff')
    
    s_buff_cent = s_buff-tf.reduce_mean(s_buff, axis = 1)[:,tf.newaxis]
    s_buff_norm = s_buff_cent/(.00000001+tf.pow( tf.reduce_sum( tf.pow(s_buff_cent,2) , axis=1), .5)[:,tf.newaxis])
    cov = tf.matmul(s_buff_norm, tf.transpose( s_buff_norm ))
    R2 = tf.pow(cov, 2) 
    
    # Setup op that initializes all the variables
    init = tf.global_variables_initializer()
    
    # Make new session
    sess = tf.Session()
    sess.run(init)
    R2_val = sess.run(R2, feed_dict= {s_buff:S[:,0:2000]})
    # All thats needed to correlation, still kinda grose
      
    R2_val = R2_val-np.diag(np.diag(R2_val))
    
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    imshow1 = ax.imshow( R2_val , cmap="hot", interpolation = 'nearest' )
    print(R2_val )
    print(np.max(R2_val) )



    
def getData(part = 'training'):
    
    mnistPath = "../Basic Data Sets/MNIST/"
    
    # Loading raw data
    imgStack_train, labels_train = mn.load_mnist(part,path = mnistPath, selection=slice(0, 60000), return_labels=True)
    n, x, y, = np.shape(imgStack_train)
    
    
    imgStack_train = nimg.filters.gaussian_filter(imgStack_train, .3, order=0, mode='reflect')    
    
    #imgStack_train = np.convolve(imgStack_train, gauss, 'same')
    
    
    # Formating the data for the network
    S_train = np.reshape( imgStack_train, [n, x*y], 'F').transpose( [1, 0] )
    
    digits = np.mgrid[0:10, 0:n]
    #print(digits.shape)
    digits = digits[0,:,:]
    
    O_train = np.equal (labels_train[:,np.newaxis].transpose([1,0]), digits, )
    O_train = O_train.astype(int)
    
    #print(O_train)
    
    return S_train, O_train
    
    
corrTest()