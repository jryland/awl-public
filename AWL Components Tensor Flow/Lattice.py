# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 17:50:23 2016

@author: james

These classes and function are designed to create 2D geometric lattices for
multipurpose use. 

will be fit into a unit area centered at <0,0> for ease of use, or scaled to
create unit area based on then number of units.

latticeTypes = {hex, grid, random}
shapes = {square, circle}

Note:  For circular bounderies the hex lattice may not give full set of points
       when the desired number of points is too small 
       
       
       Y is the set of final points that will be used for the lattice

        Types = {"hex", "grid", "random"}
        shape = {"square", "circle"}
        verts = # of vertices
        exact = {True, False} exact number of vertices

"""
import numpy as np
import math

def makeLattice(latticeType, latticeShape, numVerts, exact):
    """Get the points for a particular type of lattice """
    lat = LatticeMaker(latticeType, latticeShape, numVerts, exact)
    
    return lat.Y
    


import matplotlib.pyplot as plt

# Test the lattice types 
def testLattice(): 
    """test lattice generation """
    
    numP = 500    
    types = ('hex', 'grid', 'random')
    shapes = ('square', 'circle')
    exact = (True, False)
    
    graph = 1
    for t in range(0,len(types)):    
        for s in range(0,len(shapes)):
            for ex in range(len(exact)):
                
                Y = makeLattice(types[t], shapes[s], numP, exact[ex])
                plt.scatter(Y[:,0],Y[:,1])    
                plt.show()
                graph = graph + 1

def testEdges():
    
    numP = 1000
    Type = 'hex'
    shape = 'circle'
    exact = True
    Y = makeLattice(Type, shape, numP, exact)
    plt.scatter(Y[:,0],Y[:,1])    
    plt.show()


class LatticeMaker:
    """A class for making and manipulating geometric Lattices """

    def __init__(self, latticeType, latticeShape, numVerts, exact):
        
        self.latticeType = latticeType
        self.latticeShape = latticeShape
        self.numVersts = numVerts
        self.Y = np.zeros( (numVerts, 2) )    # The actual 2D vertices (row vectors)
        self.numY = numVerts
        self.exact = exact        
        self.scanFill()
        
        

    # Create MAP LATTICE
    # Scan for available grid positions
    # Choose random subset of available
    def scanFill(self):
        
        # fill this up
        self.viablePoints = np.zeros( (self.numY*4, 2) )
        
        # hex scan 
        if self.latticeType == 'hex':            
            self.scanHexLattice()
            
        # grid scan
        elif self.latticeType == 'grid':
            self.scanGridLattice()
        
        # random Lattice
        elif self.latticeType == 'random':           
            self.scanRandomLattice()

        # Determine whether to match desired number of points exactly or to
        # use the complete set of viable points
        if self.exact:                    
            # Get random sample of viable points to fill Y exactly
            randOrder = np.random.permutation(self.viablePoints.shape[0])
            #randOrder = randOrder[0:self.numY]
            #self.Y = self.viablePoints[randOrder, :]
            
            self.viablePoints = self.viablePoints[randOrder,:]    
            
            radii = np.sum( np.power(self.viablePoints, 2), axis=1)
            radiusOrder = np.argsort( radii )[0:self.numY]
            self.viablePoints = self.viablePoints[radiusOrder]
            
            randOrder2 = np.random.permutation(self.viablePoints.shape[0])
            self.Y = self.viablePoints[randOrder2,:]
            
            
        else:
            # Get all viable points and resize Y
            self.Y = self.viablePoints
            self.numY = self.Y.shape[0]
            
        self.numY = self.Y.shape[0]
        print('Num Verts: '+str(self.numY))
    
    
    def inBounds(self, pos):
        """Check to see is a position is in the boundery"""
        inBound = False
        
        # circle 
        if self.latticeShape == 'circle':
            if (pow(pos[0],2)+pow(pos[1],2))<=pow(.5,2):
                inBound = True
        
        # square
        elif self.latticeShape == 'square':
            if (abs(pos[0])<=.5) & (abs(pos[1])<=.5):
                inBound = True
                
        return inBound
        
    
    def scanRandomLattice(self):
        """Randomly select new points and collect viable points"""
        print('Random Lattice')
        print(self.latticeShape + ' Boundery')        
        
        self.viablePoints = np.zeros( (self.numY, 2) )        
        
        for vert in range(0, self.numY):
            
            pos = np.random.rand(2)
            pos = pos - np.array((.5, 
                                  .5))
            
            if self.inBounds(pos):
                self.viablePoints[vert, :] =  pos[np.newaxis, :] 
                    
    
    
    def scanGridLattice(self):
        "Scan accross and collect viable grid points"
        print('Grid Lattice')
        print(self.latticeShape + ' Boundery')        
                
        
        totalArea = self.numY  # this is the total area of all the unit squares
        if self.latticeShape == 'circle':
            totalArea = self.numY/( np.pi*pow(.49, 2) ) 
            # this is the total area of all the unit squares 
            # scaled to fit an incribed circle
        
        scaleAdjust = pow( totalArea, .5)             
        
        step = 1/scaleAdjust   # up down left right step size
        
        # Setup scanner        
        numRows = math.ceil(1/step)            
        numCol = math.ceil(1/step)  
        vert = 0        
        
        # Scan over all points and store Viable ones
        for i in range(0, numRows):
            
            for j in range(0, numCol):
                
                pos = np.zeros(2)
                pos[0] = (i*step)-.5
                pos[1] = (j*step)-.5                    
                
                if self.inBounds(pos):
                    self.viablePoints[vert, :] =  pos[np.newaxis, :] 
                    vert = vert + 1
                    
        self.viablePoints = self.viablePoints[0:vert, :]
    
    
    def scanHexLattice(self ):
        "Scan accross and collect viable hex points"
        print('Hex Lattice')
        print(self.latticeShape + ' Boundery')        
        
    
        # using half of equalateral triang ABC
        # C is right angle
        # A is 30 deg
        # B is 60 deg
        # AB = BC = AC = 1
        # AC = ABcos(30)

        # Lattice Diagram     (Spaces used for formatting purposes)            
        # *   *   B C * 
        #          \|---- AC
        #   *   *   A---- shift            
        
        AB = 1             
        AC = AB * np.cos(30/180*np.pi)
        shift = .5
        rStep = 1            
        
        # *---*     area of rombus is 2x area of equalateral triangle 
        #  \ / \    eq traingle is 2x (1/2)AC*BC = AC*BC: So (2*AC*BC) 
        #   *___*   we want base rombus to have unit area for easy scaling
        
        # Scale all             
        rombisArea = AC        # this is the area of a vertex rombus    
        totalArea = self.numY  # this is the total area of all the unit rombi
        if self.latticeShape == 'circle':
            totalArea = self.numY/( np.pi*pow(.49, 2) ) 
            # this is the total area of all the unit rombis's 
            # scaled to fit an incribed circle
        
        scaleAdjust = pow( totalArea*rombisArea, .5)             
        
        uStep = AC   /scaleAdjust   # Up down step size
        shift = shift/scaleAdjust   # even odd shift
        rStep = rStep/scaleAdjust   # left right step size
        
        # Setup the scanner
        numRows = math.ceil(1/uStep)            
        numCol = math.ceil(1/rStep)
        vert = 0            
        
        # Scan over all points and store Viable ones
        for i in range(0, numRows):
            
            rStart = 0
            if i%2==0:
                rStart = shift;
            
            for j in range(0, numCol):
                
                pos = np.zeros(2)
                pos[0] = (i*uStep       )-.5
                pos[1] = (j*rStep+rStart)-.5                    
                
                if self.inBounds(pos):
                    self.viablePoints[vert, :] =  pos[np.newaxis, :] 
                    vert = vert + 1
            
        self.viablePoints = self.viablePoints[0:vert, :]
        #print(self.viablePoints)
            
#testEdges()