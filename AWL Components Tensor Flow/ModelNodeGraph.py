# -*- coding: utf-8 -*-
"""
Created on Wed Oct 26 19:11:25 2016

@author: james

These classes are for finding the network update
orders. These use abstract nodes and a flow
model to figure out what the update order should
be. 
"""
# This is just a usefull class for modeling nodes that are connected to one
# Another. It is to be used for checking graph attributes not for enacting
# The behavior of the NetworkModuleGraph
class Node:
    def __init__(self, nodeType, num):
        self.num = num
        self.nodeType = nodeType
        self.inRefs = []
        self.outRefs = []   # Up
        self.inTokens = []  # Down
        self.outTokens = []
        self.firedUp = False
        self.firedDown = False
        
    def addIn(self, _node):
        self.inRefs.append(_node)  
        self.inTokens.append(False)
        
        
    def addOut(self, _node):
        self.outRefs.append(_node)        
        self.outTokens.append(False)
        
    def nodeString(self):
        
        nStr = "("+str(self.num)+")\t\t____In("
        
        for i in range(0, len( self.inRefs ) ):
            
            nStr = nStr + str(self.inRefs[i].num) + ","
        
        nStr = nStr + ")\t\t___Out("
        
        for i in range(0, len( self.outRefs ) ):
            
            nStr = nStr + str(self.outRefs[i].num) + ","
        
        nStr = nStr + ")\t\t___Type("+self.nodeType+")"
        
        return nStr
        
    def tokenString(self):
        
        nStr = "("+str(self.num)+")\t\t____In("
        
        for i in range(0, len( self.inRefs ) ):
            
            val = 'O'
            if self.inTokens[i]:
                val = 'X'
            nStr = nStr + val + ","
        
        nStr = nStr + ")\t\t___Out("
        
        for i in range(0, len( self.outRefs ) ):
            
            val = 'O'
            if self.outTokens[i]:
                val = 'X'
            nStr = nStr + val + ","
        
        nStr = nStr + ")\t\t___Type("+self.nodeType+")"
        
        if self.firedUp:
            nStr = nStr + "\t\t___FiredUp"
        elif self.firedDown:
            nStr = nStr + "\t\t___FiredDown"
        
        
        return nStr
        
    def fireIfAllTokens(self):
        # Check to see if node should send tokens
        # Return true if tokens sent, use this to
        # update the order 
        if not self.firedUp:
                
            tokenCheck = True            
            
            # check if all tokens recieved
            for i in range(0, len(self.inTokens)):
                
                if not self.inTokens[i]:
                    tokenCheck = False                    
                    break
                
            
            # send out tokens if firing              
            if tokenCheck:
                              
                self.fireUp()
                            
                self.firedUp = True
                
                return True # only return true when first firing
                
                
            return False
        
        else:
            
            return False
    
    def fireIfOneToken(self):
        # Check to see if node should send tokens
        # Return true if tokens sent, use this to
        # update the order 
        if not self.firedUp:
                
            tokenCheck = False 
            
            # check a token recieved
            for i in range(0, len(self.inTokens)):

                if self.inTokens[i]:
                    tokenCheck = True                    
                    break
                
            # If input and 0 in take connections
            if (len(self.inTokens) == 0 and self.nodeType == 'In' ):
                tokenCheck = True
                
            
            # send out tokens if firing              
            if tokenCheck:
                              
                self.fireUp()
                            
                self.firedUp = True
                
                return True # only return true when first firing
            else:
                 return False           
        else:
            return False
            
                
    def fireUp(self):
        
        # Roll through each token to send out
        for i in range(0, len(self.outRefs)):
            
            # Find the correct inToken to set to true
            for j in range(0, len(self.outRefs[i].inRefs)):
            
                if self.num == self.outRefs[i].inRefs[j].num:
                    
                    self.outRefs[i].inTokens[j] = True
        
        
    def checkNodeDown():
        print("bla")        
    

# This makes a model of a network graph, so that key information can be found
# Such as the update and reverse update order, check for cycles, check for inputs.
# Check for outputs, check for stranded modules.
class ModelNodeGraph:
    
    def __init__(self, nodeTypeList, nodeConnections):

        self.nodeTypes = ("In", "Module", "Out")
        
        self.nodeTypeList = nodeTypeList
        self.nodeConnections = nodeConnections
        
        self.nodeList = []
        
        self.cyclic = False
        self.allReachable = False   
        
        self.fullPassAvailable = False
        self.contPassAvailable = False
        
        for i in range(0, len( nodeTypeList) ):
            
            # Make new node and store it in the node list
            self.nodeList.append( Node( nodeTypeList[i], i ))
            
        for i in range(0, len(self.nodeList) ):
            
            for j in range(0, len(self.nodeConnections[i]) ):
                # add in/out ref
                fromN = i
                toN   = self.nodeConnections[i][j]
            
                self.nodeList[fromN].addOut( self.nodeList[ toN ] )
            
                self.nodeList[toN].addIn( self.nodeList[ fromN ] )
        
        # Find the forward update order
        self.flowUp()
    
    def flowUp(self):
        
        # Find the feed forward update order for non-cyclic graphs
        # If graph is cyclic order will be arbitrary_____________________________
        self.orderUp = []                  
        self.freeUp = []        
        self.dispTokenGraph()        
        self.forwardCheck = False
        
        self.fullPassUpStepsString =  "\n_______________________________________Full Pass Up__________________________________________" 
        self.freePassUpStepsString  = "\n_______________________________________Free Pass Up__________________________________________"
        
        for i in range(0, len(self.nodeList)):
            
            # Print Token states
            #self.dispTokenGraph()
            
            # Perform node update step
            for i in range(0, len(self.nodeList)):
                
                if(self.nodeList[i].fireIfAllTokens()):
                    self.fullPassUpStepsString += "\nStep="+str(i)+self.dispTokenGraph()
                    self.orderUp.append(i)
            
            # Check if all nodes have fired
            self.forwardCheck = True
            for i in range(0, len(self.nodeList)):
                if not self.nodeList[i].firedUp:
                    self.forwardCheck = False                    
                    break
        
        #print(self.orderUp)
        
        self.forwardCheckString = ""        
        
        if not self.forwardCheck:
            self.forwardCheckString += "\nERROR: Forward Order Not Established"
            for i in range(0, len(self.nodeList)):
                if not self.nodeList[i].firedUp:
                    self.forwardCheckString += "\n\t"+str(self.nodeList[i].num)+" never recieved all inputs!"
        
        self.resetTokens()
        
        # Check for reachable_________________________________________________
        
        # Cycle using fireIfOneToken()
        for i in range(0, len(self.nodeList)):
            
            if(self.nodeList[i].fireIfOneToken()):
                self.freePassUpStepsString += "\n____(Step="+str(i)+")____"+self.dispTokenGraph()
                self.freeUp.append(i)
        
        # Check if all nodes have fired
        self.reachableCheck = True
        for i in range(0, len(self.nodeList)):
            if not self.nodeList[i].firedUp:
                self.reachableCheck = False                    
                break
        
        self.reachableCheckString = ""         
        
        if not self.reachableCheck:
            self.reachableCheckString += "\n\nERROR: Not All Nodes Connected"
            for i in range(0, len(self.nodeList)):
                if not self.nodeList[i].firedUp:
                    self.reachableCheckString += "\n\t"+str(self.nodeList[i].num)+" never recieved any inputs!"            
        
        # Performa a little analysis
        if self.reachableCheck and not self.forwardCheck:
            self.cyclic = True
        else:
            self.cyclic = False
            
        
        
        self.networkAnalysisString = "\n_______________________________________Network Analysis__________________________________________"
        
        if self.reachableCheck and self.forwardCheck:
            self.fullPassAvailable = True
            self.contPassAvailable = True
            self.networkAnalysisString += "\nANALYSIS: Network is ACYCLIC "
            self.networkAnalysisString += "\nANALYSIS: Network can be run in [Full-Pass-Mode] or [Continuouse-Mode]"
        elif self.reachableCheck and not self.forwardCheck:
            self.contPassAvailable = True
            self.networkAnalysisString += self.forwardCheckString            
            self.networkAnalysisString += "\n\nANALYSIS: Network is CYCLIC "
            self.networkAnalysisString += "\nANALYSIS: Network can be run in [Continuous-Mode]"
        else: 
            self.networkAnalysisString += self.forwardCheckString
            self.networkAnalysisString += self.reachableCheckString            
            self.networkAnalysisString += "\n\nANALYSIS: Network may be cyclic and has disconnected nodes"
            self.networkAnalysisString += "\nANALYSIS: Network cannot be run in any mode !!!\n"
            
            
            
        
        
        #print(self.fullPassUpStepsString)
        #print(self.freePassUpStepsString)
        #print(self.networkAnalysisString)
        
        
    
    def resetTokens(self):
        # reset all token states to false
        for i in range(0, len(self.nodeList)):
            
            self.nodeList[i].firedUp = False
            
            for j in range(0, len(self.nodeList[i].inTokens)):
                
                self.nodeList[i].inTokens[j] = False
            
            for j in range(0, len(self.nodeList[i].outTokens)):
                
                self.nodeList[i].outTokens[j] = False
            
             
        
        
    def dispGraph(self):
        
        graphDispString = "\n___________________________________________Network_____________________________________________"
        for i in range(0, len( self.nodeList ) ):
            
            graphDispString += "\n"+self.nodeList[i].nodeString()
            
        return graphDispString
            
    def dispTokenGraph(self):

        tokenDispString = ""
        for i in range(0, len( self.nodeList) ):
            
            tokenDispString += "\n"+self.nodeList[i].tokenString()
            
        return tokenDispString
            
            

# test the NodeGraphModel

# GRAPH 1
#                      0       1         2            3        4         5        6     7
nodeTypesList = [    'In',  'In',    'Module',   'Module', 'Module', 'Module', 'Out', 'Out']

nodeConnections = [ [2, 3], [2, 3] , [4, 5] ,    [4, 5],   [ 3],     [4],      [],    []  ]

nodeGraph = ModelNodeGraph(nodeTypesList, nodeConnections)

print("\n\n\n\nTest Network 1")
print(nodeGraph.dispGraph())                  
print(nodeGraph.networkAnalysisString)



# GRAPH 2
#                      0       1         2            3        4         5        6
nodeTypesList = [    'In',  'In',    'Module',   'Module', 'Module', 'Module', 'Out']

nodeConnections = [ [2, 3], [2, 3] , [4, 5] ,    [4, 5],   [ 5],     [4, 6],      []]

nodeGraph = ModelNodeGraph(nodeTypesList, nodeConnections)

print("\n\n\n\nTest Network 2")
print(nodeGraph.dispGraph())                  
print(nodeGraph.networkAnalysisString)


# GRAPH 3
#                      0       1         2            3        4         5        6
nodeTypesList = [    'In',  'In',    'Module',   'Module', 'Module', 'Module', 'Out']

nodeConnections = [ [2, 3], [2, 3] , [4, 5] ,    [4, 5],   [ 6],     [4, 6],      []]

nodeGraph = ModelNodeGraph(nodeTypesList, nodeConnections)

print("\n\n\n\nTest Network 3")
print(nodeGraph.dispGraph())                  
print(nodeGraph.networkAnalysisString)




