# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 01:01:52 2017

@author: James Ryland


EMBEDDING FUNCTIONS
    SimpleCorrEmbed         <- Implements simple correlational window embedding
    
SOM FUNCTIONS
    SimpleNeuralGas
    SimpleKohonen


"""
import numpy as np
import tensorflow as tf
import Lattice as lat
import Ry_BasicMathTF as bm

# Define the objective function for a simple Neural Gass
def SimpleNeuralGas(x, unitNum, dim, gamma, gHalf, numN, Yinit=[]):
    
    # See about parrallelizing this...
    
    # initialize the starting positions
    Y = tf.Variable(tf.random_normal( [unitNum, dim] ))*.1
    
    if Yinit!=[]:
        Y = Yinit
    
    # find distance to x
    D = addDistanceMatrix(Y, tf.stop_gradient(x))
    
    # Find the top distances
    Dsort, inds  = tf.nn.top_k(tf.squeeze(-D), numN )
    Dsort = -Dsort
    
    k = np.linspace(0, numN, numN )
    
    t = tf.Variable(0)
    t_up = tf.assign(t, t+1)
    gamma_up = tf.to_float( gamma/(1+t_up/gHalf) )
    
    l_gas = tf.reduce_sum( tf.pow( Dsort * tf.exp( -k/gamma_up ), 2 ) ) 

    return l_gas, Y

def test_SimpleNeuralGas():
    
    # create some noise
    import Foveal as fv
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    # Define the input to the Network
    numN = 2
    gamma = 10
    gHalf = 6000
    units = 5000
    dim = 5000
    x = tf.placeholder(tf.float32, shape = (1, dim))
    
    Y_val = (np.random.rand(units, dim)-.5)
    Y_val[:,2] = 0
    Yinit = tf.Variable(Y_val, dtype=tf.float32)
    #Yinit = []
    l_gas, Y = SimpleNeuralGas(x, units, dim, gamma, gHalf, numN, Yinit = Yinit)
    
    optimizer = tf.train.GradientDescentOptimizer(.05)
    train = optimizer.minimize(l_gas)
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    Y_val = sess.run(Y)
    
    # Graphics stuff
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')        
    scM = ax1.scatter(Y_val[:,0], Y_val[:,1], Y_val[:,2])
    pad = 1.2
    ax1.set_xlim( np.min(Y_val[:,0])*pad , np.max(Y_val[:,0]*pad))
    ax1.set_ylim( np.min(Y_val[:,1])*pad , np.max(Y_val[:,1]*pad))
    ax1.set_zlim( np.min(Y_val[:,2])*pad , np.max(Y_val[:,2]*pad))
    
    
    # Fill Buffer to start
    for i in range(0, 1000000):
        
        x_val = (np.random.rand(1, dim)-.5)
        x_val[:,2] = 0
        l_gas_val, Y_val, tr_val = sess.run([l_gas, Y, train], feed_dict = {x:x_val})
        
        if np.mod(i, 10) == 0:
            scM._offsets3d = (Y_val[:,0], Y_val[:,1], Y_val[:,2])
            ax1.set_xlim( np.min(Y_val[:,0])*pad , np.max(Y_val[:,0]*pad))
            ax1.set_ylim( np.min(Y_val[:,1])*pad , np.max(Y_val[:,1]*pad))
            ax1.set_zlim( np.min(Y_val[:,2])*pad , np.max(Y_val[:,2]*pad))
            
            print('l_gas('+str(i)+') = '+str(l_gas_val))
            
            plt.pause(.1)
    
    
# Also called cne or correlational neighbor embeding
# Define the objective function for Windowed Corr Embedding
def SimpleWindowCorEmbed(R2_rows, rowinds, unitNum, dim, nPow = 1.0, shapeT = 2000):
    
    # initialize the starting positions
    X = tf.Variable(tf.random_normal( [unitNum, dim] ))*.1
   
    # X with small noise added to avoid peculiar nan error
    # when points at the same location have a gradient passed to them
    Xalt = X+tf.random_normal([unitNum, dim])*.0001
    
    # relevent distance submatrix
    D = tf.transpose( bm.addDistanceMatrix_SubY(X, Xalt, rowinds ) ) 
    
    # Q model of correlation in terms of distance e^-D
    Q = tf.exp( -tf.pow(D,2), name='Q')
    
    # Objective function ln = sum( (Q-R2)^2 )
    R2_stop = tf.stop_gradient( tf.pow(R2_rows,nPow) )
    
    # Neighbor Shaping Bias  
    #1<nbPow ~ find correlated
    t = tf.Variable(0)
    t_up = tf.assign(t, t+1)
    nShapePow = 2.0*tf.minimum( tf.maximum(1.0-t_up/shapeT, 2.0), 0 )
    nB = tf.stop_gradient( tf.pow(R2_rows, tf.to_float(nShapePow) ) )

    ln_emb = tf.reduce_sum(tf.pow( Q - R2_stop, 2) * nB  )
    
    return ln_emb, X, D, Q

def SimplewEmbedApproxR( D, nPow):
    
    return tf.pow( tf.exp(-tf.pow(D_val,2)), 1/nPow)

def test_SimpleWindowEmbed():
    
    # create some noise
    import Foveal as fv
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    # Define the stimulus generators size and dimensions
    rez = 20 #41
    depth = 1
    width = 2
    topScale = 1
    # Find the size 
    fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, width, topScale=topScale)
    col = fv.getMaskedXY(rez, depth)
    inSize = fDogMasked.shape[0]
    
    
    # Define the input to the Network
    s = tf.placeholder(tf.float32, shape = (inSize, 1))
    s_up, s_buff, S0 = bm.addActivityBuffer(s, (inSize, 1), 200)
    upNum = 200 #inSize
    
    rowInds = tf.random_uniform( (upNum, 1), minval=0, maxval=inSize, dtype=tf.int32)
   
    R2_rows = tf.pow( bm.addCorrelationMatrix_Sub( s_buff, rowInds) , 2)
    
    dim = 3
    
    nPow = .3  #1.2
    
    ln_emb, X, D, Q = SimpleWindowCorEmbed(R2_rows, rowInds, inSize, dim, nPow=nPow, shapeT = 2000)
    
    optimizer = tf.train.GradientDescentOptimizer(.5)
    train = optimizer.minimize(ln_emb)
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    X_val = sess.run(X)
    
    # Graphics stuff
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(121, projection='3d')        
    scM = ax1.scatter(X_val[:,0], X_val[:,1], X_val[:,2], c=col)
    pad = 1.2
    scM.set_facecolors(col)
    ax1.set_xlim( np.min(X_val[:,0])*pad , np.max(X_val[:,0]*pad))
    ax1.set_ylim( np.min(X_val[:,1])*pad , np.max(X_val[:,1]*pad))
    ax1.set_zlim( np.min(X_val[:,2])*pad , np.max(X_val[:,2]*pad))
    
    ax2 = fig.add_subplot(122)        
    ax2.hist
    fig.canvas.draw() 
    plt.show()   
    plt.pause(1)
    
    # Fill Buffer to start
    for i in range(0, 200):
        fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, width, topScale=topScale)
        s_val = fDogMasked
        ln_val, R2_rows_val, dummy, rowInds_valm, D_val, Q_val = sess.run([ln_emb, R2_rows, s_up, rowInds, D, Q], feed_dict = {s:s_val})
        
    # Actually Learn
    for i in range(0, 10000):
        
        fDog, fDogMasked = fv.GetFovealPerlinDog(rez, depth, width, topScale=topScale)
        s_val = fDogMasked
        
        ln_val, X_val, dummy, dummy, D_val, Q_val, R2_val, rowInds_val = sess.run([ln_emb, X, train, s_up, D, Q, R2_rows, rowInds], feed_dict = {s:s_val})
        
        # Dynamically update points
        if np.mod(i, 100)==0:
            
            scM.set_facecolors(col)
            scM._offsets3d = (X_val[:,0], X_val[:,1], X_val[:,2])
            # reset axes limits
            ax1.set_xlim( np.min(X_val[:,0])*pad , np.max(X_val[:,0]*pad))
            ax1.set_ylim( np.min(X_val[:,1])*pad , np.max(X_val[:,1]*pad))
            ax1.set_zlim( np.min(X_val[:,2])*pad , np.max(X_val[:,2]*pad))
            #print('mean(X)='+str(np.mean(X_val)))
            #print(' min(D)='+str(np.min(D_val)))
            #print(' min(Q)='+str(np.min(Q_val)))
            #print('min(R2)='+str(np.min(R2_val)))
            
            # Turn to vecs first!!
            ax2.cla()
            R2aprox = np.power( np.exp(-np.power(D_val,2)), 1/nPow)
            ax2.hist(R2aprox.flatten(), 200, normed=1, facecolor='green', alpha=0.75)
            ax2.set_title('R2 Approx')
            
            print('lo_emb(t='+str(i)+') = '+str(ln_val))
            print('')
            #fig.canvas.draw()    
            plt.pause(.5)
    

# define the objective function for a simple kohonen...
# Requires Lattic.py to function
# X is an input tf variable that contains the total set of target points
# rows of X are points
def SimpleKohonen(X, latSize, numUp, spread, center=0, rad=1, svdRot=[]):
    
    # Import lattice internally
    import Lattice as lat
    
    #dim = X.shape[1]
    Xshape = X.get_shape()
    
    
    # Setting up the latice points
    L = lat.makeLattice('hex', 'circle', latSize, True)
    L = L.astype(np.float32)
    if L.shape[0]!=latSize:
        print("Alert Lattice did not return enough points")
        print("Recieved "+latPoints.shape[0]+" Needed "+latSize)
        
    # Initialize points in fitting space
    initY = np.random.normal( size=[latSize, Xshape[1]] ).astype(np.float32)+np.ones( (latSize, Xshape[1]) )*center
    Y = tf.Variable(initY, dtype=tf.float32, name='Y')
    
        
    # Setup random sampling of distances
    Xinds = tf.random_uniform( [numUp, 1], 0, Xshape[0], tf.int32) 
    Dyx_sub = bm.addDistanceMatrix_SubY(Y, X, Xinds, svdRot = svdRot)
    
    # Calculate lattice influence based on nearest points
    nearestInds = tf.argmin( Dyx_sub, axis=0)[:,tf.newaxis]
    Dll_sub = bm.addDistanceMatrix_SubY(L,L, nearestInds)
    infl = tf.exp( -tf.pow(Dll_sub, 2)/spread)         
    
    # Define Objective function (should we have a ^2 in here?)
    lo = tf.reduce_sum( tf.multiply(infl, Dyx_sub) )
    
    return lo, Y, L   #, spread

    
# This function is for testing the simple kohonen network
# Move to the back when done
def SKTest():
    print('Testing simple Kohonen Definition')
    
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    #from mpl_toolkits.mplot3d import Axes3D # Needed for 3d plots
    
    X = tf.constant( np.random.uniform(-.5,.5, (500, 3)), dtype=np.float32 )
    spread = tf.placeholder( tf.float32, shape = [1,1], name = 'spread')
    lo, Y, L = SimpleKohonen(X, 500, 20, spread )
    
    optimizer = tf.train.GradientDescentOptimizer(.01)
    train = optimizer.minimize(lo)
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    Y_val = sess.run(Y)
    
    
    # Graphics stuff
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    scL = ax1.scatter(L[:,0], L[:,1], 100)
    ax1.set_axis_bgcolor((0, 0, 0))
    ax2 = fig.add_subplot(122, projection='3d')
    scM = ax2.scatter(Y_val[:,0], Y_val[:,1], Y_val[:,2])
    ax2.set_aspect('equal', 'datalim')        
    plt.show()    
    
    for i in range(0, 100000):
        
        # must be (1,1)
        spread_val = np.array( 1.0/(1.0+i*.05) , dtype=np.float32 )[np.newaxis,np.newaxis]
        lo_val, Y_val = sess.run([lo, Y], feed_dict = {spread:spread_val})
        sess.run(train, feed_dict = {spread:spread_val})
        
        # Dynamically update points
        if np.mod(i, 100)==0:
            
            col = (Y_val-np.min(Y_val,axis=0))/(np.max(Y_val,axis=0)-np.min(Y_val,axis=0))
            scL.set_facecolors(col)
            scM._offsets3d = (Y_val[:,0], Y_val[:,1], Y_val[:,2])
            # reset axes limits
            ax2.set_xlim( np.min(Y_val[:,0]) , np.max(Y_val[:,0]))
            ax2.set_ylim( np.min(Y_val[:,1]) , np.max(Y_val[:,1]))
            ax2.set_zlim( np.min(Y_val[:,2]) , np.max(Y_val[:,2]))
            fig.canvas.draw()       
            print('lo(t='+str(i)+') = '+str(lo_val))
            plt.pause(.01)
            