# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 00:32:39 2017

@author: James Ryland

This is a set of mathematical helper functions that offer functionality that 
cannot be found in tensor flow


HELPER CALCULATIONS/FUNCT/OBJs
    bufferNorm
    addActivityBuffer           <- Tensor flow version
    ActivationBuffer            <- Ordinary Object version
    addCorrelationMatrix
    addCorrelationMatrix_Sub    <- finds a subset of the rows of full R2 matrix
    addDistanceMatrix
    addDistanceMatrix_SubY

"""

import tensorflow as tf
import numpy as np
import Ry_LayerGraphs as lg

# Readout layer
# This is a very simple neural network layer that transforms the ouput
# representation of a deep neural representation into a classification
def ReadoutLayer(s, inSize, c, classNum, deepTrain=False, rate = .1, decay = .9, momentum = .1, bias=True, center= False, magNorm = False):
    
    # Check to see if gradients are allowed to propogate back
    if not deepTrain:
        s = tf.stop_gradient(s)
    
    if center:
        s = s-tf.reduce_mean(s, axis=0)
        
    if magNorm:
        s = s/tf.norm(s, axis=0)
        
    W = tf.Variable(tf.random_normal((classNum,inSize)))
    b = tf.Variable(tf.random_normal((classNum,1)))
    
    r = tf.matmul(W,tf.squeeze(s))
    
    if bias:
        r = r + b
    
    p = tf.nn.softmax(r, dim=0)
    
    c = tf.squeeze(c)
    r = tf.squeeze(r)
    
    ln = tf.nn.softmax_cross_entropy_with_logits(labels=c,logits=r, dim=0)
    ln = tf.reduce_sum(ln)
    
    
    #opt = tf.train.AdagradOptimizer(rate)
    opt = tf.train.RMSPropOptimizer(rate, decay, momentum)
    #opt = tf.train.AdamOptimizer(rate) 
    #opt = tf.train.GradientDescentOptimizer(rate)
        
    ln_train = opt.minimize(ln)
    
    return p, ln, ln_train

def test_readoutLayer():
    
    import matplotlib.pyplot as plt
    
    numClass = 100
    
    C_val = np.eye(numClass)
    S_val = C_val+np.random.normal( size=C_val.shape )
    
    s = tf.placeholder(tf.float32, shape = (numClass,None))
    c = tf.placeholder(tf.float32, shape = (numClass,None))
    
    p, ln, ln_train = ReadoutLayer(s, numClass, c, numClass, deepTrain=False, rate=.1, bias = True, center = True, magNorm=True)
    
    # Basic tensor flow initializations
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    acc = 0
    
    accPanel = lg.AccuracyPanel()
    
    for i in range(1, 100000):
        
        # pick a random s
        pick = np.random.randint(0, numClass-10)
        s_val = S_val[:,pick:(pick+10)]
        c_val = C_val[:,pick:(pick+10)]
        
        s_val = s_val+np.random.normal(size=s_val.shape)/10
        c_val = c_val+np.random.normal(size=c_val.shape)/10
        
        ln_val, p_val, dum = sess.run([ln, p, ln_train], feed_dict={s:s_val, c:c_val})
        
        #print(ln_val)
        #print(np.concatenate( (c_val, p_val), axis=1)) 
        
        hit = np.mean(1*(np.argmax(p_val, axis=0)==np.argmax(c_val, axis=0)))
        
        acc = acc*.9+.1*hit*100
        
        print("Accuracy: "+str(acc))
        
        if np.mod(i,100)==0:
            accPanel.update(ln_val, acc, i)
            plt.pause(.01)
            
        
#test_readoutLayer()
    


# Very similar to batch normalization but designed to work on a buffer
def bufferNorm(s, s_shape, S_buff, runningDecay):
    
    # Create variables for holding the mean and variance
    s_mean = tf.Variable(tf.zeros(s_shape))
    s_var  = tf.Variable(tf.ones(s_shape))
    
    # Calculate mean and variance from S_buff
    s_meanNew, s_varNew = tf.nn.moments(S_buff, axes=[1])
    
    # Create updators for mean and variance
    s_mean_up = tf.assign(s_mean, s_mean*(runningDecay)+(1-runningDecay)*s_meanNew[:,tf.newaxis])
    s_var_up  = tf.assign(s_var , s_var *(runningDecay)+(1-runningDecay)*s_varNew[:,tf.newaxis] )
    
    # combine updators
    norm_up = tf.reduce_sum(s_mean_up)+tf.reduce_sum(s_var_up)
    
    # Prevent gradents from passing back to mean and varaince variables
    s_mean_stop = tf.stop_gradient(s_mean)
    s_var_stop = tf.stop_gradient(s_var)
    
    # Apply batch normalization (s-s_mean)/(s_var+beta)
    s_bNorm = tf.nn.batch_normalization(s, s_mean_stop, s_var_stop, None, None, .0000000001)
    
    return s_bNorm, norm_up


def test_bufferNormalization():
    
    X_buff_val = np.random.normal(size=(4,200))*20 + 10
    
    x      = tf.placeholder( tf.float32, shape = [4, 1] )
    X_buff = tf.placeholder( tf.float32, shape = [4,200] )
    
    x_bNorm, norm_up = bufferNorm(x, [4,1], X_buff, .99)
    
    
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    
    for i in range(0,200):
        
        x_val = X_buff_val[:,i,np.newaxis]
        
        x_bNorm_val, dum = sess.run([x_bNorm, norm_up], feed_dict={x:x_val, X_buff:X_buff_val})
        
        print(x_bNorm_val.transpose())
     

# Not designed to work with batch learning...
def addTemporalInput(s, s_shape, steps):
    
    # Create a buffer
    s_up, S_buff, S_buff_zero = addActivityBuffer(s, s_shape, steps)
    
    # reshape buffer to be a new activation vector
    s_temporal = tf.reshape(S_buff, (s_shape[0]*steps,1))
    
    # combine s_temporal op with it's updator
    s_temporal = s_temporal+tf.reduce_sum(s_up)*0
    
    return s_temporal
    

# Implemented in tensor flow!!
# This adds an activity buffer
def addActivityBuffer(s, s_shape, bufferLength, batch=False):
    
    # If using batch learning and buffers pull the first item from a batch
    s = tf.squeeze(s[:,0])[:,tf.newaxis]
    
    # Create the variable to store the buffer
    S_buff = tf.Variable( tf.zeros( [s_shape[0], bufferLength] ) , dtype = tf.float32)
    
    # Remove the end pattern
    S_crop = S_buff[:,0:(bufferLength-1)]
    
    # Concat the new with the old
    S_calc = tf.concat( [s, S_crop], axis=1, name='S_calc')
    
    # Create assignment ref (When used it will update S_buff)
    S_buff_asgn = tf.assign(S_buff, S_calc)
    
    # Create an s that automatically updates S_buff when used
    s_up = S_buff_asgn[:,0,tf.newaxis]

    # Create an op that blanks out the buffer, important for various uses
    S_buff_zero = tf.assign(S_buff, tf.zeros_like(S_buff) )
    
    return s_up, S_buff, S_buff_zero

   
def addActivityBuffer_test():
    
    s = tf.placeholder( tf.float32, shape = [4, 1] )
    
    s_up, S_buff, S_buff_zero = addActivityBuffer(s, s.shape, 5)
    
    # Setup op that initializes all the variables
    init = tf.global_variables_initializer()
    
    # Make new session
    sess = tf.Session()
    sess.run(init)
    
    for i in range(0, 10):
        
        # Value that will be fed into s
        s_val = np.random.rand(4, 1)
        
        feed_dict = {s:s_val}
        
        s_val_new = sess.run(s_up, feed_dict=feed_dict)
        
        print('\nBuffer Values t='+str(i))
        print(sess.run(S_buff))
        print('\n s new value')
        print(s_val_new)
        
#addActivityBuffer_test()

# This is the numpy implementation
# Takes activation vectors or matrices over time and rolls them into a buffer
# Be carefull not to put more than the buffer can take or an error could pop..
# Inputs must be (s,l) or (s,1) arrays
# Assumed order [stim1 stim2 stim3 ...]
# Will apply flip to keep buffer in order [stimt stimt-1 stimt-2]
class ActivationBuffer():
    
    def __init__(self, inDim, buffLenth, randInit = True ):
        #print('Buffer Init')
        self.S_buff = np.zeros( (inDim, buffLenth)  )
        if randInit:
            self.S_buff = np.random.rand( inDim, buffLenth )
        
    def push(self,S_in):
        #print('Add value or values and roll')
        numStim = S_in.shape[1]
        self.S_buff = np.roll(self.S_buff, numStim, 1 )
        self.S_buff[:, 0:numStim] = np.flip(S_in, axis=1)[:, 0:numStim]
        
    def getBuff(self):
        return self.S_buff
    def getEmpty(self):
        return np.zeros_like(self.S_buff)
    
    # def getBuffSmooth(self, filter size)

# Takes an input stream and uses an additive z-transform to create a correlation
# Matrix that can be efficiently updated, this requires no buffer YAY!!
# THANKS FOR THE IDEA DR. GOLDEN!
def addRunningCorrelationMatrix(s, smooth, diag0 = False):
    'Laugh Laugh you fools!'
    
    a = 1-1/smooth
    b = 1/smooth
    
    # Running Variables
    n = tf.Variable(1, dtype = tf.float32 ,trainable=False)
    s_mn = tf.Variable(tf.zeros( (s.shape[0],1)) )
    s_sd = tf.Variable(tf.ones( (s.shape[0],1)) )
    R = tf.Variable(tf.zeros( (s.shape[0],s.shape[0]) ))
    
    # Current Z
    s_cent = (s-s_mn)
    z = (s-s_mn)/s_sd
    
    # Running variable updates
    s_mn_up = tf.assign(s_mn,s_mn*a+s*b  )
    s_sd_up = tf.assign(s_sd, tf.sqrt( tf.pow(s_sd,2)*a +tf.pow(s_cent,2)*b ))
    R_up    = tf.assign(R, R*a+z*tf.transpose(z)*b  )
    n_up    = tf.assign(n, n+1)
    
    # Whole Update
    Rall_up = tf.reduce_sum(s_mn_up)+tf.reduce_sum(s_sd_up)+tf.reduce_sum(R_up)
    Rall_up = Rall_up+n_up
    
    if diag0:
        R = R-tf.diag( tf.diag_part(R) )
    
    return R, Rall_up

# This has turned into a correlation benchmarking file...
def test_addRunningCorrelationMatrix():
    
    inSize = 4
    samples = 1000
    
    S_vals = np.random.rand(inSize,samples)
    s = tf.placeholder(tf.float32, shape = [S_vals.shape[0], 1 ])
    R, R_up = addRunningCorrelationMatrix(s, 100, diag0 = False)
    Rsqr = tf.pow(R,2)
    
    
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    R_val = []
    Rsqr_val = []
    
    for i in range(0, S_vals.shape[1]):
        R_val, Rsqr_val, dummy = sess.run([R, Rsqr, R_up] , feed_dict = {s:S_vals[:,i,np.newaxis] })
    
    print('\nRunning Squared Correlation')
    print(Rsqr_val)
    print('\nUnit Test Complete')
    

# Takes an input matrix and correlates the rows to produce a correlation matrix
def addCorrelationMatrix(s_buff, diag0 = False):
    
    s_buff_cent = s_buff-tf.reduce_mean(s_buff, axis = 1)[:,tf.newaxis]
    s_buff_norm = s_buff_cent/(.00000001+tf.pow( tf.reduce_sum( tf.pow(s_buff_cent,2) , axis=1), .5)[:,tf.newaxis])
    R = tf.matmul(s_buff_norm, tf.transpose( s_buff_norm ))
    
    if not diag0:
        return R
    else:
        R_diag0 = R-tf.diag( tf.diag_part(R) )
        return R_diag0
    
# Takes an input matrix and correlates the rows to produce a correlation matrix
# This will only corrlate the whole set of rows with a subset of rows
# Not clear how to set diag to 0...
def addCorrelationMatrix_Sub(s_buff, row_inds):
    
    s_buff_cent = s_buff-tf.reduce_mean(s_buff, axis = 1)[:,tf.newaxis]
    s_buff_norm = s_buff_cent/(.00000001+tf.pow( tf.reduce_sum( tf.pow(s_buff_cent,2) , axis=1), .5)[:,tf.newaxis])
    
    s_buff_norm_sub = tf.gather_nd(s_buff_norm, row_inds)
    
    R = tf.matmul(s_buff_norm_sub, tf.transpose( s_buff_norm ))
    
    return R
    

# Takes an input matrix and correlates the rows with a sub set of the rows
# def addTruncCorrelationMatrix(s_buff)
    # This could be usefull later for making things more efficient in large 
    # Networks.


# Takes two matrices of points (rowise) and calculates the distance matrix    
def addDistanceMatrix(X, Y):
    
    # Standard distance calculation
    D = tf.pow(tf.reduce_sum(tf.pow(X[:,tf.newaxis,:] - Y[tf.newaxis,:,:],2), axis=2), .5)
    return D
    
    
# Convenience function that wraps a gather operation
# Yrow_inds must be have shape (k,1)  [ [rowind1] [rowind2]... ] 
# This cannot take X and X as inputs when performing gradient descent on X
# Adding and option to calculate Distance using SVD, to reduce computation
# in high dimensional spaces...
# Unfortunately tensorflows svd cannot be capped at a certain number of singular
# Vectors making it scale very poorly... in order for this feature to be usefull
# the svdRot matrix should be calculated over an extended sample once at the
# beggining of training...
def addDistanceMatrix_SubY(X, Y, Yrow_inds, svdRot = []):
    Y_sub = tf.gather_nd(Y, Yrow_inds, name = 'Y_sub')
    
    if svdRot==[]:
        D = tf.pow(tf.reduce_sum(tf.pow(X[:,tf.newaxis,:] - Y_sub[tf.newaxis,:,:],2), axis=2), .5)
        return D
    else:
        UX = tf.matmul(X,svdRot)
        UY = tf.matmul(Y_sub,svdRot)
        D = tf.pow(tf.reduce_sum(tf.pow(UX[:,tf.newaxis,:] - UY[tf.newaxis,:,:],2), axis=2), .5)
        return D
        
     

# This is a unit test for many of the functions in this file
def testBasics():
    
    S = np.random.randint(0, 9, size = (4, 6))
    S_actBuff = ActivationBuffer(4, 5, False)
    print('Activation Buffer')
    print('\n S')
    print(S)
    S_actBuff.push(S[:, 0, np.newaxis])
    print('\n Step 1')
    print(S_actBuff.getBuff())
    S_actBuff.push(S[:, 1, np.newaxis])
    print('\n Step 2')
    print(S_actBuff.getBuff())
    S_actBuff.push(S[:, 2:5])
    print('\n Step 3')
    print(S_actBuff.getBuff())
    
    
    s_buff_val = np.random.rand(4,100)
    s_buff_val[3,:] = 0
    s_buff = tf.placeholder(tf.float32, shape = [None, None ], name='s_buff')
    R = addCorrelationMatrix(s_buff)
    Rsqr = tf.pow(R,2)
    R_d0 = addCorrelationMatrix(s_buff, True)
    Rsqr_d0 = tf.pow(R_d0,2)
    
    X_val = np.random.rand(3, 6)
    Y_val = np.random.rand(3, 6)
    X = tf.placeholder(tf.float32, shape = [None, None], name='X')
    Y = tf.placeholder(tf.float32, shape = [None, None], name='Y')
    D = addDistanceMatrix(X,Y)
    
    inds_val = [[0], [1]]
    inds = tf.placeholder(tf.int32, shape = [None, 1], name='inds')
    D_sub = addDistanceMatrix_SubY(X, Y, inds)
    
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    R_val, Rsqr_val, Rsqr_d0_val, D_val, D_sub_val = sess.run([R, Rsqr, Rsqr_d0, D, D_sub ] , feed_dict = {s_buff:s_buff_val, X:X_val, Y:Y_val, inds:inds_val})
    
    print('\nCorrelation Matrix')
    print(R_val)
    print('\nSquared Correlation Matrix')
    print(Rsqr_val)
    print('\nSquared Correlation Matrix Diag 0')
    print(Rsqr_d0_val)
    print('\nDistance Matrix')
    print(D_val)
    print('\nSub Distance Matrix')
    print(D_sub_val)
    print('\nUnit Test Complete')


#test_bufferNormalization()
        
   